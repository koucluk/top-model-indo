package com.forest_interactive.topmodelindo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.facebook.drawee.drawable.ProgressBarDrawable;
import com.facebook.drawee.view.SimpleDraweeView;
import com.forest_interactive.topmodelindo.Models.Video;
import com.forest_interactive.topmodelindo.Models.Wallpaper;
import com.forest_interactive.topmodelindo.Widgets.CustomProgressFragment;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by abdulla.hainum on 16/2/2016.
 */
public class HomeFrag extends CustomProgressFragment {
    SliderLayout sliderLayout;
    TopModelGlobal global;
    AsyncHttpClient client;
    ImageView bottomBanner, bottomBanner2, bottomBanner3;
    private View mContentView;
    private Handler mHandler;
    private ArrayList<Video> videoObj;
    private ArrayList<Wallpaper> wallpaperObj;
    private boolean running;

    private Runnable mShowContentRunnable = new Runnable() {

        @Override
        public void run() {
            client = new AsyncHttpClient();
            client.setTimeout(40000);

            getRecommendedVideos();

            getRecommendedWallpapers();

            initSliderLayout(mContentView);

            //initDrawees(mContentView);

            initOnClickListeners(mContentView);

            setContentShown(true);
        }

    };

    public static HomeFrag newInstance() {
        HomeFrag home = new HomeFrag();
        return home;
    }

    private void getRecommendedVideos() {
        final SharedPreferences settings = getActivity().getSharedPreferences("sharedpref", 0);
        if (settings.getString("rec_video_data", "").equalsIgnoreCase("")) {
            if (!isNetworkAvailable()) {
                crossFadeViews(mContentView.findViewById(R.id.top_video_progress),
                        mContentView.findViewById(R.id.top_video_error_tv));
                TextView error_tv = (TextView) mContentView.findViewById(R.id.top_video_error_tv);
                error_tv.setText(getStringRes(R.string.error_no_internet));
                mContentView.findViewById(R.id.topvideo_thumbnail1_container).setVisibility(View.GONE);
                mContentView.findViewById(R.id.topvideo_thumbnail2_container).setVisibility(View.GONE);
            } else {
                client.get("http://210.5.41.103/tuneapp/api/FeaturedVideo/top%20model%20indo/3", new TextHttpResponseHandler() {
                /*@Override
                public void onProgress(long bytesWritten, long totalSize) {
                    super.onProgress(bytesWritten, totalSize);
                    ProgressWheel progress = (ProgressWheel) mContentView.findViewById(R.id.top_video_progress);
                    progress.setProgress(bytesWritten/totalSize);
                }*/

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        if (running) {
                            crossFadeViews(mContentView.findViewById(R.id.top_video_progress),
                                    mContentView.findViewById(R.id.top_video_error_tv));
                            mContentView.findViewById(R.id.topvideo_thumbnail1_container).setVisibility(View.GONE);
                            mContentView.findViewById(R.id.topvideo_thumbnail2_container).setVisibility(View.GONE);
                            //return;
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        if (running) {
                            Gson gson = new GsonBuilder().create();
                            JsonParser parser = new JsonParser();
                            JsonArray jArray = parser.parse(responseString).getAsJsonArray();

                            settings.edit().putString("rec_video_data", responseString).commit();

                            videoObj = new ArrayList<Video>();
                            for (JsonElement obj : jArray) {
                                Video temp = gson.fromJson(obj, Video.class);
                                videoObj.add(temp);
                            }

                            if (videoObj.size() > 0) {
                                if (videoObj.size() == 1) {
                                    populateLargeVideoDrawee();

                                    fadeOutView(mContentView.findViewById(R.id.topvideo_thumbnail1_container));
                                    mContentView.findViewById(R.id.topvideo_thumbnail1_container).setVisibility(View.GONE);
                                    fadeOutView(mContentView.findViewById(R.id.topvideo_thumbnail2_container));
                                    mContentView.findViewById(R.id.topvideo_thumbnail2_container).setVisibility(View.GONE);
                                } else if (videoObj.size() == 2) {
                                    populateLargeVideoDrawee();
                                    populateSmallVideoDrawee1();

                                    //center 2
                                    View second = mContentView.findViewById(R.id.topvideo_thumbnail2_container);
                                    RelativeLayout.LayoutParams layoutParams =
                                            (RelativeLayout.LayoutParams) second.getLayoutParams();
                                    layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
                                    second.setLayoutParams(layoutParams);

                                    mContentView.findViewById(R.id.topvideo_thumbnail1_container).setVisibility(View.VISIBLE);
                                    fadeInView(mContentView.findViewById(R.id.topvideo_thumbnail1_container));

                                    fadeOutView(mContentView.findViewById(R.id.topvideo_thumbnail2_container));
                                    mContentView.findViewById(R.id.topvideo_thumbnail2_container).setVisibility(View.GONE);
                                } else if (videoObj.size() >= 3) {
                                    populateLargeVideoDrawee();
                                    populateSmallVideoDrawee1();
                                    populateSmallVideoDrawee2();

                                    mContentView.findViewById(R.id.topvideo_thumbnail1_container).setVisibility(View.VISIBLE);
                                    fadeInView(mContentView.findViewById(R.id.topvideo_thumbnail1_container));

                                    mContentView.findViewById(R.id.topvideo_thumbnail2_container).setVisibility(View.VISIBLE);
                                    fadeInView(mContentView.findViewById(R.id.topvideo_thumbnail2_container));

                                }
                            } else {
                                fadeInView(mContentView.findViewById(R.id.top_video_error_tv));
                                fadeOutView(mContentView.findViewById(R.id.top_video_content_container));
                                mContentView.findViewById(R.id.top_video_content_container).setVisibility(View.GONE);
                                fadeOutView(mContentView.findViewById(R.id.topvideo_thumbnail1_container));
                                mContentView.findViewById(R.id.topvideo_thumbnail1_container).setVisibility(View.GONE);
                                fadeOutView(mContentView.findViewById(R.id.topvideo_thumbnail2_container));
                                mContentView.findViewById(R.id.topvideo_thumbnail2_container).setVisibility(View.GONE);
                            }
                        }
                    }
                });
            }
        } else {
            String json = settings.getString("rec_video_data", "");
            Gson gson = new GsonBuilder().create();
            JsonParser parser = new JsonParser();
            JsonArray jArray = parser.parse(json).getAsJsonArray();

            videoObj = new ArrayList<Video>();
            for (JsonElement obj : jArray) {
                Video temp = gson.fromJson(obj, Video.class);
                videoObj.add(temp);
            }

            if (videoObj.size() > 0) {
                doBackgroundVideoGet();
                if (videoObj.size() == 1) {
                    populateLargeVideoDrawee();

                    fadeOutView(mContentView.findViewById(R.id.topvideo_thumbnail1_container));
                    mContentView.findViewById(R.id.topvideo_thumbnail1_container).setVisibility(View.GONE);
                    fadeOutView(mContentView.findViewById(R.id.topvideo_thumbnail2_container));
                    mContentView.findViewById(R.id.topvideo_thumbnail2_container).setVisibility(View.GONE);
                } else if (videoObj.size() == 2) {
                    populateLargeVideoDrawee();
                    populateSmallVideoDrawee1();

                    //center 2
                    View second = mContentView.findViewById(R.id.topvideo_thumbnail2_container);
                    RelativeLayout.LayoutParams layoutParams =
                            (RelativeLayout.LayoutParams) second.getLayoutParams();
                    layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
                    second.setLayoutParams(layoutParams);

                    mContentView.findViewById(R.id.topvideo_thumbnail1_container).setVisibility(View.VISIBLE);
                    fadeInView(mContentView.findViewById(R.id.topvideo_thumbnail1_container));

                    fadeOutView(mContentView.findViewById(R.id.topvideo_thumbnail2_container));
                    mContentView.findViewById(R.id.topvideo_thumbnail2_container).setVisibility(View.GONE);
                } else if (videoObj.size() >= 3) {
                    populateLargeVideoDrawee();
                    populateSmallVideoDrawee1();
                    populateSmallVideoDrawee2();

                    mContentView.findViewById(R.id.topvideo_thumbnail1_container).setVisibility(View.VISIBLE);
                    fadeInView(mContentView.findViewById(R.id.topvideo_thumbnail1_container));

                    mContentView.findViewById(R.id.topvideo_thumbnail2_container).setVisibility(View.VISIBLE);
                    fadeInView(mContentView.findViewById(R.id.topvideo_thumbnail2_container));

                }
            } else {
                fadeInView(mContentView.findViewById(R.id.top_video_error_tv));
                fadeOutView(mContentView.findViewById(R.id.top_video_content_container));
                mContentView.findViewById(R.id.top_video_content_container).setVisibility(View.GONE);
                fadeOutView(mContentView.findViewById(R.id.topvideo_thumbnail1_container));
                mContentView.findViewById(R.id.topvideo_thumbnail1_container).setVisibility(View.GONE);
                fadeOutView(mContentView.findViewById(R.id.topvideo_thumbnail2_container));
                mContentView.findViewById(R.id.topvideo_thumbnail2_container).setVisibility(View.GONE);
            }
        }
    }

    private void populateLargeVideoDrawee() {
        RelativeLayout container = (RelativeLayout) mContentView.findViewById(R.id.top_video_content_container);
        SimpleDraweeView topVideoLargeDrawee = (SimpleDraweeView) mContentView.findViewById(R.id.video_thumb_image);
        TextView topVideoTitle = (TextView) mContentView.findViewById(R.id.featured_video_title);
        TextView topVideoAuthor = (TextView) mContentView.findViewById(R.id.featured_video_author);
        TextView topVideoPublishDate = (TextView) mContentView.findViewById(R.id.featured_video_publishdate);
        TextView topVideoDownloadCount = (TextView) mContentView.findViewById(R.id.featured_video_downloadcount);
        ProgressBarDrawable pgd = new ProgressBarDrawable();

        pgd.setColor(Color.parseColor("#80FFFFFF"));

        /*MaterialProgressBar pgd = new MaterialProgressBar(getActivity().getApplicationContext());
        //pgd.setIndeterminateDrawable(new IndeterminateProgressDrawable(getActivity().getApplicationContext()));*/

        topVideoTitle.setText(videoObj.get(0).getVideo_name());
        //topVideoAuthor.setText("Top Model Indonesia");
        topVideoAuthor.setText(getStringRes(R.string.by) + " " + "Top Model Indonesia");
        topVideoPublishDate.setText(videoObj.get(0).getPublishDate());
        //topVideoDownloadCount.setText(videoObj.get(0).getTotal_download() + " Downloads");
        topVideoDownloadCount.setText(videoObj.get(0).getTotal_download() + " " + getStringRes(R.string.download_count));


        //Log.d("FacebookID", ((MainActivity)getActivity()).getFacebookProfile().getId());
        //Log.d("facebookurl", global.getFacebook_profile_pic_url());

        /*try {
            topVideoLargeDrawee.setImageURI
                    //(Uri.parse("http://210.5.41.103/CmsAdminPanel/" + videoObj.get(0).getThumbnail_url()));
                    //(Uri.parse("http://graph.facebook.com/"+((MainActivity)getActivity()).getFacebookProfile().getId()+"/picture?type=large"));
                    (Uri.parse(global.getFacebook_profile_pic_url()));
        } catch (NullPointerException e) {
            e.printStackTrace();
        }*/

        topVideoLargeDrawee.setImageURI
                (Uri.parse("http://210.5.41.103/CmsAdminPanel/" + videoObj.get(0).getThumbnail_url()));

        topVideoLargeDrawee.getHierarchy().setProgressBarImage(pgd);

        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle arg = new Bundle();
                arg.putString("content_id", videoObj.get(0).getContent_id());
                arg.putString("artist_name", videoObj.get(0).getArtist_name());
                arg.putString("video_name", videoObj.get(0).getVideo_name());
                arg.putString("video_url", videoObj.get(0).getVideo_url());
                arg.putString("video_description", Html.fromHtml(videoObj.get(0).getVideo_description()).toString().trim());
                arg.putString("publishdate", videoObj.get(0).getPublishDate());
                arg.putString("total_download", videoObj.get(0).getTotal_download());
                arg.putString("total_count", videoObj.get(0).getTotal_count());
                arg.putString("thumbnail", videoObj.get(0).getThumbnail_url());
                arg.putString("price", videoObj.get(0).getPrice());

                changeFragment(new VideoDetailFragment(), "videodetail", arg);
            }
        });

        crossFadeViews(mContentView.findViewById(R.id.top_video_progress),
                mContentView.findViewById(R.id.top_video_content_container));

    }

    private void populateSmallVideoDrawee1() {
        RelativeLayout container = (RelativeLayout) mContentView.findViewById(R.id.topvideo_thumbnail1_container);
        SimpleDraweeView thumbnail = (SimpleDraweeView) mContentView.findViewById(R.id.topvideo_thumbnail1_image);
        TextView title = (TextView) mContentView.findViewById(R.id.topvideo_thumbnail1_title);
        TextView downloadCount = (TextView) mContentView.findViewById(R.id.topvideo_thumbnail1_downloads);
        ProgressBarDrawable pgd = new ProgressBarDrawable();

        pgd.setColor(Color.parseColor("#80FFFFFF"));

        thumbnail.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/"
                + videoObj.get(1).getThumbnail_url()));
        thumbnail.getHierarchy().setProgressBarImage(pgd);
        title.setText(videoObj.get(1).getVideo_name());
        //downloadCount.setText(videoObj.get(1).getTotal_download() + " Downloads");
        downloadCount.setText(videoObj.get(1).getTotal_download() + " " + getStringRes(R.string.download_count));

        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle arg = new Bundle();
                arg.putString("content_id", videoObj.get(1).getContent_id());
                arg.putString("artist_name", videoObj.get(1).getArtist_name());
                arg.putString("video_name", videoObj.get(1).getVideo_name());
                arg.putString("video_url", videoObj.get(1).getVideo_url());
                arg.putString("video_description", Html.fromHtml(videoObj.get(1).getVideo_description()).toString().trim());
                arg.putString("publishdate", videoObj.get(1).getPublishDate());
                arg.putString("total_download", videoObj.get(1).getTotal_download());
                arg.putString("total_count", videoObj.get(1).getTotal_count());
                arg.putString("thumbnail", videoObj.get(1).getThumbnail_url());
                arg.putString("price", videoObj.get(1).getPrice());

                changeFragment(new VideoDetailFragment(), "videodetail", arg);
            }
        });

    }

    private void populateSmallVideoDrawee2() {
        RelativeLayout container = (RelativeLayout) mContentView.findViewById(R.id.topvideo_thumbnail2_container);
        SimpleDraweeView thumbnail = (SimpleDraweeView) mContentView.findViewById(R.id.topvideo_thumbnail2_image);
        TextView title = (TextView) mContentView.findViewById(R.id.topvideo_thumbnail2_title);
        TextView downloadCount = (TextView) mContentView.findViewById(R.id.topvideo_thumbnail2_downloads);
        ProgressBarDrawable pgd = new ProgressBarDrawable();

        pgd.setColor(Color.parseColor("#80FFFFFF"));

        thumbnail.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/"
                + videoObj.get(2).getThumbnail_url()));
        thumbnail.getHierarchy().setProgressBarImage(pgd);
        title.setText(videoObj.get(2).getVideo_name());
        //downloadCount.setText(videoObj.get(2).getTotal_download() + " Downloads");
        downloadCount.setText(videoObj.get(2).getTotal_download() + " " + getStringRes(R.string.download_count));

        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle arg = new Bundle();
                arg.putString("content_id", videoObj.get(2).getContent_id());
                arg.putString("artist_name", videoObj.get(2).getArtist_name());
                arg.putString("video_name", videoObj.get(2).getVideo_name());
                arg.putString("video_url", videoObj.get(2).getVideo_url());
                arg.putString("video_description", Html.fromHtml(videoObj.get(2).getVideo_description()).toString().trim());
                arg.putString("publishdate", videoObj.get(2).getPublishDate());
                arg.putString("total_download", videoObj.get(2).getTotal_download());
                arg.putString("total_count", videoObj.get(2).getTotal_count());
                arg.putString("thumbnail", videoObj.get(2).getThumbnail_url());
                arg.putString("price", videoObj.get(2).getPrice());

                changeFragment(new VideoDetailFragment(), "videodetail", arg);
            }
        });
    }

    private void getRecommendedWallpapers() {
        SharedPreferences settings = getActivity().getSharedPreferences("sharedpref", 0);

        if (settings.getString("rec_wallpaper_data", "").equalsIgnoreCase("")) {
            if (!isNetworkAvailable()) {
                crossFadeViews(mContentView.findViewById(R.id.topwallpaper_progress),
                        mContentView.findViewById(R.id.topwallpaper_error_tv));
                TextView error_tv = (TextView) mContentView.findViewById(R.id.topwallpaper_error_tv);
                error_tv.setText(getStringRes(R.string.error_no_internet));
            } else {
                client.get("http://210.5.41.103/tuneapp/api/FeaturedWallpaper/top%20model%20indo/3", new TextHttpResponseHandler() {
                /*@Override
                public void onProgress(long bytesWritten, long totalSize) {
                    super.onProgress(bytesWritten, totalSize);
                    ProgressWheel progress = (ProgressWheel) mContentView.findViewById(R.id.topwallpaper_progress);
                    progress.setProgress(bytesWritten/totalSize);
                }*/

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        if (running) {
                            crossFadeViews(mContentView.findViewById(R.id.topwallpaper_progress),
                                    mContentView.findViewById(R.id.topwallpaper_error_tv));
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        if (running) {
                            Gson gson = new GsonBuilder().create();
                            JsonParser parser = new JsonParser();
                            JsonArray jArray = parser.parse(responseString).getAsJsonArray();

                            wallpaperObj = new ArrayList<Wallpaper>();
                            for (JsonElement obj : jArray) {
                                Wallpaper temp = gson.fromJson(obj, Wallpaper.class);
                                wallpaperObj.add(temp);
                            }

                            if (wallpaperObj.size() > 0) {

                                if (wallpaperObj.size() == 1) {
                                    populateSmallWallpaper1();
                                    //center1
                                    View first = mContentView.findViewById(R.id.topwallpaper_container1);
                                    RelativeLayout.LayoutParams layoutParams =
                                            (RelativeLayout.LayoutParams) first.getLayoutParams();
                                    layoutParams.removeRule(RelativeLayout.ALIGN_PARENT_LEFT);
                                    layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
                                    first.setLayoutParams(layoutParams);

                                    fadeOutView(mContentView.findViewById(R.id.topwallpaper_container2));
                                    mContentView.findViewById(R.id.topwallpaper_container2).setVisibility(View.GONE);
                                    fadeOutView(mContentView.findViewById(R.id.topwallpaper_container3));
                                    mContentView.findViewById(R.id.topwallpaper_container3).setVisibility(View.GONE);
                                } else if (wallpaperObj.size() == 2) {
                                    populateSmallWallpaper1();
                                    populateSmallWallpaper2();

                                    //center - add left right padding
                                    View container = mContentView.findViewById(R.id.topwallpaper_content_container);
                                    container.setPadding(convertToDp(40), 0, convertToDp(40), 0);

                                    fadeOutView(mContentView.findViewById(R.id.topwallpaper_container2));
                                    mContentView.findViewById(R.id.topwallpaper_container2).setVisibility(View.GONE);
                                } else if (wallpaperObj.size() >= 3) {
                                    populateSmallWallpaper1();
                                    populateSmallWallpaper2();
                                    populateSmallWallpaper3();

                                }
                                crossFadeViews(mContentView.findViewById(R.id.topwallpaper_progress),
                                        mContentView.findViewById(R.id.topwallpaper_content_container));
                            }
                        }
                    }
                });
            }
        } else {
            String json = settings.getString("rec_wallpaper_data", "");
            Gson gson = new GsonBuilder().create();
            JsonParser parser = new JsonParser();
            JsonArray jArray = parser.parse(json).getAsJsonArray();

            wallpaperObj = new ArrayList<Wallpaper>();
            for (JsonElement obj : jArray) {
                Wallpaper temp = gson.fromJson(obj, Wallpaper.class);
                wallpaperObj.add(temp);
            }

            if (wallpaperObj.size() > 0) {

                if (wallpaperObj.size() == 1) {
                    populateSmallWallpaper1();
                    //center1
                    View first = mContentView.findViewById(R.id.topwallpaper_container1);
                    RelativeLayout.LayoutParams layoutParams =
                            (RelativeLayout.LayoutParams) first.getLayoutParams();
                    layoutParams.removeRule(RelativeLayout.ALIGN_PARENT_LEFT);
                    layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
                    first.setLayoutParams(layoutParams);

                    fadeOutView(mContentView.findViewById(R.id.topwallpaper_container2));
                    mContentView.findViewById(R.id.topwallpaper_container2).setVisibility(View.GONE);
                    fadeOutView(mContentView.findViewById(R.id.topwallpaper_container3));
                    mContentView.findViewById(R.id.topwallpaper_container3).setVisibility(View.GONE);
                } else if (wallpaperObj.size() == 2) {
                    populateSmallWallpaper1();
                    populateSmallWallpaper2();

                    //center - add left right padding
                    View container = mContentView.findViewById(R.id.topwallpaper_content_container);
                    container.setPadding(convertToDp(40), 0, convertToDp(40), 0);

                    fadeOutView(mContentView.findViewById(R.id.topwallpaper_container2));
                    mContentView.findViewById(R.id.topwallpaper_container2).setVisibility(View.GONE);
                } else if (wallpaperObj.size() >= 3) {
                    populateSmallWallpaper1();
                    populateSmallWallpaper2();
                    populateSmallWallpaper3();

                }
                crossFadeViews(mContentView.findViewById(R.id.topwallpaper_progress),
                        mContentView.findViewById(R.id.topwallpaper_content_container));
            }
        }
    }

    private void populateSmallWallpaper1() {
        RelativeLayout container = (RelativeLayout) mContentView.findViewById(R.id.topwallpaper_container1);
        SimpleDraweeView thumbnail = (SimpleDraweeView) mContentView.findViewById(R.id.topwallpaper_image1);
        TextView title = (TextView) mContentView.findViewById(R.id.topwallpaper_title1);
        TextView downloadCount = (TextView) mContentView.findViewById(R.id.topwallpaper_download1);
        ProgressBarDrawable pgd = new ProgressBarDrawable();

        pgd.setColor(Color.parseColor("#80FFFFFF"));

        if (isPurchased(wallpaperObj.get(0).getContent_id())) {
            thumbnail.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/"
                    + wallpaperObj.get(0).getWallpaper_url()));
        } else {
            /*thumbnail.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/"
                    + wallpaperObj.get(0).getThumbnail()));*/
            thumbnail.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/"
                    + wallpaperObj.get(0).getWallpaper_url()));
            setWaterMark(thumbnail, false);

        }
        thumbnail.getHierarchy().setProgressBarImage(pgd);
        title.setText(wallpaperObj.get(0).getWallpaper_name());
        //downloadCount.setText(wallpaperObj.get(0).getTotal_download() + " Downloads");
        downloadCount.setText(wallpaperObj.get(0).getTotal_download() + " " + getStringRes(R.string.download_count));

        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle arg = new Bundle();
                arg.putString("content_id", wallpaperObj.get(0).getContent_id());
                arg.putString("artist_name", wallpaperObj.get(0).getArtist_name());
                arg.putString("wallpaper_name", wallpaperObj.get(0).getWallpaper_name());
                arg.putString("wallpaper_url", wallpaperObj.get(0).getWallpaper_url());
                arg.putString("wallpaper_description", Html.fromHtml(wallpaperObj.get(0).getWallpaper_description()).toString().trim());
                arg.putString("publishdate", wallpaperObj.get(0).getPublishDate());
                arg.putString("total_download", wallpaperObj.get(0).getTotal_download());
                arg.putString("total_count", wallpaperObj.get(0).getTotal_count());
                arg.putString("price", wallpaperObj.get(0).getPrice());
                arg.putString("thumbnail_url", wallpaperObj.get(0).getThumbnail());

                changeFragment(new WallpaperDetailFragment(), "wallpaperdetail", arg);
            }
        });
    }

    private void populateSmallWallpaper2() {
        RelativeLayout container = (RelativeLayout) mContentView.findViewById(R.id.topwallpaper_container2);
        SimpleDraweeView thumbnail = (SimpleDraweeView) mContentView.findViewById(R.id.topwallpaper_image2);
        TextView title = (TextView) mContentView.findViewById(R.id.topwallpaper_title2);
        TextView downloadCount = (TextView) mContentView.findViewById(R.id.topwallpaper_download2);
        ProgressBarDrawable pgd = new ProgressBarDrawable();

        pgd.setColor(Color.parseColor("#80FFFFFF"));

        if (isPurchased(wallpaperObj.get(1).getContent_id())) {
            thumbnail.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/"
                    + wallpaperObj.get(1).getWallpaper_url()));
        } else {
            /*thumbnail.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/"
                    + wallpaperObj.get(1).getThumbnail()));*/
            thumbnail.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/"
                    + wallpaperObj.get(1).getWallpaper_url()));
            setWaterMark(thumbnail, false);

        }
        thumbnail.getHierarchy().setProgressBarImage(pgd);
        title.setText(wallpaperObj.get(1).getWallpaper_name());
        //downloadCount.setText(wallpaperObj.get(1).getTotal_download()+ " Downloads");
        downloadCount.setText(wallpaperObj.get(1).getTotal_download() + " " + getStringRes(R.string.download_count));

        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle arg = new Bundle();
                arg.putString("content_id", wallpaperObj.get(1).getContent_id());
                arg.putString("artist_name", wallpaperObj.get(1).getArtist_name());
                arg.putString("wallpaper_name", wallpaperObj.get(1).getWallpaper_name());
                arg.putString("wallpaper_url", wallpaperObj.get(1).getWallpaper_url());
                arg.putString("wallpaper_description", Html.fromHtml(wallpaperObj.get(1).getWallpaper_description()).toString().trim());
                arg.putString("publishdate", wallpaperObj.get(1).getPublishDate());
                arg.putString("total_download", wallpaperObj.get(1).getTotal_download());
                arg.putString("total_count", wallpaperObj.get(1).getTotal_count());
                arg.putString("price", wallpaperObj.get(1).getPrice());
                arg.putString("thumbnail_url", wallpaperObj.get(1).getThumbnail());

                changeFragment(new WallpaperDetailFragment(), "wallpaperdetail", arg);
            }
        });
    }

    private void populateSmallWallpaper3() {
        RelativeLayout container = (RelativeLayout) mContentView.findViewById(R.id.topwallpaper_container3);
        SimpleDraweeView thumbnail = (SimpleDraweeView) mContentView.findViewById(R.id.topwallpaper_image3);
        TextView title = (TextView) mContentView.findViewById(R.id.topwallpaper_title3);
        TextView downloadCount = (TextView) mContentView.findViewById(R.id.topwallpaper_download3);
        ProgressBarDrawable pgd = new ProgressBarDrawable();

        pgd.setColor(Color.parseColor("#80FFFFFF"));

        if (isPurchased(wallpaperObj.get(2).getContent_id())) {
            thumbnail.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/"
                    + wallpaperObj.get(2).getWallpaper_url()));
        } else {
            /*thumbnail.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/"
                    + wallpaperObj.get(2).getThumbnail()));*/
            thumbnail.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/"
                    + wallpaperObj.get(2).getWallpaper_url()));
            setWaterMark(thumbnail, false);
        }
        thumbnail.getHierarchy().setProgressBarImage(pgd);
        title.setText(wallpaperObj.get(2).getWallpaper_name());
        //downloadCount.setText(wallpaperObj.get(2).getTotal_download()+ " Downloads");
        downloadCount.setText(wallpaperObj.get(2).getTotal_download() + " " + getStringRes(R.string.download_count));

        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle arg = new Bundle();
                arg.putString("content_id", wallpaperObj.get(2).getContent_id());
                arg.putString("artist_name", wallpaperObj.get(2).getArtist_name());
                arg.putString("wallpaper_name", wallpaperObj.get(2).getWallpaper_name());
                arg.putString("wallpaper_url", wallpaperObj.get(2).getWallpaper_url());
                arg.putString("wallpaper_description", Html.fromHtml(wallpaperObj.get(2).getWallpaper_description()).toString().trim());
                arg.putString("publishdate", wallpaperObj.get(2).getPublishDate());
                arg.putString("total_download", wallpaperObj.get(2).getTotal_download());
                arg.putString("total_count", wallpaperObj.get(2).getTotal_count());
                arg.putString("price", wallpaperObj.get(2).getPrice());
                arg.putString("thumbnail_url", wallpaperObj.get(2).getThumbnail());

                changeFragment(new WallpaperDetailFragment(), "wallpaperdetail", arg);
            }
        });
    }

    public void initSliderLayout(View view) {
        sliderLayout = (SliderLayout) view.findViewById(R.id.slider_layout);
        final DefaultSliderView sliderView1 = new DefaultSliderView(getActivity().getApplicationContext());
        final DefaultSliderView sliderView2 = new DefaultSliderView(getActivity().getApplicationContext());
        final DefaultSliderView sliderView3 = new DefaultSliderView(getActivity().getApplicationContext());
        final DefaultSliderView sliderView4 = new DefaultSliderView(getActivity().getApplicationContext());
        final DefaultSliderView sliderView5 = new DefaultSliderView(getActivity().getApplicationContext());
        final DefaultSliderView sliderView6 = new DefaultSliderView(getActivity().getApplicationContext());

        sliderView1.image(R.drawable.header_01).setScaleType(BaseSliderView.ScaleType.CenterCrop);
        //sliderView1.image("https://pbs.twimg.com/profile_images/638751551457103872/KN-NzuRl.png").setScaleType(BaseSliderView.ScaleType.CenterCrop);
        sliderView2.image(R.drawable.crazy_chicken_banner).setScaleType(BaseSliderView.ScaleType.CenterCrop);
        sliderView6.image(R.drawable.header_add).setScaleType(BaseSliderView.ScaleType.CenterCrop);
//        sliderView3.image(R.drawable.resque_your_friend_banner).setScaleType(BaseSliderView.ScaleType.CenterCrop);
//        sliderView4.image(R.drawable.header_04).setScaleType(BaseSliderView.ScaleType.CenterCrop);
//        sliderView5.image(R.drawable.header_05).setScaleType(BaseSliderView.ScaleType.CenterCrop);

        sliderView2.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
            @Override
            public void onSliderClick(BaseSliderView slider) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.gamvento.crazychicken")));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.gamvento.crazychicken")));
                }
            }
        });

        sliderView6.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
            @Override
            public void onSliderClick(BaseSliderView slider) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.forest.zombieattack")));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.forest.zombieattack")));
                }
            }
        });

        /*sliderView3.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
            @Override
            public void onSliderClick(BaseSliderView slider) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.gamvento.crazychicken")));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.gamvento.crazychicken")));
                }
            }
        });*/

        sliderLayout.addSlider(sliderView1);
        sliderLayout.addSlider(sliderView2);
        /*sliderLayout.addSlider(sliderView3);
        sliderLayout.addSlider(sliderView4);
        sliderLayout.addSlider(sliderView5);*/
        sliderLayout.addSlider(sliderView6);
        sliderLayout.setDuration(3000);
    }

    public void initDrawees(View view) {

        /*topVideoLargeDrawee.setImageURI(Uri.parse("http://www.planwallpaper.com/static/images/i-should-buy-a-boat.jpg"));*/

        //testDrawee.getHierarchy().setControllerOverlay(getResources().getDrawable(R.drawable.overlay_landscape));

        /*ControllerListener controllerListener = new BaseControllerListener<ImageInfo>(){
            @Override
            public void onFinalImageSet(
                    String id,
                    @Nullable ImageInfo imageInfo,
                    @Nullable Animatable anim) {
                *//*if (imageInfo == null) {
                    return;
                }
                QualityInfo qualityInfo = imageInfo.getQualityInfo();
                FLog.d("Final image received! " +
                                "Size %d x %d",
                        "Quality level %d, good enough: %s, full quality: %s",
                        imageInfo.getWidth(),
                        imageInfo.getHeight(),
                        qualityInfo.getQuality(),
                        qualityInfo.isOfGoodEnoughQuality(),
                        qualityInfo.isOfFullQuality());*//*
                topVideoLargeDrawee.getHierarchy().setControllerOverlay(getResources().getDrawable(R.drawable.overlay_landscape));
            }

            @Override
            public void onIntermediateImageSet(String id, @Nullable ImageInfo imageInfo) {
                //FLog.d("Intermediate image received");
            }

            @Override
            public void onFailure(String id, Throwable throwable) {
                //FLog.e(getClass(), throwable, "Error loading %s", id)
            }
        };

        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setControllerListener(controllerListener)
                .setUri(Uri.parse("http://www.planwallpaper.com/static/images/i-should-buy-a-boat.jpg"))
                .build();
        topVideoLargeDrawee.setController(controller);
        ProgressBarDrawable pgd = new ProgressBarDrawable();
        //pgd.setAlpha(50);
        pgd.setColor(Color.parseColor("#80FFFFFF"));
        topVideoLargeDrawee.getHierarchy().setProgressBarImage(pgd);*/

        //testDrawee.getHierarchy().setProgressBarImage(new CircularProgressBarDrawable(getActivity().getApplicationContext()));

        /*ImageRequest request = ImageRequestBuilder
                .newBuilderWithSource
                        (Uri.parse("http://wallpaper-nature.com/images/wallpapers/House-On-The-Hill-Wallpaper-567313.jpeg"))
                .setProgressiveRenderingEnabled(true)
                .build();*/

        /*ControllerListener listener = new BaseControllerListener();
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setUri(Uri.parse("http://www.planwallpaper.com/static/images/i-should-buy-a-boat.jpg"))
                //.setImageRequest(request)
                .setOldController(testDrawee.getController())
                .setControllerListener(listener)
                .setTapToRetryEnabled(true)
                .build();
        testDrawee.setController(controller);*/

        //testDrawee.getHierarchy().setProgressBarImage(new CircularProgressBarDrawable(getActivity().getApplicationContext()));
        //testDrawee.getHierarchy().setProgressBarImage(new CircularProgressBarDrawable());
        //testDrawee.getHierarchy().setProgressBarImage(new ProgressBarDrawable());


        final SimpleDraweeView topVideoLargeDrawee = (SimpleDraweeView) view.findViewById(R.id.video_thumb_image);
        SimpleDraweeView tvThumb1 = (SimpleDraweeView) view.findViewById(R.id.topvideo_thumbnail1_image);
        SimpleDraweeView tvThumb2 = (SimpleDraweeView) view.findViewById(R.id.topvideo_thumbnail2_image);
        SimpleDraweeView twThumb1 = (SimpleDraweeView) view.findViewById(R.id.topwallpaper_image1);
        SimpleDraweeView twThumb2 = (SimpleDraweeView) view.findViewById(R.id.topwallpaper_image2);
        SimpleDraweeView twThumb3 = (SimpleDraweeView) view.findViewById(R.id.topwallpaper_image3);

        ProgressBarDrawable pgd = new ProgressBarDrawable();
        //pgd.setAlpha(50);
        pgd.setColor(Color.parseColor("#80FFFFFF"));

        topVideoLargeDrawee.setImageURI(Uri.parse("http://developer.android.com/images/activity_fragment_lifecycle.png"));
        topVideoLargeDrawee.getHierarchy().setProgressBarImage(pgd);

        /*tvThumb1.setController(Fresco.newDraweeControllerBuilder()
                .setUri(Uri.parse("res:///" + R.drawable.image_video_small))
                .build());
        tvThumb2.setController( Fresco.newDraweeControllerBuilder()
                .setUri(Uri.parse("res:///"+R.drawable.image_video_small))
                .build());
        twThumb1.setController(Fresco.newDraweeControllerBuilder()
                .setUri(Uri.parse("res:///"+R.drawable.image_wallpaper))
                .build());
        twThumb2.setController(Fresco.newDraweeControllerBuilder()
                .setUri(Uri.parse("res:///"+R.drawable.image_wallpaper))
                .build());
        twThumb3.setController(Fresco.newDraweeControllerBuilder()
                .setUri(Uri.parse("res:///" + R.drawable.image_wallpaper))
                .build());*/


    }

    public void initOnClickListeners(View view) {
        view.findViewById(R.id.topvideo_large_container).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragment(new VideoFragment(), "video");
            }
        });

        view.findViewById(R.id.topwallpaper_header).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragment(new WallpaperFragment(), "wallpaper");
            }
        });

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContentView = inflater.inflate(R.layout.fragment_home, null);

        bottomBanner2 = (ImageView) mContentView.findViewById(R.id.bottom_banner2);
        bottomBanner3 = (ImageView) mContentView.findViewById(R.id.bottom_banner3);

        bottomBanner2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.forest.zombieattack")));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.forest.zombieattack")));
                }
            }
        });

        bottomBanner3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.gamvento.crazychicken")));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.gamvento.crazychicken")));
                }
            }
        });

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public void setUpViews() {

        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        int windowWidthPix = getActivity().getApplicationContext().getResources().getDisplayMetrics().widthPixels;

//        ImageView banner1 = (ImageView) mContentView.findViewById(R.id.banner_01);
//        bottomBanner = (ImageView) mContentView.findViewById(R.id.bottom_banner);
        /*LinearLayout.LayoutParams bannerParams = (LinearLayout.LayoutParams) bottomBanner.getLayoutParams();
        bannerParams.width = convertToDp(windowWidthPix);
        bannerParams.height = (int) (convertToDp(windowWidthPix) / 6.67);*/
//        banner1.setLayoutParams(bannerParams);

//        bottomBanner.setLayoutParams(bannerParams);
        /*bottomBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.gamvento.crazychicken")));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.gamvento.crazychicken")));
                }
            }
        });*/
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        global = (TopModelGlobal) getActivity().getApplicationContext();
        running = true;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Setup content view
        setContentView(mContentView);
        // Setup text for empty content
        setEmptyText("");
        setUpViews();
        setToolBarTitle("");
        obtainData();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mHandler.removeCallbacks(mShowContentRunnable);
        try {
            client.cancelAllRequests(true);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        running = false;
    }

    /*@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.refresh, menu);
    }*/

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_refresh:
                obtainData();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }*/

    private void obtainData() {
        // Show indeterminate progress
        setContentShown(false);

        mHandler = new Handler();
        mHandler.postDelayed(mShowContentRunnable, 1000);
    }

    private void doBackgroundVideoGet() {
        final SharedPreferences settings = getActivity().getSharedPreferences("sharedpref", 0);
        if (isNetworkAvailable()) {
            client.get("http://210.5.41.103/tuneapp/api/FeaturedVideo/top%20model%20indo/3", new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    if (running) {
                        crossFadeViews(mContentView.findViewById(R.id.top_video_progress),
                                mContentView.findViewById(R.id.top_video_error_tv));
                        mContentView.findViewById(R.id.topvideo_thumbnail1_container).setVisibility(View.GONE);
                        mContentView.findViewById(R.id.topvideo_thumbnail2_container).setVisibility(View.GONE);
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    if (running) {
                        Gson gson = new GsonBuilder().create();
                        JsonParser parser = new JsonParser();
                        JsonArray jArray = parser.parse(responseString).getAsJsonArray();

                        settings.edit().putString("rec_video_data", responseString).commit();

                        videoObj = new ArrayList<Video>();
                        for (JsonElement obj : jArray) {
                            Video temp = gson.fromJson(obj, Video.class);
                            videoObj.add(temp);
                        }

                        if (videoObj.size() > 0) {
                            if (videoObj.size() == 1) {
                                populateLargeVideoDrawee();

                                fadeOutView(mContentView.findViewById(R.id.topvideo_thumbnail1_container));
                                mContentView.findViewById(R.id.topvideo_thumbnail1_container).setVisibility(View.GONE);
                                fadeOutView(mContentView.findViewById(R.id.topvideo_thumbnail2_container));
                                mContentView.findViewById(R.id.topvideo_thumbnail2_container).setVisibility(View.GONE);
                            } else if (videoObj.size() == 2) {
                                populateLargeVideoDrawee();
                                populateSmallVideoDrawee1();

                                //center 2
                                View second = mContentView.findViewById(R.id.topvideo_thumbnail2_container);
                                RelativeLayout.LayoutParams layoutParams =
                                        (RelativeLayout.LayoutParams) second.getLayoutParams();
                                layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
                                second.setLayoutParams(layoutParams);

                                mContentView.findViewById(R.id.topvideo_thumbnail1_container).setVisibility(View.VISIBLE);
                                fadeInView(mContentView.findViewById(R.id.topvideo_thumbnail1_container));

                                fadeOutView(mContentView.findViewById(R.id.topvideo_thumbnail2_container));
                                mContentView.findViewById(R.id.topvideo_thumbnail2_container).setVisibility(View.GONE);
                            } else if (videoObj.size() >= 3) {
                                populateLargeVideoDrawee();
                                populateSmallVideoDrawee1();
                                populateSmallVideoDrawee2();

                                mContentView.findViewById(R.id.topvideo_thumbnail1_container).setVisibility(View.VISIBLE);
                                fadeInView(mContentView.findViewById(R.id.topvideo_thumbnail1_container));

                                mContentView.findViewById(R.id.topvideo_thumbnail2_container).setVisibility(View.VISIBLE);
                                fadeInView(mContentView.findViewById(R.id.topvideo_thumbnail2_container));

                            }
                        }
                    }
                }
            });
        }
    }

    private void doBackgroundWallpaperGet() {

    }
}
