package com.forest_interactive.topmodelindo.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by abdulla.hainum on 1/3/2016.
 */
public class Comment {

    @SerializedName("fullname")
    String fullname;
    @SerializedName("datecreated")
    String date_created;
    @SerializedName("description")
    String description;
    @SerializedName("username")
    String username;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getDate_created() {
        //return date_created;
        return getDateOnly(date_created);
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }

    private String getDateOnly(String date){
        String[] dateSplit = date.split("T");
        return dateSplit[0];
    }


}
