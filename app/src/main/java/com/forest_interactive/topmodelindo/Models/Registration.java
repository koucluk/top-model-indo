package com.forest_interactive.topmodelindo.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by abdulla.hainum on 4/3/2016.
 */
public class Registration {
    @SerializedName("username")
    String username;
    @SerializedName("status")
    String status;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
