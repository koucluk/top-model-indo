package com.forest_interactive.topmodelindo.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by abdulla.hainum on 9/3/2016.
 */
public class PurchaseHistory {
    @SerializedName("date_purchase")
    String date_purchase;
    @SerializedName("media_name")
    String media_name;
    @SerializedName("content_type")
    String content_type;
    @SerializedName("price")
    String price;
    @SerializedName("content_id")
    String content_id;

    public String getContent_id() {
        return content_id;
    }

    public void setContent_id(String content_id) {
        this.content_id = content_id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getContent_type() {
        return content_type;
    }

    public void setContent_type(String content_type) {
        this.content_type = content_type;
    }

    public String getMedia_name() {
        return media_name;
    }

    public void setMedia_name(String media_name) {
        this.media_name = media_name;
    }

    public String getDate_purchase() {
        return date_purchase;
    }

    public void setDate_purchase(String date_purchase) {
        this.date_purchase = date_purchase;
    }


}
