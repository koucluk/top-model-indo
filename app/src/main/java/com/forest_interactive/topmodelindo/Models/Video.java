package com.forest_interactive.topmodelindo.Models;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by abdulla.hainum on 26/2/2016.
 */
public class Video {
    @SerializedName("content_id")
    String content_id;
    @SerializedName("artist_name")
    String artist_name;
    @SerializedName("video_name")
    String video_name;
    @SerializedName("video_url")
    String video_url;
    @SerializedName("video_description")
    String video_description;
    @SerializedName("publishdate")
    String publishDate;
    @SerializedName("total_download")
    String total_download;
    @SerializedName("total_count")
    String total_count;
    @SerializedName("thumbnail")
    String thumbnail_url;
    @SerializedName("price")
    String price;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getThumbnail_url() {
        if (thumbnail_url.contains(" ")){
            return encodeURL(thumbnail_url);
        } else {
            return thumbnail_url;
        }
    }

    public void setThumbnail_url(String thumbnail_url) {
        this.thumbnail_url = thumbnail_url;
    }

    public String getContent_id() {
        return content_id;
    }

    public void setContent_id(String content_id) {
        this.content_id = content_id;
    }

    public String getArtist_name() {
        return artist_name;
    }

    public void setArtist_name(String artist_name) {
        this.artist_name = artist_name;
    }

    public String getVideo_name() {
        return video_name;
    }

    public void setVideo_name(String video_name) {
        this.video_name = video_name;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getVideo_description() {
        return video_description;
    }

    public void setVideo_description(String video_description) {
        this.video_description = video_description;
    }

    public String getPublishDate() {
        //return publishDate;
        return getDateOnly(publishDate);
    }

    public String getPublishDateDifference (boolean isIndo){
        if (!isIndo) {
            return findTimeDifferenceEng(getPublishDateFormatted());
        } else
            return findTimeDifferenceIndo(getPublishDateFormatted());
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public String getTotal_download() {
        return total_download;
    }

    public void setTotal_download(String total_download) {
        this.total_download = total_download;
    }

    public String getTotal_count() {
        return total_count;
    }

    public void setTotal_count(String total_count) {
        this.total_count = total_count;
    }

    private String getDateOnly(String date){
        String[] dateSplit = date.split("T");
        return dateSplit[0];
    }

    private String encodeURL(String URL){
        return URL.replaceAll(" ", "%20");
    }

    public Date getPublishDateFormatted(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-ddHH:mm:ss");
        Date date = null;
        try {
            date = sdf.parse(getPublishDateActual());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    //Remove T
    private String getPublishDateActual(){
        //return publishDate;
        String workingdate = publishDate;
        int index = 10;
        StringBuilder sb = new StringBuilder(workingdate);
        sb.deleteCharAt(index);
        return sb.toString();
    }

    private String findTimeDifferenceEng(Date dt1) {
        Date currentTime = new Date();
        long diff = currentTime.getTime() - dt1.getTime();
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000);
        int diffInDays = (int) ((currentTime.getTime() - dt1.getTime()) / (1000 * 60 * 60 * 24));

        if (diffSeconds > 60){
            return diffMinutes + " minutes ago";
        }else if (diffMinutes > 60) {
            return diffHours + " hours ago";
        } else if (diffHours > 24) {
            return diffInDays + " days ago";
        } else {
            return diffSeconds + " seconds ago";
        }

    }

    private String findTimeDifferenceIndo(Date dt1) {
        Date currentTime = new Date();
        long diff = currentTime.getTime() - dt1.getTime();
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000);
        int diffInDays = (int) ((currentTime.getTime() - dt1.getTime()) / (1000 * 60 * 60 * 24));

        if (diffSeconds > 60){
            return diffMinutes + " indo minutes ago";
        }else if (diffMinutes > 60) {
            return diffHours + " indo hours ago";
        } else if (diffHours > 24) {
            return diffInDays + " indo days ago";
        } else {
            return diffSeconds + " indo seconds ago";
        }

    }

    public int getDownloadCountInt() {
        return Integer.parseInt(total_download);
    }

}
