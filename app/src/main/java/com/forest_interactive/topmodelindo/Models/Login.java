package com.forest_interactive.topmodelindo.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by abdulla.hainum on 4/3/2016.
 */
public class Login {

    @SerializedName("account_status")
    String account_status;

    @SerializedName("username")
    String username;

    @SerializedName("fullname")
    String fullname;

    @SerializedName("wallet")
    String wallet;

    @SerializedName("notification_status")
    String notification_status;

    public String getAccount_status() {
        return account_status;
    }

    public void setAccount_status(String account_status) {
        this.account_status = account_status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getWallet() {
        return wallet;
    }

    public void setWallet(String wallet) {
        this.wallet = wallet;
    }

    public String getNotification_status() {
        return notification_status;
    }

    public boolean getNotification_statusBool(){
        if (getNotification_status().equalsIgnoreCase("true")){
            return true;
        } else {
            return false;
        }
    }

    public void setNotification_status(String notification_status) {
        this.notification_status = notification_status;
    }


}
