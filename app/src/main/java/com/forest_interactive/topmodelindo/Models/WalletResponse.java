package com.forest_interactive.topmodelindo.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by abdulla.hainum on 22/3/2016.
 */
public class WalletResponse {
    @SerializedName("username")
    String username;
    @SerializedName("status")
    String status;
    @SerializedName("new_wallet")
    String new_wallet;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNew_wallet() {
        return new_wallet;
    }

    public void setNew_wallet(String new_wallet) {
        this.new_wallet = new_wallet;
    }
}
