package com.forest_interactive.topmodelindo.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by abdulla.hainum on 9/3/2016.
 */
public class PurchaseReturn {
    @SerializedName("username")
    String username;
    @SerializedName("status")
    String status;
    @SerializedName("contentid")
    String contentid;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getContentid() {
        return contentid;
    }

    public void setContentid(String contentid) {
        this.contentid = contentid;
    }
}
