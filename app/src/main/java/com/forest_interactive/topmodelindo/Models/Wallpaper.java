package com.forest_interactive.topmodelindo.Models;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by abdulla.hainum on 29/2/2016.
 */
public class Wallpaper {
    @SerializedName("content_id")
    String content_id;
    @SerializedName("artist_name")
    String artist_name;
    @SerializedName("wallpaper_name")
    String wallpaper_name;
    @SerializedName("wallpaper_url")
    String wallpaper_url;
    @SerializedName("wallpaper_description")
    String wallpaper_description;
    @SerializedName("publishdate")
    String publishDate;
    @SerializedName("total_download")
    String total_download;
    @SerializedName("total_count")
    String total_count;
    @SerializedName("price")
    String price;
    @SerializedName("thumbnail")
    String thumbnail;
    @SerializedName("category_name")
    String category_name;

    public String getThumbnail() {
        //return thumbnail;
        if (thumbnail.contains(" ")){
            return encodeURL(thumbnail);
        } else {
            return thumbnail;
        }
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTotal_count() {
        return total_count;
    }

    public void setTotal_count(String total_count) {
        this.total_count = total_count;
    }

    public String getTotal_download() {
        return total_download;
    }

    public void setTotal_download(String total_download) {
        this.total_download = total_download;
    }
    public String getPublishDate() {
        //return publishDate;
        return getDateOnly(publishDate);
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public String getWallpaper_description() {
        return wallpaper_description;
    }

    public void setWallpaper_description(String wallpaper_description) {
        this.wallpaper_description = wallpaper_description;
    }

    public String getWallpaper_url() {
        //return wallpaper_url;
        if (wallpaper_url.contains(" ")){
            return encodeURL(wallpaper_url);
        } else {
            return wallpaper_url;
        }
    }

    public void setWallpaper_url(String wallpaper_url) {
        this.wallpaper_url = wallpaper_url;
    }

    public String getWallpaper_name() {
        return wallpaper_name;
    }

    public void setWallpaper_name(String wallpaper_name) {
        this.wallpaper_name = wallpaper_name;
    }

    public String getContent_id() {
        return content_id;
    }

    public void setContent_id(String content_id) {
        this.content_id = content_id;
    }

    public String getArtist_name() {
        return artist_name;
    }

    public void setArtist_name(String artist_name) {
        this.artist_name = artist_name;
    }


    private String getDateOnly(String date){
        String[] dateSplit = date.split("T");
        return dateSplit[0];
    }

    private String encodeURL(String URL){
        return URL.replaceAll(" ", "%20");
    }

    public Date getPublishDateFormatted(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-ddHH:mm:ss");
        Date date = null;
        try {
            date = sdf.parse(getPublishDateActual());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    //Remove T
    private String getPublishDateActual(){
        //return publishDate;
        String workingdate = publishDate;
        int index = 10;
        StringBuilder sb = new StringBuilder(workingdate);
        sb.deleteCharAt(index);
        return sb.toString();
    }

    public int getDownloadCountInt() {
        return Integer.parseInt(total_download);
    }

}
