package com.forest_interactive.topmodelindo;

import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidadvance.topsnackbar.TSnackbar;
import com.bartoszlipinski.viewpropertyobjectanimator.ViewPropertyObjectAnimator;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.facebook.imagepipeline.listener.RequestListener;
import com.facebook.imagepipeline.listener.RequestLoggingListener;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.forest_interactive.aseandcb.AdcbHelper;
import com.forest_interactive.aseandcb.AseandcbResult;
import com.forest_interactive.topmodelindo.Models.Login;
import com.forest_interactive.topmodelindo.Models.PurchaseHistory;
import com.forest_interactive.topmodelindo.Models.WalletResponse;
import com.forest_interactive.topmodelindo.Utils.RegistrationIntentService;
import com.forest_interactive.topmodelindo.Widgets.DrawerArrowDrawable;
import com.forest_interactive.topmodelindo.util.IabHelper;
import com.forest_interactive.topmodelindo.util.IabResult;
import com.forest_interactive.topmodelindo.util.Inventory;
import com.forest_interactive.topmodelindo.util.Purchase;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.jirbo.adcolony.AdColony;
import com.jirbo.adcolony.AdColonyV4VCAd;
import com.jirbo.adcolony.AdColonyV4VCListener;
import com.jirbo.adcolony.AdColonyV4VCReward;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity implements AseandcbResult, GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks,
        EditProfileFragment.OnFragmentInteractionListener, LoginFragment.OnFragmentInteractionListener,
        MyProfileFragment.OnFragmentInteractionListener, VideoDetailFragment.OnFragmentInteractionListener,
        VideoFragment.OnFragmentInteractionListener, WallpaperDetailFragment.OnFragmentInteractionListener,
        WallpaperFragment.OnFragmentInteractionListener, MyVideoFragment.OnFragmentInteractionListener,
        MyWallpaperFragment.OnFragmentInteractionListener, CoinStoreFragment.OnFragmentInteractionListener,
        ContactUsFragment.OnFragmentInteractionListener, HelpAboutFragment.OnFragmentInteractionListener,
        RegisterFragment.OnFragmentInteractionListener, ChangePasswordFragment.OnFragmentInteractionListener, ForgotPassFragment.OnFragmentInteractionListener {

    private static final int RC_SIGN_IN = 9001;
    //Camera
    private static final int CAMERA_REQUEST = 1888;
    public float scale;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    boolean expanded = false;
    int searchBarWidth;
    AdcbHelper adcbHelper;
    AsyncHttpClient client;
    ArrayList<WalletResponse> walletResponse;
    ArrayList<Login> loginObj;
    Locale myLocale;
    //Google
    GoogleApiClient mGoogleApiClient;
    //Facebook
    CallbackManager callbackManager;
    Profile profile;
    //GIAB
    String TAG = "GIAB";
    IabHelper mHelper;
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener;
    IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener;
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener;
    String base64EncodedPublicKey =
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkVwtebd7YnyJHi16iyvfprUDxUbACEX0v2I1PZHK1ec" +
                    "66tGC10DSF9DHaOL4Ta0sBylWWOgmfZvw1NRg3MjIrE5YXGki3WWj03zyzcfNJdZKjd1f0MST452EDs" +
                    "HQf0RmLZld3JFOegYVSr1GrGUmsFJjXX+v2PrnNkKhdmMeVBzclRxqrSpC++Uv1ciD4An3bFmbLG46E" +
                    "CcyQc7syiegpXF6nt0ZM6CVGYqufdLJbDfxgUUBePFcrk8mB9M5+zCW31mOHOGOQMKBqQ1wT4HLUBXw" +
                    "Dfpy9L/KgemYkILC8IwXd6uyLtPi0QApnx8YzIUIrUul/yTRcs90Z7DaXcXOkQIDAQAB";
    String[] ITEM_SKU = {"coin_20", "coin_50", "coin_80", "coin_130", "coin_200", "coin_250"};
    String chosen_SKU;
    //GCM
    String gcmToken;
    //Adcolony
    String colony_zone_id = "vzc6784ea2587244da9a";
    String colony_app_id = "app17674e6eb9fb477d92";
    TopModelGlobal globalState;
    private DrawerArrowDrawable drawerArrowDrawable;
    private ImageView nav_menu_toggle;
    //    private SlidingPaneLayout slidingPane;
    private DrawerLayout slidingPane;
    private Toolbar tb;
    private EditText searchBar;
    private GoogleSignInAccount acct;
    private boolean googleSignIn;
    private int TAKE_PHOTO_CODE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Set<RequestListener> listeners = new HashSet<>();
        listeners.add(new RequestLoggingListener());
        ImagePipelineConfig config = ImagePipelineConfig.newBuilder(this)
                .setRequestListeners(listeners)
                .setBitmapsConfig(Bitmap.Config.ARGB_8888)
                .build();
        Fresco.initialize(this, config);

        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        SharedPreferences settings = getSharedPreferences("sharedpref", 0);
        setLocale(settings.getString("locale", "in"));

        setContentView(R.layout.activity_main_sliding);

        //Adcolony
        AdColony.configure(this, "version:2.1,store:google", colony_app_id, colony_zone_id);
        AdColonyV4VCListener v4vclistener = new AdColonyV4VCListener() {
            @Override
            public void onAdColonyV4VCReward(AdColonyV4VCReward reward) {
                if (reward.success()) {
                    rewardCoins(String.valueOf(reward.amount()));
                } else {
                    showSnack(getStringRes(R.string.error_ad_show_failed));
                }
            }
        };
        AdColony.addV4VCListener(v4vclistener);

        adcbHelper = new AdcbHelper(MainActivity.this);
        client = new AsyncHttpClient();
        client.setTimeout(40000);

        //Google+
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this  /*FragmentActivity*/, this  /*OnConnectionFailedListener*/)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        scale = getResources().getDisplayMetrics().density;

        globalState = (TopModelGlobal) getApplicationContext();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        final Resources resources = getResources();

        tb = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(tb);

        final android.support.v7.app.ActionBar ab = getSupportActionBar();
        ab.setDisplayShowCustomEnabled(true);

        fragmentManager = getSupportFragmentManager();

        fragmentTransaction = fragmentManager.beginTransaction();

        setDrawerMainList();

        setLoginAdapter();

        nav_menu_toggle = (ImageView) tb.findViewById(R.id.nav_icon);
        nav_menu_toggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (slidingPane.isDrawerOpen(Gravity.LEFT)) {
                    slidingPane.closeDrawer(Gravity.LEFT);
                } else
                    slidingPane.closeDrawer(Gravity.LEFT);
            }
        });

        drawerArrowDrawable = new DrawerArrowDrawable(resources, true);
        drawerArrowDrawable.setStrokeColor(resources.getColor(android.R.color.white));
        nav_menu_toggle.setImageDrawable(drawerArrowDrawable);

        slidingPane = (DrawerLayout) findViewById(R.id.sliding_Panel);
        slidingPane.setScrimColor(Color.parseColor("#CC231F20"));

        /*slidingPane.setPanelSlideListener(new SlidingPaneLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {

            }

            @Override
            public void onPanelOpened(View panel) {

            }

            @Override
            public void onPanelClosed(View panel) {

            }
        });*/

        searchBar = (EditText) tb.findViewById(R.id.toolbar_search_et);
        ViewTreeObserver vto = searchBar.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                searchBar.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                int width = searchBar.getMeasuredWidth();
                int height = searchBar.getMeasuredHeight();
                searchBarWidth = width;

            }
        });
        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0) {
                    if (!expanded) {
                        expandSearch();

                    }
                } else {
                    if (expanded) {

                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        setProfilePic();

        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        float dpHeight = displayMetrics.heightPixels / displayMetrics.density;
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;


    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.getExtras() != null) {
            Bundle data = intent.getBundleExtra("data");
            if (data.getString("content_type").equalsIgnoreCase("video")) {
                changeFragment(new VideoDetailFragment(), "videodetail", data);
            } else if (data.getString("content_type").equalsIgnoreCase("wallpaper")) {
                changeFragment(new WallpaperDetailFragment(), "wallpaperdetail", data);
            }
        }
    }

    public void setProfilePic() {
        SimpleDraweeView profile_pic = (SimpleDraweeView) findViewById(R.id.nav_profile_pic);

        if (globalState.isFacebookLogin()) {
            profile_pic.setImageURI(Uri.parse(globalState.getFacebook_profile_pic_url()));
        } else if (globalState.isGoogleLogin()) {
            try {
                if (acct.getPhotoUrl().toString().length() > 0) {
                    profile_pic.setImageURI(acct.getPhotoUrl());
                } else {
                    profile_pic.setImageURI(Uri.parse("res:///" + R.drawable.avatar_default));
                }
            } catch (NullPointerException e) {
                profile_pic.setImageURI(Uri.parse("res:///" + R.drawable.avatar_default));
            }
        } else {
            final String dir = Environment.getExternalStorageDirectory().getPath() + "/Top Model/";
            File newdir = new File(dir);
            String file = dir + "failed_image" + ".png";
            File newfile = new File(file);
            if (newfile.exists()) {
                profile_pic.setImageURI(Uri.parse("res:///" + R.drawable.avatar_default));
            } else {
                profile_pic.setImageURI(Uri.parse("res:///" + R.drawable.avatar_default));
            }
        }

        RoundingParams roundingParams = RoundingParams.fromCornersRadius(5f);
        roundingParams.setBorder(Color.WHITE, 5.0f);
        roundingParams.setRoundAsCircle(true);
        profile_pic.getHierarchy().setRoundingParams(roundingParams);

    }

    public void expandSearch() {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ObjectAnimator scaleAnimator = ViewPropertyObjectAnimator.animate(searchBar)
                                //.width(searchBarWidth + convertToDp(100))
                                .width(searchBarWidth + convertToDp(150))
                                .get();
                        scaleAnimator.setDuration(150);
                        scaleAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
                        scaleAnimator.start();
                        changeFragment(new SearchFrag(), "search");
                        expanded = true;
                    }
                }, 50);
            }
        }).start();
    }

    public void collapseSearch(final boolean state) {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ObjectAnimator scaleAnimator = ViewPropertyObjectAnimator.animate(searchBar)
                                .width(searchBarWidth)
                                .get();
                        scaleAnimator.setDuration(150);
                        scaleAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
                        scaleAnimator.start();
                        if (state) {
                            fragmentManager.popBackStackImmediate();
                        }
                        searchBar.setText("");
                        expanded = false;
                    }
                }, 50);
            }
        }).start();
    }

    public void collapseSearch() {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //collapse(searchBar);
                        ObjectAnimator scaleAnimator = ViewPropertyObjectAnimator.animate(searchBar)
                                .width(searchBarWidth)
                                .get();
                        scaleAnimator.setDuration(150);
                        scaleAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
                        scaleAnimator.start();
                        fragmentManager.popBackStackImmediate();
                        searchBar.setText("");
                        expanded = false;
                    }
                }, 50);
            }
        }).start();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void setToolBarTitle(final String title) {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (title.isEmpty()) {
                            tb.findViewById(R.id.toolbar_search_et).setVisibility(View.VISIBLE);
                            tb.findViewById(R.id.toolbar_logo_icon).setVisibility(View.VISIBLE);
                            tb.findViewById(R.id.toolbar_title).setVisibility(View.GONE);
                        } else {
                            tb.findViewById(R.id.toolbar_search_et).setVisibility(View.GONE);
                            tb.findViewById(R.id.toolbar_logo_icon).setVisibility(View.GONE);
                            TextView toolbarTitle = (TextView) tb.findViewById(R.id.toolbar_title);
                            toolbarTitle.setVisibility(View.VISIBLE);
                            toolbarTitle.setText(title);
                        }
                    }
                }, 50);
            }
        }).start();

    }

    public void changeFragment(final android.support.v4.app.Fragment fragment, final String tag, Bundle bundle) {
        ThreadTask fragmentTask = new ThreadTask();
        fragmentTask.type = "changefrag";
        fragmentTask.fragment = fragment;
        fragmentTask.tag = tag;
        fragmentTask.bundle = bundle;
        fragmentTask.execute();
    }

    public void changeFragment(final android.support.v4.app.Fragment fragment, final String tag) {
        ThreadTask fragmentTask = new ThreadTask();
        fragmentTask.type = "changefrag";
        fragmentTask.fragment = fragment;
        fragmentTask.tag = tag;
        fragmentTask.execute();
    }

    @Override
    public void onBackPressed() {
        if (slidingPane.isDrawerOpen(Gravity.LEFT)) {
            closePane();
        } else {
            if (fragmentManager.getBackStackEntryCount() == 0) {
                super.onBackPressed();
            } else {
                //Toast.makeText(MainActivity.this, "Got", Toast.LENGTH_SHORT).show();
                super.onBackPressed();
            }
        }
        if (expanded) {
            ObjectAnimator scaleAnimator = ViewPropertyObjectAnimator.animate(searchBar)
                    .width(searchBarWidth)
                    .get();
            scaleAnimator.setDuration(300);
            scaleAnimator.start();
            fragmentManager.popBackStackImmediate();
            expanded = false;
            searchBar.setText("");
            searchBar.clearFocus();
        }
    }

    public int convertToDp(int dps) {
        int pixels = (int) (dps * scale + 0.5f);
        return pixels;
    }

    public void TitleVisibility(final boolean show, final int duration) {
        final TextView title = (TextView) tb.findViewById(R.id.toolbar_title);
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (show && title.getVisibility() == View.GONE
                                && fragmentManager.getBackStackEntryCount() != 0) {
                            Animation fadein = AnimationUtils.loadAnimation(MainActivity.this, R.anim.fade_in);
                            fadein.setDuration(duration);
                            title.startAnimation(fadein);
                            title.setVisibility(View.VISIBLE);
                        } else if (!show && title.getVisibility() == View.VISIBLE) {
                            Animation fadeout = AnimationUtils.loadAnimation(MainActivity.this, R.anim.fade_out);
                            fadeout.setDuration(duration);
                            title.startAnimation(fadeout);
                            title.setVisibility(View.GONE);
                        }
                    }
                }, 50);
            }
        }).start();
    }

    public void LogoVisibility(final boolean show, final int duration) {
        final ImageView logo = (ImageView) tb.findViewById(R.id.toolbar_logo_icon);
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (show && logo.getVisibility() == View.GONE
                                && fragmentManager.getBackStackEntryCount() == 0) {
                            Animation fadein = AnimationUtils.loadAnimation(MainActivity.this, R.anim.fade_in);
                            fadein.setDuration(duration);
                            logo.startAnimation(fadein);
                            logo.setVisibility(View.VISIBLE);
                        } else if (!show && logo.getVisibility() == View.VISIBLE) {
                            Animation fadeout = AnimationUtils.loadAnimation(MainActivity.this, R.anim.fade_out);
                            fadeout.setDuration(duration);
                            logo.startAnimation(fadeout);
                            logo.setVisibility(View.GONE);
                        }
                    }
                }, 50);
            }
        }).start();
    }

    public void closePane() {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        slidingPane.closeDrawer(Gravity.LEFT);
                    }
                }, 50);
            }
        }).start();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences settings = getSharedPreferences("sharedpref", 0);
        TopModelGlobal global = (TopModelGlobal) getApplicationContext();

        //first time
        if (settings.getBoolean("my_first_time", true)) {
            global.setLoggedIn(false);

            // record the fact that the app has been started at least once
            settings.edit().putBoolean("my_first_time", false).commit();
            settings.edit().putBoolean("loggedIn", false).commit();
            global.setLocale("in");
            settings.edit().putString("locale", "in").commit();

            //start SplashActivity
            startActivity(new Intent(MainActivity.this, SplashActivity.class));
            finish();
        } else {
            global.setLoggedIn(settings.getBoolean("loggedIn", false));
            global.setLocale(settings.getString("locale", "in"));
        }

        //Locale
        setLocale(global.getLocale());
        //setLocale("en");

        AdColony.resume(this);


        //
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            try {
                Bundle data = intent.getBundleExtra("data");
                if (data.getString("content_type").equalsIgnoreCase("video")) {
                    changeFragment(new VideoDetailFragment(), "videodetail", data);
                } else if (data.getString("content_type").equalsIgnoreCase("wallpaper")) {
                    changeFragment(new WallpaperDetailFragment(), "wallpaperdetail", data);
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }

        if (fragmentManager.findFragmentById(R.id.fragment_container) == null) {
            fragmentTransaction.add(R.id.fragment_container, new HomeFrag(), "home");
            fragmentTransaction.commit();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        AdColony.pause();
    }

    public void setLoginAdapter() {

        if (globalState.getLoggedIn() == false) {
            findViewById(R.id.nav_profile_pic).setVisibility(View.INVISIBLE);
            findViewById(R.id.nav_username_tv).setVisibility(View.INVISIBLE);

            String[] loginlistitems = {getStringRes(R.string.nav_login)};
            ListView loginList = (ListView) findViewById(R.id.nav_login_list);
            loginList.setAdapter(new ArrayAdapter<String>(this,
                    R.layout.nav_rowlayout, R.id.list_item_label, loginlistitems));
            loginList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (slidingPane.isDrawerOpen(Gravity.LEFT)) {
                        switch (position) {
                            case 0:
                                if (fragmentManager.findFragmentById(R.id.fragment_container) instanceof LoginFragment) {
                                    closePane();
                                } else {
                                    changeFragment(new LoginFragment(), "login");
                                }
                                break;
                            default:
                                Toast.makeText(MainActivity.this, "Oops! Index: " + position, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        } else {
            ImageView profilePic_iv = (ImageView) findViewById(R.id.nav_profile_pic);
            TextView profileName_tv = (TextView) findViewById(R.id.nav_username_tv);

            profilePic_iv.setVisibility(View.VISIBLE);
            profileName_tv.setVisibility(View.VISIBLE);
            profileName_tv.setText(globalState.getUser_data().getFullname());
            String[] logoutlistitems = {getStringRes(R.string.nav_logout)};
            ListView logoutList = (ListView) findViewById(R.id.nav_login_list);
            logoutList.setAdapter(new ArrayAdapter<String>(this,
                    R.layout.nav_rowlayout, R.id.list_item_label, logoutlistitems));
            logoutList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (slidingPane.isDrawerOpen(Gravity.LEFT)) {
                        switch (position) {
                            case 0:
                                if (globalState.isFacebookLogin()) {
                                    FacebookSignOut();
                                    globalState.setIsFacebookLogin(false);

                                    SharedPreferences settings = getSharedPreferences("sharedpref", 0);
                                    settings.edit().putBoolean("facebook_login", false).commit();
                                    settings.edit().putString("facebook_pic", "").commit();
                                    settings.edit().putString("facebook_email", "").commit();
                                }
                                if (globalState.isGoogleLogin()) {
                                    GoogleSignOut();
                                    googleSignIn = false;
                                    globalState.setIsGoogleLogin(false);
                                    SharedPreferences settings = getSharedPreferences("sharedpref", 0);
                                    settings.edit().putBoolean("google_login", false).commit();
                                    settings.edit().putString("google_pic", "").commit();
                                    settings.edit().putString("facebook_email", "").commit();
                                }

                                emailLogout();
                                setProfilePic();

                                setDrawerMainList();
                                setLoginAdapter();
                                if (haveToPop()) {
                                    if (fragmentManager.findFragmentById(R.id.fragment_container) instanceof HomeFrag) {
                                        closePane();
                                    } else {
                                        changeFragment(new HomeFrag(), "home");
                                    }
                                }
                                showSnack(getString(R.string.notif_logout));
                                break;
                            default:
                                Toast.makeText(MainActivity.this, "Oops! Index: " + position, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        }
    }

    public void setDrawerMainList() {
        if (globalState.getLoggedIn() == true) {
            ArrayList<String> listItems = new ArrayList<String>();
            listItems.add(getStringRes(R.string.nav_home));
            listItems.add(getStringRes(R.string.nav_video));
            listItems.add(getStringRes(R.string.nav_wallpaper));
            listItems.add(getStringRes(R.string.nav_my_profile));

            ListView mainList = (ListView) findViewById(R.id.nav_main_list);
            mainList.setAdapter(new ArrayAdapter<String>(this,
                    R.layout.nav_rowlayout, R.id.list_item_label, listItems));
            mainList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (slidingPane.isDrawerOpen(Gravity.LEFT)) {
                        switch (position) {
                            case 0:
                                if (fragmentManager.findFragmentById(R.id.fragment_container) instanceof HomeFrag) {
                                    closePane();
                                } else {
                                    changeFragment(new HomeFrag(), "home");
                                }
                                break;
                            case 1:
                                if (fragmentManager.findFragmentById(R.id.fragment_container) instanceof VideoFragment) {
                                    closePane();
                                } else {
                                    changeFragment(new VideoFragment(), "video");
                                }
                                break;
                            case 2:
                                if (fragmentManager.findFragmentById(R.id.fragment_container) instanceof WallpaperFragment) {
                                    closePane();
                                } else {
                                    changeFragment(new WallpaperFragment(), "wallpaper");
                                }
                                break;
                            case 3:
                                if (fragmentManager.findFragmentById(R.id.fragment_container) instanceof MyProfileFragment) {
                                    closePane();
                                } else {
                                    changeFragment(new MyProfileFragment(), "myprofile");
                                }
                                break;
                            default:
                                Toast.makeText(MainActivity.this, "Oops! Index: " + position, Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }
                }
            });
        } else {
            ArrayList<String> listItems = new ArrayList<String>();
            listItems.add(getStringRes(R.string.nav_home));
            listItems.add(getStringRes(R.string.nav_video));
            listItems.add(getStringRes(R.string.nav_wallpaper));
            ListView mainList = (ListView) findViewById(R.id.nav_main_list);
            mainList.setAdapter(new ArrayAdapter<String>(this,
                    R.layout.nav_rowlayout, R.id.list_item_label, listItems));
            mainList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (slidingPane.isDrawerOpen(Gravity.LEFT)) {
                        switch (position) {
                            case 0:
                                //closePane();
                                if (fragmentManager.findFragmentById(R.id.fragment_container) instanceof HomeFrag) {
                                    closePane();
                                } else {
                                    changeFragment(new HomeFrag(), "home");
                                }
                                break;
                            case 1:
                                //closePane();
                                if (fragmentManager.findFragmentById(R.id.fragment_container) instanceof VideoFragment) {
                                    closePane();
                                } else {
                                    changeFragment(new VideoFragment(), "video");
                                }
                                break;
                            case 2:
                                //closePane();
                                if (fragmentManager.findFragmentById(R.id.fragment_container) instanceof WallpaperFragment) {
                                    closePane();
                                } else {
                                    changeFragment(new WallpaperFragment(), "wallpaper");
                                }
                                break;
                            default:
                                Toast.makeText(MainActivity.this, "Oops! Index: " + position, Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }
                }
            });
        }
    }

    private boolean haveToPop() {
        if (fragmentManager.findFragmentById(R.id.fragment_container) instanceof HomeFrag) {
            return false;
        } else {
            return true;
        }
    }

    public void showSnack(String message, String actionMessage, View.OnClickListener actionListener) {
        TSnackbar snackBar = TSnackbar
                .make(findViewById(android.R.id.content), message, TSnackbar.LENGTH_LONG)
                .setAction(actionMessage, actionListener);

        View sbView = snackBar.getView();
        TextView textView = (TextView) sbView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
        textView.setTypeface(Typeface.createFromAsset(getAssets(), "Fonts/SourceSansPro-Regular.otf"));

        //
        Button action = (Button) sbView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_action);
        action.setTypeface(Typeface.createFromAsset(getAssets(), "Fonts/Rubik-Regular.ttf"));
        action.setTextColor(Color.WHITE);
        snackBar.show();
    }

    public void showSnack(String message) {
        final TSnackbar snackBar = TSnackbar.make(findViewById(android.R.id.content),
                message, TSnackbar.LENGTH_LONG);
        View sbView = snackBar.getView();
        TextView textView = (TextView) sbView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
        textView.setTypeface(Typeface.createFromAsset(getAssets(), "Fonts/SourceSansPro-Regular.otf"));
        sbView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackBar.dismiss();
            }
        });

        snackBar.show();
    }

    @Override
    public void onConnected(Bundle bundle) {
        try {

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Google+", e.toString());
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private boolean checkDupe(ArrayList<PurchaseHistory> array, PurchaseHistory obj) {
        if (array.size() > 0) {
            for (int i = 0; i < array.size(); i++) {
                if (array.get(i).getContent_id().equalsIgnoreCase(obj.getContent_id())) {
                    return true;
                }
            }
        } else {
            return false;
        }
        return false;
    }

    public void setLocale(String lang) {
        myLocale = new Locale(lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        SharedPreferences settings = getSharedPreferences("sharedpref", 0);
        settings.edit().putString("locale", lang).commit();
    }

    public String getStringRes(int res) {
        return getResources().getString(res);
    }

    @Override
    public void AseandcbChargingResult(String transactionId, String StatusCode, String amount, String telco) {
        if (adcbHelper.checkPayment(StatusCode, telco)) {
            showSnack(getStringRes(R.string.notif_payment_successful));
            awardCoins(amount);
        } else {
            showSnack(getStringRes(R.string.error_payment_failed));
        }
    }

    private void awardGoogleCoins(String SKU) {
        Login userData = globalState.getUser_data();
        int currentWallet = Integer.parseInt(userData.getWallet());
        int newWallet;
        int reward;
        int amount;

        switch (SKU) {
            case "coin20":
                newWallet = currentWallet + 20;
                reward = 20;
                amount = 2000;
                break;
            case "coin50":
                newWallet = currentWallet + 50;
                reward = 50;
                amount = 3000;
                break;
            case "coin120":
                newWallet = currentWallet + 120;
                reward = 120;
                amount = 5000;
                break;
            case "coin250":
                newWallet = currentWallet + 250;
                reward = 250;
                amount = 10000;
                break;
            default:
                showSnack(getStringRes(R.string.error_invalid_amount));
                newWallet = currentWallet;
                reward = 0;
                amount = 0;
                break;
        }

        Product product = new Product()
                .setId(SKU)
                .setName("Coins " + SKU)
                .setVariant("Google Coint");

        ProductAction action = new ProductAction(ProductAction.ACTION_CLICK)
                .setProductActionList("Buy Coins");

        HitBuilders.ScreenViewBuilder builder = new HitBuilders.ScreenViewBuilder()
                .setProductAction(new ProductAction(ProductAction.ACTION_PURCHASE)
                        .setTransactionId("TC-GWallet" + amount)
                        .setTransactionRevenue(amount))
                .addProduct(product);

        Tracker t = globalState.getDefaultTracker(
                TopModelGlobal.TrackerName.APP_TRACKER);
        t.setScreenName("Buying Coins");
        t.set("&cu", "IDR");
        t.send(builder.build());

        userData.setWallet(String.valueOf(newWallet));
        globalState.setUser_data(userData);
        updateSharedPrefUserData(userData, String.valueOf(reward));

    }

    private void awardCoins(String amt) {
        Login userData = globalState.getUser_data();
        int currentWallet = Integer.parseInt(userData.getWallet());
        int newWallet;
        int reward;

        int amount = Integer.parseInt(amt);
        switch (amount) {
            case 2000:
                newWallet = currentWallet + 20;
                reward = 20;
                break;
            case 3000:
                newWallet = currentWallet + 50;
                reward = 50;
                break;
            case 5000:
                newWallet = currentWallet + 120;
                reward = 120;
                break;
            case 10000:
                newWallet = currentWallet + 250;
                reward = 250;
                break;
            default:
                showSnack(getStringRes(R.string.error_invalid_amount));
                newWallet = currentWallet;
                reward = 0;
                break;
        }

        Product product = new Product()
                .setId("coins_" + amt)
                .setName("Coins " + amt)
                .setPrice(amount)
                .setVariant("AseanDCB Coins");

        /*ProductAction action = new ProductAction(ProductAction.ACTION_PURCHASE)
                .setProductActionList("Buy Coins");*/
        HitBuilders.ScreenViewBuilder builder = new HitBuilders.ScreenViewBuilder()
                .setProductAction(new ProductAction(ProductAction.ACTION_PURCHASE)
                        .setTransactionId("TC-ASEANdcb" + amt)
                        .setTransactionRevenue(amount))
                .addImpression(product, "Coins");


        Tracker t = globalState.getDefaultTracker(
                TopModelGlobal.TrackerName.ECOMMERCE_TRACKER);
        t.setScreenName("Buying Coins");
        t.set("&cu", "IDR");
        t.send(builder.build());

        userData.setWallet(String.valueOf(newWallet));
        globalState.setUser_data(userData);
        updateSharedPrefUserData(userData, String.valueOf(reward));
    }

    private void updateSharedPrefUserData(Login userData, String amount) {
        ArrayList<Login> loginList = new ArrayList<Login>();
        loginList.add(userData);

        String json = new Gson().toJson(loginList);
        SharedPreferences settings = getSharedPreferences("sharedpref", 0);
        settings.edit().putString("user_data", json).commit();

        updateDatabase(amount);
    }

    private void updateDatabase(final String amount) {
        String url = "";
        String LOGIN_TYPE = "";

        try {
            if (globalState.isGoogleLogin()) {
                LOGIN_TYPE = "gmail";
            } else if (globalState.isFacebookLogin()) {
                LOGIN_TYPE = "facebook";
            } else {
                LOGIN_TYPE = "email";
            }

            url = "http://210.5.41.103/tuneapp/api/wallet/" +
                    URLEncoder.encode(globalState.getUser_data().getUsername(), "UTF-8") + "/" +
                    URLEncoder.encode(amount, "UTF-8") + "/" +
                    "top%20model%20indo/" +
                    LOGIN_TYPE;
        } catch (UnsupportedEncodingException e) {
            showSnack(getStringRes(R.string.error_encoding));
        }

        Log.e("BUY COINS", url);

        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                showSnack(getStringRes(R.string.error_unknown));
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Gson gson = new GsonBuilder().create();
                JsonParser parser = new JsonParser();
                JsonArray jArray = parser.parse(responseString).getAsJsonArray();

                walletResponse = new ArrayList<WalletResponse>();
                for (JsonElement obj : jArray) {
                    WalletResponse temp = gson.fromJson(obj, WalletResponse.class);
                    walletResponse.add(temp);
                }

                if (walletResponse.get(0).getStatus().equalsIgnoreCase("success")) {
                    showSnack(getStringRes(R.string.notif_changes_saved));
                    globalState.getUser_data().setWallet(walletResponse.get(0).getNew_wallet());
                } else {
                    showSnack(getStringRes(R.string.error_unknown));
                }


            }
        });

    }

    public void emailLogout() {
        globalState.setLoggedIn(false);
        globalState.setPurchaseHistory(null);
        SharedPreferences settings = getSharedPreferences("sharedpref", 0);
        settings.edit().putBoolean("loggedIn", false).commit();
        settings.edit().putString("user_data", "").commit();
        settings.edit().putString("user_purchase_history", "").commit();
    }

    public void FacebookSignIn() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("user_friends", "email", "public_profile"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        GraphRequest request =
                                GraphRequest.newMeRequest(loginResult.getAccessToken(),
                                        new GraphRequest.GraphJSONObjectCallback() {

                                            @Override
                                            public void onCompleted(JSONObject object, GraphResponse response) {
                                                try {
                                                    String email = object.getString("email");
                                                    //globalState.getUser_data().setUsername(email);
                                                    globalState.setFacebook_email(email);
                                                    globalState.setFacebook_profile_pic_url(object.getJSONObject("picture").getJSONObject("data").getString("url"));
                                                    LoginFragment fragment = (LoginFragment) fragmentManager.findFragmentById(R.id.fragment_container);
                                                    fragment.onFacebookSignIn(true);
                                                } catch (JSONException e) {
                                                    //e.printStackTrace();
                                                    /*Toast.makeText(MainActivity.this, "JSONException " + e.toString(),
                                                            Toast.LENGTH_SHORT).show();*/
                                                    showSnack(getStringRes(R.string.error_unknown));
                                                }
                                            }
                                        });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,picture.type(large)");
                        request.setParameters(parameters);
                        request.executeAsync();

                    }

                    @Override
                    public void onCancel() {
                        LoginFragment fragment = (LoginFragment) fragmentManager.findFragmentById(R.id.fragment_container);
                        fragment.onFacebookSignIn(false);
                    }

                    @Override
                    public void onError(FacebookException error) {
                        LoginFragment fragment = (LoginFragment) fragmentManager.findFragmentById(R.id.fragment_container);
                        fragment.onFacebookSignIn(false);
                    }
                });

    }

    public void FacebookSignOut() {
        LoginManager.getInstance().logOut();
        //showSnack("Facebook Signed Out");
        showSnack(getStringRes(R.string.notif_logout));
        globalState.setLoggedIn(false);
    }

    public Profile getFacebookProfile() {
        return Profile.getCurrentProfile();
    }


    public void GoogleSignIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    public void GoogleSignOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                        //updateUI(false);
                        // [END_EXCLUDE]
                        //Toast.makeText(MainActivity.this, "Google+ Signed out", Toast.LENGTH_SHORT).show();
                        showSnack(getStringRes(R.string.notif_logout));
                        globalState.setLoggedIn(false);
                    }
                });
    }

    public void doCamera() {
        final String dir = Environment.getExternalStorageDirectory() + "/Top Model/";
        File newdir = new File(dir);
        newdir.mkdirs();
        String file = dir + "mypic" + ".jpg";
        File newfile = new File(file);
        try {
            newfile.createNewFile();
        } catch (IOException e) {
            Log.d("BMP", "IOException" + e.toString());
        }

        Uri outputFileUri = Uri.fromFile(newfile);

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

        startActivityForResult(cameraIntent, TAKE_PHOTO_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }

        if (requestCode == TAKE_PHOTO_CODE && resultCode == RESULT_OK) {
            Log.d("BMP", "Pic saved");
        }

        callbackManager.onActivityResult(requestCode, resultCode, data);


        if (!mHelper.handleActivityResult(requestCode,
                resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            acct = result.getSignInAccount();
            googleSignIn = true;
            //mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));
            //Toast.makeText(MainActivity.this, "Account :" +acct.getDisplayName(), Toast.LENGTH_SHORT).show();
            //Toast.makeText(MainActivity.this, "Account :" +acct.getId(), Toast.LENGTH_SHORT).show();
            //Toast.makeText(MainActivity.this, "Account :" +acct.getEmail(), Toast.LENGTH_SHORT).show();
            //Toast.makeText(MainActivity.this, "Account :" +acct.getPhotoUrl(), Toast.LENGTH_SHORT).show();
            //updateUI(true);

            //fragmentManager.findFragmentById(R.id.fragment_container) instanceof HomeFrag;
            LoginFragment fragment = (LoginFragment) fragmentManager.findFragmentById(R.id.fragment_container);
            fragment.onGoogleSignIn(true);
        } else {
            // Signed out, show unauthenticated UI.
            //updateUI(false);
            LoginFragment fragment = (LoginFragment) fragmentManager.findFragmentById(R.id.fragment_container);
            fragment.onGoogleSignIn(false);
        }
    }

    public GoogleSignInAccount getAcct() {
        return acct;
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //Toast.makeText(MainActivity.this, "Connection Failed", Toast.LENGTH_SHORT).show();
        showSnack(getStringRes(R.string.error_no_internet));
    }

    @Override
    public void onStart() {
        super.onStart();
        googleSignIn = false;

        GoogleAnalytics.getInstance(this).reportActivityStart(this);

        if (globalState.getLoggedIn() == true) {
            globalState.getDefaultTracker(TopModelGlobal.TrackerName.APP_TRACKER)
                    .setScreenName("Home Screen");

            globalState.getDefaultTracker(TopModelGlobal.TrackerName.APP_TRACKER)
                    .send(new HitBuilders.ScreenViewBuilder().build());
        }

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            //Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            //handleSignInResult(result);
            handleSignInOnStart(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            //showProgressDialog();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    //hideProgressDialog();
                    //handleSignInResult(googleSignInResult);
                    handleSignInOnStart(googleSignInResult);
                }
            });
        }

        mHelper = new IabHelper(this, base64EncodedPublicKey);

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    Log.d(TAG, "In-app Billing setup failed: " +
                            result);
                } else {
                    Log.d(TAG, "In-app Billing is set up OK");
                }
            }
        });

        mPurchaseFinishedListener
                = new IabHelper.OnIabPurchaseFinishedListener() {
            public void onIabPurchaseFinished(IabResult result,
                                              Purchase purchase) {
                if (result.isFailure()) {
                    // Handle error
                    return;
                } else if (purchase.getSku().equals(chosen_SKU)) {
                    consumeItem();
                    awardGoogleCoins(chosen_SKU);

                }
            }
        };

        mReceivedInventoryListener
                = new IabHelper.QueryInventoryFinishedListener() {
            public void onQueryInventoryFinished(IabResult result,
                                                 Inventory inventory) {
                if (result.isFailure()) {
                    // Handle failure
                    //Toast.makeText(InAppActivity.this, "There was an error processing purchase", Toast.LENGTH_SHORT).show();
                    //AppsFlyerLib.sendTrackingWithEvent(getApplicationContext(), "In App Activity", "Google IAB purchase processing error");
                    showSnack(getStringRes(R.string.error_purchasing_coins));
                } else {
                    //mHelper.consumeAsync(inventory.getPurchase(/*ITEM_SKU*/coins100_SKU),
                    mHelper.consumeAsync(inventory.getPurchase(/*ITEM_SKU*/chosen_SKU),
                            mConsumeFinishedListener);
                }
            }
        };

        mConsumeFinishedListener =
                new IabHelper.OnConsumeFinishedListener() {
                    public void onConsumeFinished(Purchase purchase,
                                                  IabResult result) {

                        if (result.isSuccess()) {
                            //clickButton.setEnabled(true);
                            awardGoogleCoins(chosen_SKU);
                        } else {
                            // handle error
                            showSnack(getStringRes(R.string.error_purchasing_coins));
                        }
                    }
                };
    }

    private void handleSignInOnStart(GoogleSignInResult result) {
        if (result.isSuccess()) {
            acct = result.getSignInAccount();
            googleSignIn = true;
        }
    }

    public void googlePurchase(String ITEM_SKU) {
        chosen_SKU = ITEM_SKU;
        mHelper.launchPurchaseFlow(this, chosen_SKU, 10001,
                mPurchaseFinishedListener, "mypurchasetoken");
    }

    public void consumeItem() {
        mHelper.queryInventoryAsync(mReceivedInventoryListener);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null) mHelper.dispose();
        mHelper = null;
    }

    public void getGCM() {
        getTokenTask getToken = new getTokenTask();
        getToken.ctx = MainActivity.this;
        getToken.execute();
    }

    public void passGcmToken(String token) {
        EditProfileFragment fragment = (EditProfileFragment) fragmentManager.findFragmentById(R.id.fragment_container);
        fragment.receiveGcmToken(token);
    }

    public void setgcmToken(String tok) {
        /*SharedPreferences settings = getSharedPreferences("sharedpref", 0);
        settings.edit().putString("gcm_token", tok);*/
        gcmToken = tok;
    }

    public void showadColony() {
        AdColonyV4VCAd ad = new AdColonyV4VCAd();
        if (ad.isReady()) {
            //Toast.makeText(MainActivity.this, "ready", Toast.LENGTH_SHORT).show();
        } else {
            //Toast.makeText(MainActivity.this, "notready", Toast.LENGTH_SHORT).show();
        }
        ad.show();
    }

    public void rewardCoins(final String amount) {
        final Dialog progressDialog = new Dialog(MainActivity.this, R.style.ThemeDialogCustom);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        progressDialog.setCancelable(false);
        final TextView dialog_text = (TextView) progressDialog.findViewById(R.id.dialog_progress_text);
        dialog_text.setText("Rewarding Coins\nPlease Wait");
        String url = "";
        String LOGIN_TYPE = "";

        if (globalState.isGoogleLogin()) {
            LOGIN_TYPE = "gmail";
        } else if (globalState.isFacebookLogin()) {
            LOGIN_TYPE = "facebook";
        } else {
            LOGIN_TYPE = "email";
        }

        try {
            url = "http://210.5.41.103/tuneapp/api/wallet/" +
                    URLEncoder.encode(globalState.getUser_data().getUsername(), "UTF-8") + "/" +
                    URLEncoder.encode(amount, "UTF-8") + "/" +
                    "top%20model%20indo/" +
                    LOGIN_TYPE;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            showSnack(getStringRes(R.string.error_encoding));
        }

        Log.d("Reward", "URL " + url);
        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progressDialog.show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.d("Reward", String.valueOf(statusCode));
                progressDialog.dismiss();
                showSnack(getStringRes(R.string.error_ad_show_failed));
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Gson gson = new GsonBuilder().create();
                JsonParser parser = new JsonParser();
                JsonArray jArray = parser.parse(responseString).getAsJsonArray();

                walletResponse = new ArrayList<WalletResponse>();
                for (JsonElement obj : jArray) {
                    WalletResponse temp = gson.fromJson(obj, WalletResponse.class);
                    walletResponse.add(temp);
                }

                if (walletResponse.get(0).getStatus().equalsIgnoreCase("success")) {
                    progressDialog.dismiss();
                    showSnack(amount + " Coins Rewarded Successfully");
                    SharedPreferences settings = getSharedPreferences("sharedpref", 0);
                    String oldUserData = settings.getString("user_data", "");

                    JsonArray ujArray = parser.parse(oldUserData).getAsJsonArray();
                    loginObj = new ArrayList<Login>();
                    for (JsonElement obj : ujArray) {
                        Login temp = gson.fromJson(obj, Login.class);
                        loginObj.add(temp);
                    }
                    if (loginObj.size() > 0) {
                        String oldSWallet = loginObj.get(0).getWallet();
                        int newSwallet = Integer.parseInt(oldSWallet) + Integer.parseInt(amount);
                        loginObj.get(0).setWallet(String.valueOf(newSwallet));
                        globalState.getUser_data().setWallet(String.valueOf(newSwallet));
                        settings.edit().putString("user_data", gson.toJson(loginObj)).commit();
                        //Toast.makeText(MainActivity.this, "global "+globalState.getUser_data().getWallet(), Toast.LENGTH_SHORT).show();
                        MyProfileFragment fragment = (MyProfileFragment) fragmentManager.findFragmentById(R.id.fragment_container);
                        fragment.setViews();
//                        changeFragment(fragment, "myprofile");
                    } else {
                        showSnack(getStringRes(R.string.error_unknown));
                    }
                } else {
                    progressDialog.dismiss();
                    showSnack(getStringRes(R.string.error_unknown));
                }
            }
        });
    }

    public void startDownloadService(String contentName, String url) {
        Intent serviceIntent = new Intent(MainActivity.this, DownloadService.class);
        Bundle intentBundle = new Bundle();
        intentBundle.putString("content_name", contentName);
        intentBundle.putString("url", url);
        serviceIntent.putExtra("download_data", intentBundle);
        startService(serviceIntent);
    }


    public EditText getSearchBar() {
        return searchBar;
    }

    public boolean getExpanded() {
        return expanded;
    }

    private class ThreadTask extends AsyncTask<Void, Void, Void> {
        String type;
        String tag;
        Fragment fragment;
        Bundle bundle;

        @Override
        protected Void doInBackground(Void... params) {
            if (type.equalsIgnoreCase("changefrag")) {
                fragmentTransaction = fragmentManager.beginTransaction();
                Fragment checkFrag = fragmentManager.findFragmentByTag(tag);
                //if (checkFrag == null) {
                if (checkFrag == null && bundle == null) {
                    fragmentTransaction
                            .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                            .replace(R.id.fragment_container, fragment, tag);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                } else if (bundle != null) {
                    fragment.setArguments(bundle);
                    fragmentTransaction
                            .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                            .replace(R.id.fragment_container, fragment, tag);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                } else {
                    fragmentTransaction
                            .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                            .replace(R.id.fragment_container, checkFrag, tag);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
            }
            return null;
        }
    }

    private class getTokenTask extends AsyncTask<String, Void, String> {
        protected Context ctx;
        String token;

        @Override
        protected String doInBackground(String... params) {
            InstanceID instanceID = InstanceID.getInstance(ctx);
            try {
                token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                        GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            } catch (IOException e) {
                e.printStackTrace();
                //Toast.makeText(ctx, "IOException in task", Toast.LENGTH_SHORT).show();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d("GCM", "Token: " + token);
            //Toast.makeText(ctx, "Token "+token, Toast.LENGTH_SHORT).show();
            setgcmToken(token);
            passGcmToken(token);
        }
    }

}
