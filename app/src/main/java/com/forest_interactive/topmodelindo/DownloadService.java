package com.forest_interactive.topmodelindo;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.BinaryHttpResponseHandler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import cz.msebera.android.httpclient.Header;

public class DownloadService extends Service {
    private int id;
    private String url;
    private String contentName;
    private NotificationManager mNotifyManager;
    private NotificationCompat.Builder mBuilder;
    private AsyncHttpClient client;

    public DownloadService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
    /*    // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");*/
        return null;
    }

    @Override
    public void onCreate(){
        client = new AsyncHttpClient();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        /*contentName = intent.getStringExtra("video_name");
        url = intent.getStringExtra("url");*/

        Bundle downloadData = intent.getBundleExtra("download_data");
        contentName = downloadData.getString("content_name");
        url = downloadData.getString("url");

        id = (int) (System.currentTimeMillis() & 0xfffffff);
        mNotifyManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(getApplicationContext());
        mBuilder.setContentTitle(contentName)
                .setContentText(getString(R.string.notif_downloading_video))
                .setSmallIcon(R.mipmap.ic_launcher);

        String[] allowedTypes = new String[]{".*"};
        String downloadurl = "";
        try {
            downloadurl = "http://210.5.41.103/CmsAdminPanel/" +
                    //"35868e7f-e9b2-4c6d-b5f2-a5fa84085ce1/DSC_0786.JPG"
                    URLEncoder.encode(url, "UTF-8");
        } catch (UnsupportedEncodingException e) {

        }

        client.get(downloadurl, new BinaryHttpResponseHandler(allowedTypes) {

            @Override
            public void onStart() {
                super.onStart();
                mBuilder.setProgress(100, 0, false);
                mNotifyManager.notify(id, mBuilder.build());
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                super.onProgress(bytesWritten, totalSize);
                mBuilder.setProgress(100, (int) ((bytesWritten * 1.0 / totalSize) * 100), false);
                mNotifyManager.notify(id, mBuilder.build());
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] binaryData) {
                OutputStream f = null;
                Uri uri = null;
                try {
                    String root = Environment.getExternalStorageDirectory().toString();
                    File myDir = new File(root + "/Top Model");
                    if (!myDir.exists()){
                        myDir.mkdirs();
                    }

                    String fname = contentName + "." + getFileNameExtension(url);
                    File file = new File (myDir, fname);
                    if (file.exists()){
                        file.delete();
                    } else {
                        file.createNewFile();
                    }
                    f = new FileOutputStream(file);
                    f.write(binaryData);
                    f.close();
                    Log.d("error", "done");
                    Intent intent =
                            new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    intent.setData(Uri.fromFile(file));
                    sendBroadcast(intent);
                    uri = Uri.fromFile(file);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NullPointerException e){
                    e.printStackTrace();
                }

                Intent fileIntent = new Intent(Intent.ACTION_VIEW);
                fileIntent.setDataAndType(uri, "video/*");
                fileIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), id, fileIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
                //
                mBuilder.setContentIntent(pendingIntent);
                mBuilder.setAutoCancel(true);
                mBuilder.setContentText(getString(R.string.notif_download_complete));
                // Removes the progress bar
                mBuilder.setProgress(0, 0, false);
                mNotifyManager.notify(id, mBuilder.build());

                stopSelf();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] binaryData, Throwable error) {
                mBuilder.setContentText(getString(R.string.error_unknown));
                // Removes the progress bar
                mBuilder.setProgress(0, 0, false);
                mNotifyManager.notify(id, mBuilder.build());

                stopSelf();
            }
        });

        return Service.START_STICKY;
    }

    @Override
    public void onDestroy(){
        client.cancelAllRequests(true);
    }

    private String getFileNameExtension(String url){
        String[] split = url.split("\\.");
        String fname = split[split.length-1];
        return fname;
    }
}
