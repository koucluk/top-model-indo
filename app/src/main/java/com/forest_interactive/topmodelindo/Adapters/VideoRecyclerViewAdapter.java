package com.forest_interactive.topmodelindo.Adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.common.util.UriUtil;
import com.facebook.drawee.drawable.ProgressBarDrawable;
import com.facebook.drawee.view.SimpleDraweeView;
import com.forest_interactive.topmodelindo.MainActivity;
import com.forest_interactive.topmodelindo.Models.Video;
import com.forest_interactive.topmodelindo.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abdulla.hainum on 26/2/2016.
 */
public class VideoRecyclerViewAdapter extends RecyclerView.Adapter<VideoRecyclerViewAdapter.DataObjectHolder> {
    private static ListClickListener listClickListener;
    private ArrayList<Video> mDataset;
    private Activity mainActivity;

    private boolean isIndo = false;


    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        SimpleDraweeView thumbnail_dv;
        TextView title_tv;
        TextView author_tv;
        TextView price_tv;
        TextView publishDate_tv;
        TextView downloadCount_tv;

        public DataObjectHolder(View itemView) {
            super(itemView);
            title_tv = (TextView) itemView.findViewById(R.id.video_list_title);
            author_tv = (TextView) itemView.findViewById(R.id.video_list_author);
            price_tv = (TextView) itemView.findViewById(R.id.video_list_price);
            publishDate_tv = (TextView) itemView.findViewById(R.id.video_list_publishdate);
            downloadCount_tv = (TextView) itemView.findViewById(R.id.video_list_downloadcount);
            thumbnail_dv = (SimpleDraweeView) itemView.findViewById(R.id.video_list_thumbnail);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            listClickListener.onItemClick(getPosition(), v);
        }

    }

    public void setOnItemClickListener(ListClickListener listClickListener) {
        this.listClickListener = listClickListener;
    }

    public VideoRecyclerViewAdapter (ArrayList<Video> myDataset, Activity act){
        mDataset = myDataset;
        mainActivity = act;
    }

    public VideoRecyclerViewAdapter (ArrayList<Video> myDataset, Activity act, boolean lang){
        mDataset = myDataset;
        mainActivity = act;
        isIndo = lang;
    }

    public VideoRecyclerViewAdapter(ArrayList<Video> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_video, parent, false);
        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        holder.title_tv.setText(mDataset.get(position).getVideo_name());
        //holder.author_tv.setText("by " + mDataset.get(position).getArtist_name());
        holder.author_tv.setText(getStringRes(R.string.by) + " " +
                mDataset.get(position).getArtist_name());
        holder.price_tv.setText(mDataset.get(position).getPrice());
        holder.publishDate_tv.setText(mDataset.get(position).getPublishDate());
        //holder.publishDate_tv.setText(mDataset.get(position).getPublishDateDifference(isIndo));

        //holder.downloadCount_tv.setText(mDataset.get(position).getTotal_download() + " Downloads");
        holder.downloadCount_tv.setText(mDataset.get(position).getTotal_download() + " " +
                getStringRes(R.string.download_count));

        ProgressBarDrawable pgd = new ProgressBarDrawable();
        pgd.setColor(Color.parseColor("#80FFFFFF"));
        holder.thumbnail_dv.getHierarchy().setProgressBarImage(pgd);
        holder.thumbnail_dv.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/"
                + mDataset.get(position).getThumbnail_url()));

        //holder.thumbnail_dv.setImageURI(Uri.parse("res:///" + R.drawable.header_01));

    }

    public void addItem(Video video, int index) {
        mDataset.add(video);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface ListClickListener{
        public void onItemClick(int position, View v);
    }

    public String getStringRes(int res){
        return mainActivity.getResources().getString(res);
    }

}
