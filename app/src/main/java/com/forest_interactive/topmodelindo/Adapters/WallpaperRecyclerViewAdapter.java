package com.forest_interactive.topmodelindo.Adapters;

import android.app.Activity;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.drawable.ProgressBarDrawable;
import com.facebook.drawee.view.SimpleDraweeView;
import com.forest_interactive.topmodelindo.Models.PurchaseHistory;
import com.forest_interactive.topmodelindo.Models.Wallpaper;
import com.forest_interactive.topmodelindo.R;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by abdulla.hainum on 29/2/2016.
 */
public class WallpaperRecyclerViewAdapter extends RecyclerView.Adapter<WallpaperRecyclerViewAdapter.DataObjectHolder> {
    private static ListClickListener listClickListener;
    private ArrayList<Wallpaper> mDataset;
    private ArrayList<PurchaseHistory> purchaseHistory;
    private Activity mainActivity;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        SimpleDraweeView thumbnail_dv;
        TextView title_tv;
        TextView author_tv;
        TextView price_tv;
        TextView publishDate_tv;
        TextView downloadCount_tv;


        public DataObjectHolder(View itemView) {
            super(itemView);

            title_tv = (TextView) itemView.findViewById(R.id.featured_wallpaper_title);
            //author_tv = (TextView) itemView.findViewById(R.id.video_list_author);
            price_tv = (TextView) itemView.findViewById(R.id.featured_wallpaper_price);
            //publishDate_tv = (TextView) itemView.findViewById(R.id.video_list_publishdate);
            downloadCount_tv = (TextView) itemView.findViewById(R.id.wallpaper_download);
            thumbnail_dv = (SimpleDraweeView) itemView.findViewById(R.id.wallpaper_image);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            listClickListener.onItemClick(getPosition(), v);
        }
    }

    public void setOnItemClickListener(ListClickListener listClickListener) {
        this.listClickListener = listClickListener;
    }

    public WallpaperRecyclerViewAdapter(ArrayList<Wallpaper> myDataset, ArrayList<PurchaseHistory> purchaseHistories,
                                        Activity act) {
        mDataset = myDataset;
        purchaseHistory = purchaseHistories;
        mainActivity = act;
    }

    public WallpaperRecyclerViewAdapter(ArrayList<Wallpaper> myDataset, ArrayList<PurchaseHistory> purchaseHistories) {
        mDataset = myDataset;
        purchaseHistory = purchaseHistories;
    }

    public WallpaperRecyclerViewAdapter(ArrayList<Wallpaper> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_wallpaper_thumbnail, parent, false);
        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        holder.title_tv.setText(mDataset.get(position).getWallpaper_name());
        //holder.author_tv.setText(mDataset.get(position).getArtist_name());
        holder.price_tv.setText(mDataset.get(position).getPrice());
        //holder.publishDate_tv.setText(mDataset.get(position).getPublishDate());
        //holder.downloadCount_tv.setText(mDataset.get(position).getTotal_download() + " Downloads");
        holder.downloadCount_tv.setText(mDataset.get(position).getTotal_download() + " "
                +getStringRes(R.string.download_count));

        ProgressBarDrawable pgd = new ProgressBarDrawable();
        pgd.setColor(Color.parseColor("#80FFFFFF"));
        holder.thumbnail_dv.getHierarchy().setProgressBarImage(pgd);
        if (isPurchased(mDataset.get(position).getContent_id())) {
            holder.thumbnail_dv.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/"
                    + mDataset.get(position).getWallpaper_url()));
        } else {
            /*holder.thumbnail_dv.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/"
                    + mDataset.get(position).getThumbnail()));*/
            holder.thumbnail_dv.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/"
                    + mDataset.get(position).getWallpaper_url()));
            holder.thumbnail_dv.getHierarchy()
                    .setControllerOverlay(mainActivity.getResources()
                            .getDrawable(R.drawable.watermark_portrait));
        }
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface ListClickListener{
        public void onItemClick(int position, View v);
    }

    private boolean isPurchased(String contentID){
        try {
            if (purchaseHistory.size() > 0) {
                for (int i = 0; i < purchaseHistory.size(); i++) {
                    if (purchaseHistory.get(i).getContent_id().equalsIgnoreCase(contentID)) {
                        return true;
                    }
                }

            } else {
                return false;
            }
        } catch (NullPointerException e){
            return false;
        }
        return false;
    }

    public String getStringRes(int res){
        return mainActivity.getResources().getString(res);
    }

}
