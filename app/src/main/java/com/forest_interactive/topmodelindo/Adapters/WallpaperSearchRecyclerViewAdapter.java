package com.forest_interactive.topmodelindo.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.forest_interactive.topmodelindo.Models.Wallpaper;
import com.forest_interactive.topmodelindo.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abdulla.hainum on 14/3/2016.
 */
public class WallpaperSearchRecyclerViewAdapter extends RecyclerView.Adapter<WallpaperSearchRecyclerViewAdapter.DataObjectHolder> {
    private static ListClickListener listClickListener;
    private ArrayList<Wallpaper> mDataset;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        TextView title_tv;

        public DataObjectHolder(View itemView) {
            super(itemView);
            title_tv = (TextView) itemView.findViewById(R.id.video_list_title);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listClickListener.onItemClick(getPosition(), v);
        }

    }

    public void setOnItemClickListener(ListClickListener listClickListener) {
        this.listClickListener = listClickListener;
    }

    public WallpaperSearchRecyclerViewAdapter (ArrayList<Wallpaper> myDataset){
        mDataset = myDataset;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_search, parent, false);
        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        holder.title_tv.setText(mDataset.get(position).getWallpaper_name());

    }

    public void addItem(Wallpaper wallpaper, int index) {
        mDataset.add(wallpaper);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface ListClickListener{
        public void onItemClick(int position, View v);
    }

    //
    public void animateTo(List<Wallpaper> models) {
        applyAndAnimateRemovals(models);
        applyAndAnimateAdditions(models);
        applyAndAnimateMovedItems(models);
    }

    private void applyAndAnimateRemovals(List<Wallpaper> newModels) {
        for (int i = mDataset.size() - 1; i >= 0; i--) {
            final Wallpaper model = mDataset.get(i);
            if (!newModels.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<Wallpaper> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final Wallpaper model = newModels.get(i);
            if (!mDataset.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<Wallpaper> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final Wallpaper model = newModels.get(toPosition);
            final int fromPosition = mDataset.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    public Wallpaper removeItem(int position) {
        final Wallpaper model = mDataset.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void moveItem(int fromPosition, int toPosition) {
        final Wallpaper model = mDataset.remove(fromPosition);
        mDataset.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }

    public void addItem(int position, Wallpaper model) {
        mDataset.add(position, model);
        notifyItemInserted(position);
    }

}
