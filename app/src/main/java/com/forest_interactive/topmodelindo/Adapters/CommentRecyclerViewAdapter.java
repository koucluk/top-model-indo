package com.forest_interactive.topmodelindo.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.forest_interactive.topmodelindo.Models.Comment;
import com.forest_interactive.topmodelindo.R;

import java.util.ArrayList;

/**
 * Created by abdulla.hainum on 1/3/2016.
 */
public class CommentRecyclerViewAdapter extends RecyclerView.Adapter<CommentRecyclerViewAdapter.DataObjectHolder> {
    private static ListClickListener listClickListener;
    private ArrayList<Comment> mDataset;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener{
        TextView commenter_name;
        TextView comment_description;
        TextView comment_date;

        public DataObjectHolder(View itemView) {
            super(itemView);
            commenter_name = (TextView) itemView.findViewById(R.id.comment_name);
            comment_description = (TextView) itemView.findViewById(R.id.comment_description);
            comment_date = (TextView) itemView.findViewById(R.id.comment_publishDate);
            itemView.setOnClickListener(this);

        }
        @Override
        public void onClick(View v) {
            listClickListener.onItemClick(getPosition(), v);
        }
    }

    public CommentRecyclerViewAdapter(ArrayList<Comment> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_comment, parent, false);
        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        holder.commenter_name.setText(mDataset.get(position).getFullname());
        holder.comment_description.setText(mDataset.get(position).getDescription());
        holder.comment_date.setText(mDataset.get(position).getDate_created());
    }

    public void addItem(Comment comment, int index) {
        mDataset.add(comment);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void setOnItemClickListener(ListClickListener listClickListener) {
        this.listClickListener = listClickListener;
    }

    public interface ListClickListener{
        public void onItemClick(int position, View v);
    }

}
