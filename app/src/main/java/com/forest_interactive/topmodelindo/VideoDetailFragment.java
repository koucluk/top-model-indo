package com.forest_interactive.topmodelindo;

import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.drawable.ProgressBarDrawable;
import com.facebook.drawee.view.SimpleDraweeView;
import com.forest_interactive.topmodelindo.Adapters.CommentRecyclerViewAdapter;
import com.forest_interactive.topmodelindo.Models.Comment;
import com.forest_interactive.topmodelindo.Models.Login;
import com.forest_interactive.topmodelindo.Models.PurchaseHistory;
import com.forest_interactive.topmodelindo.Models.PurchaseReturn;
import com.forest_interactive.topmodelindo.Widgets.CustomBaseFragment;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.Base64;
import com.loopj.android.http.BinaryHttpResponseHandler;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link VideoDetailFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link VideoDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VideoDetailFragment extends CustomBaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    TopModelGlobal global;

    View mView;
    AsyncHttpClient client;
    int id = 1;
    private ArrayList<Comment> commentObj;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private String sWallet;
    private boolean running;
    private NotificationManager mNotifyManager;
    private Builder mBuilder;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public VideoDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment VideoDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static VideoDetailFragment newInstance(String param1, String param2) {
        VideoDetailFragment fragment = new VideoDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        //getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_video_detail_dep, container, false);


        //final View view = inflater.inflate(R.layout.fragment_video_detail_dep, container, false);
        final View view = inflater.inflate(R.layout.fragment_video_detail, container, false);

        mView = view;

        return view;
    }

    public void getComments(final View view) {
        if (isNetworkAvailable()) {
            client.get("http://210.5.41.103/tuneapp/api/RetrieveUserComment/" + getArguments().getString("content_id")+"/top%20model%20indo", new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    if (running) {
                        //Toast.makeText(getActivity().getApplicationContext(), "Failed", Toast.LENGTH_SHORT).show();
                        //showSnack("Unexpected Error Occurred\nPlease try again");
                        TextView error_tv = (TextView) mView.findViewById(R.id.video_comment_error_tv);
                        error_tv.setText(getStringRes(R.string.error_unknown));
                        crossFadeViews(mView.findViewById(R.id.video_comment_progress),
                                mView.findViewById(R.id.video_comment_error_tv));
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    //Toast.makeText(getActivity().getApplicationContext(), ""+responseString, Toast.LENGTH_SHORT).show();
                    if (running) {
                        Gson gson = new GsonBuilder().create();
                        JsonParser parser = new JsonParser();
                        JsonArray jArray = parser.parse(responseString).getAsJsonArray();

                        commentObj = new ArrayList<Comment>();
                        for (JsonElement obj : jArray) {
                            Comment temp = gson.fromJson(obj, Comment.class);
                            commentObj.add(temp);
                        }
                        mAdapter = new CommentRecyclerViewAdapter(commentObj);
                        //mAdapter = new CommentRecyclerViewAdapter(getData());
                        mRecyclerView.setAdapter(mAdapter);
                        ((CommentRecyclerViewAdapter) mAdapter).setOnItemClickListener(new CommentRecyclerViewAdapter.ListClickListener() {
                            @Override
                            public void onItemClick(int position, View v) {
                                //Toast.makeText(getActivity().getApplicationContext(), "Clicked " + position, Toast.LENGTH_SHORT).show();
                            }
                        });
                        //Toast.makeText(getActivity().getApplicationContext(), "" + commentObj.get(0).getFullname(), Toast.LENGTH_SHORT).show();
                        crossFadeViews(view.findViewById(R.id.video_comment_progress), mRecyclerView);

                        //Disable RecyclerView Scroll - Set Static Height
                        RelativeLayout commentContainer = (RelativeLayout) view.findViewById(R.id.comment_holder);
                        ViewGroup.LayoutParams params = commentContainer.getLayoutParams();
                        params.height = convertToDp(70 * commentObj.size());
                        commentContainer.setLayoutParams(params);
                    }
                }
            });

            mRecyclerView = (RecyclerView) view.findViewById(R.id.video_comment_rv);
            mRecyclerView.setHasFixedSize(false);
            mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
            mRecyclerView.setLayoutManager(mLayoutManager);

        } else {
            //showSnack("Error\nPlease check your internet connection");
            TextView error_tv = (TextView) mView.findViewById(R.id.video_comment_error_tv);
            error_tv.setText(getStringRes(R.string.error_no_internet));
            crossFadeViews(mView.findViewById(R.id.video_comment_progress),
                    mView.findViewById(R.id.video_comment_error_tv));
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        global = (TopModelGlobal) getActivity().getApplicationContext();
        running = true;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceBundle) {
        super.onActivityCreated(savedInstanceBundle);
        setToolBarTitle(getStringRes(R.string.title_video));
        client = new AsyncHttpClient();
        client.setTimeout(40000);
        initOnClickListeners(mView);

        setViews(mView);

        getComments(mView);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        running = false;

    }

    @Override
    public void onStop() {
        super.onStop();
        client.cancelAllRequests(true);
    }

    public void initOnClickListeners(final View view) {
        if (global.getLoggedIn()) {
            sWallet = global.getUser_data().getWallet();
        } else {
            sWallet = "0";
        }

        final String sPrice = getArguments().getString("price");


        /*Toast.makeText(getActivity().getApplicationContext(), "Wallet "+sWallet+
                "\nPrice " + sPrice, Toast.LENGTH_SHORT).show();
        Toast.makeText(getActivity().getApplicationContext(), "Wallet " + Integer.parseInt(sWallet) +
                "\nPrice " + Integer.parseInt(sPrice), Toast.LENGTH_SHORT).show();*/
        if (!isPurchased(getArguments().getString("content_id"))) {
            view.findViewById(R.id.video_download_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    if (!isPurchased(getArguments().getString("content_id"))) {
                    try {
                        //if (global.getUser_data() == null) {
                        if (!global.getLoggedIn()) {
                            //showSnack(getStringRes(R.string.error_login_to_download));
                            showSnack(getStringRes(R.string.error_login_to_download),
                                    getStringRes(R.string.title_login),
                                    new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            changeFragment(new LoginFragment(), "login");
                                        }
                                    });
                        } else if ((Integer.parseInt(sWallet) >= Integer.parseInt(sPrice))
                                && !isPurchased(getArguments().getString("content_id"))) {

                            final Dialog tdialog = new Dialog(getActivity(), R.style.ThemeDialogCustom);
                            tdialog.setContentView(R.layout.dialog_red_green_btn);
                            //No
                            tdialog.findViewById(R.id.dialog_twobtn_no).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    tdialog.dismiss();
                                }
                            });
                            //Yes - Proceed with Purchase
                            tdialog.findViewById(R.id.dialog_twobtn_yes).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //tdialog.dismiss();
                                    //Purchase method
                                    crossFadeViews(tdialog.findViewById(R.id.dialog_content), tdialog.findViewById(R.id.dialog_progress));
                                    makePurchase(tdialog, view);

                            /*final Dialog sdialog = new Dialog(getActivity(), R.style.ThemeDialogCustom);
                            sdialog.setContentView(R.layout.dialog_single_btn);
                            sdialog.findViewById(R.id.dialog_onebtn_btn).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    sdialog.dismiss();
                                }
                            });
                            sdialog.show();*/
                                }
                            });
                        /*SpannableString ss = new SpannableString("Anda akan membeli konten ini dengan (C) " +
                                sPrice + "," + " pastikan koin Anda mencukupi");*/
                            SpannableString ss = new SpannableString(getStringRes(R.string.purchase_dialog_1) + " " +
                                    sPrice + "," + getStringRes(R.string.purchase_dialog_2));
                            Drawable d = getResources().getDrawable(R.drawable.icon_coin_01);
                            d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                            ImageSpan span = new ImageSpan(d, ImageSpan.ALIGN_BASELINE);
                            int coinIndex = ss.toString().indexOf("(");
                            ss.setSpan(span, coinIndex, coinIndex + 3, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                            TextView dialogText = (TextView) tdialog.findViewById(R.id.dialog_txt);
                            dialogText.setText(ss);
                            tdialog.show();

                        } else if (!(Integer.parseInt(global.getUser_data().getWallet()) >=
                                Integer.parseInt(getArguments().getString("price")))) {
                            showSnack(getStringRes(R.string.error_insufficient_coins));
                        } else if (isPurchased(getArguments().getString("content_id"))) {
                            fadeOutView(v);
                            //download stuff?
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                        /*Toast.makeText(getActivity().getApplicationContext(),
                                "Number Format Exception\n" + e.toString(), Toast.LENGTH_SHORT).show();*/
                        showSnack(getStringRes(R.string.error_unknown));
                    } catch (Resources.NotFoundException e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity().getApplicationContext(),
                                "Resources Not Found Exception\n" + e.toString(), Toast.LENGTH_SHORT).show();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                        showSnack(getStringRes(R.string.error_login_to_download));
                    }
                    /*} else {
                        //downloadContent();
                        downloadContentService();
                        //testFileName();
                    }*/
                }
            });
        } else {
            view.findViewById(R.id.video_download_btn).setVisibility(View.INVISIBLE);
        }

        view.findViewById(R.id.wallpaper_thumb_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPurchased(getArguments().getString("content_id"))) {
                    Intent videoActivity = new Intent(getActivity(), VideoPlayerActivity.class);
                    videoActivity.putExtra("video_url", getArguments().getString("video_url"));
                    startActivity(videoActivity);
                } else {
                    showSnack(getStringRes(R.string.error_purchase_content_to_view));
                }
            }
        });

        final Button publishButton = (Button) view.findViewById(R.id.publish_comment_btn);
        publishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (publishButton.getAlpha() != 0 && global.getUser_data().getUsername() == null) {
                        showSnack(getStringRes(R.string.error_login_to_comment));
                    } else if (publishButton.getAlpha() != 0) {
                        //fadeOutView(publishButton);
                        publishComment(view);
                    }
                } catch (NullPointerException e) {
                    showSnack(getStringRes(R.string.error_login_to_comment));
                    e.printStackTrace();
                }
            }
        });

        EditText commentBoxTv = (EditText) view.findViewById(R.id.comment_self_et);
        /*final Button publishButton = (Button)view.findViewById(R.id.publish_comment_btn);*/
        commentBoxTv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    if (publishButton.getAlpha() != 0) {
                        fadeOutView(publishButton);
                    }
                } else {
                    if (publishButton.getAlpha() == 0) {
                        fadeInView(publishButton);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        view.findViewById(R.id.share_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);

                // change the type of data you need to share,
                // for image use "image*//*"
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, "Top Model Indo");
                intent.putExtra(Intent.EXTRA_TEXT,
                        "https://play.google.com/store/apps/details?id=com.forest_interactive.topmodelindo");
                startActivity(Intent.createChooser(intent, "Share"));
            }
        });

    }

    private void setViews(View view) {

        TextView video_title = (TextView) view.findViewById(R.id.featured_wallpaper_title);
        TextView video_downloadCount = (TextView) view.findViewById(R.id.featured_wallpaper_downloadcount);
        TextView video_publishDate = (TextView) view.findViewById(R.id.featured_wallpaper_publishdate);
        TextView video_author = (TextView) view.findViewById(R.id.featured_wallpaper_author);
        TextView video_description = (TextView) view.findViewById(R.id.wallpaper_description);
        TextView video_price = (TextView) view.findViewById(R.id.featured_wallpaper_price);
        TextView video_duration = (TextView) view.findViewById(R.id.featured_video_duration);
        SimpleDraweeView video_thumbnail = (SimpleDraweeView) view.findViewById(R.id.wallpaper_thumb_image);
        Button download_btn = (Button) view.findViewById(R.id.video_download_btn);

        video_title.setText(getArguments().getString("video_name"));
        //video_downloadCount.setText(getArguments().getString("total_download") + " Downloads");
        video_downloadCount.setText(getArguments().getString("total_download") + " " + getStringRes(R.string.download_count));
        //video_publishDate.setText("Published on "+getArguments().getString("publishdate"));
        if (!getArguments().getString("publishdate").contains("T")) {
            video_publishDate.setText(getStringRes(R.string.published_on) + " " + getArguments().getString("publishdate"));
        } else {
            video_publishDate.setText(getStringRes(R.string.published_on) + " "
                    + getPublishDateActual(getArguments().getString("publishdate")));
        }
        video_author.setText(getArguments().getString("artist_name"));
        video_description.setText(Html.fromHtml(getArguments().getString("video_description")));
        video_price.setText(getArguments().getString("price"));
        video_duration.setText("");
        ProgressBarDrawable pgd = new ProgressBarDrawable();
        pgd.setColor(Color.parseColor("#80FFFFFF"));
        video_thumbnail.getHierarchy().setProgressBarImage(pgd);
        video_thumbnail.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/" + getArguments().getString("thumbnail")));
        //video_thumbnail.setImageURI(Uri.parse("res:///" + R.drawable.header_01));
        if (isPurchased(getArguments().getString("content_id"))) {
            //fadeOutView(download_btn);
            /*download_btn.setBackground(getActivity().getApplicationContext()
                    .getResources().getDrawable(R.drawable.button_blue));
            download_btn.setText(getStringRes(R.string.download_btn));*/

            download_btn.setVisibility(View.INVISIBLE);
            //video_thumbnail.setClickable(true);
        } else {
            //video_thumbnail.setClickable(false);
            download_btn.setText(getStringRes(R.string.purchase_btn));
        }
    }

    public void formatDate() {
        //Date dateNow = new DateAndTime().getCurrentDateTime();
    }

    private ArrayList<Comment> getData() {
        ArrayList<Comment> results = new ArrayList<Comment>();
        for (int index = 0; index < 5; index++) {
            Comment obj = new Comment();
            obj.setFullname("FullName " + index);
            obj.setUsername("User");
            obj.setDescription("Description");
            obj.setDate_created("Date");
            results.add(index, obj);
        }
        return results;
    }

    public void publishComment(final View view) {
        final EditText comment_et = (EditText) view.findViewById(R.id.comment_self_et);
        String comment = comment_et.getText().toString();
        String userId = global.getUser_data().getUsername();
        String contentId = getArguments().getString("content_id");

        String url = "";
        String LOGIN_TYPE = "";
        if (global.isFacebookLogin()) {
            LOGIN_TYPE = "facebook";
        } else if (global.isGoogleLogin()) {
            LOGIN_TYPE = "gmail";
        } else {
            LOGIN_TYPE = "email";
        }

        try {
            url = "http://210.5.41.103/tuneapp/api/AddComment/" +
                    //"{username}/" +
                    URLEncoder.encode(userId, "UTF-8") + "/" +
                    //"{contentid}/" +
                    URLEncoder.encode(contentId, "UTF-8") + "/" +
                    //"{description}";
                    URLEncoder.encode(new String(Base64.encodeToString(comment.getBytes(), Base64.NO_WRAP)), "UTF-8") + "/" +
                    LOGIN_TYPE;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            showSnack(getStringRes(R.string.error_encoding));
        } catch (NullPointerException e) {
            e.printStackTrace();
            showSnack(getStringRes(R.string.error_unknown));
        }

        Log.e("COMMENT VIDEO", url);

        if (isNetworkAvailable()) {
            client.get(url, new TextHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    crossFadeViews(view.findViewById(R.id.publish_comment_btn), view.findViewById(R.id.publish_comment_progress));
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    if (running) {
                        showSnack(getStringRes(R.string.error_unknown));
                        fadeOutView(view.findViewById(R.id.publish_comment_progress));
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    if (running) {
                        comment_et.setText("");
                        showSnack(getStringRes(R.string.notif_comment_post_success));
                        fadeOutView(view.findViewById(R.id.publish_comment_progress));
                        crossFadeViews(mRecyclerView, view.findViewById(R.id.video_comment_progress));
                        getComments(view);
                    }
                }
            });
        } else {
            showSnack(getStringRes(R.string.error_no_internet));
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        if (global.getLoggedIn() == true) {
            GoogleAnalytics.getInstance(getActivity()).reportActivityStart(getActivity());

            global.getDefaultTracker(TopModelGlobal.TrackerName.APP_TRACKER)
                    .setScreenName("View Video");
            global.getDefaultTracker(TopModelGlobal.TrackerName.APP_TRACKER)
                    .send(new HitBuilders.ScreenViewBuilder().build());
        }

    }

    private void makePurchase(final Dialog dialog, final View view) {
        String url = "";
        String LOGIN_TYPE = "";
        if (global.isFacebookLogin()) {
            LOGIN_TYPE = "facebook";
        } else if (global.isGoogleLogin()) {
            LOGIN_TYPE = "gmail";
        } else {
            LOGIN_TYPE = "email";
        }

        try {
            url = "http://210.5.41.103/tuneapp/api/PurchaseContent/" +
                    //"{username}/" +
                    URLEncoder.encode(global.getUser_data().getUsername(), "UTF-8") + "/" +
                    //"{contentid}";
                    URLEncoder.encode(getArguments().getString("content_id"), "UTF-8") + "/"
                    + LOGIN_TYPE;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            showSnack("Encoding Error");
            return;
        } catch (NullPointerException e) {
            e.printStackTrace();
            showSnack("Error Occured\nPlease try again");
            return;
        }

        Log.e("PURCHASE URL", url);
        if (isNetworkAvailable()) {
            client.get(url, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    //showSnack("Error Purchasing Content\nPlease try again");
                    dialog.dismiss();
                    final Dialog sdialog = new Dialog(getActivity(), R.style.ThemeDialogCustom);
                    sdialog.setContentView(R.layout.dialog_single_btn);
                    TextView message = (TextView) sdialog.findViewById(R.id.dialog_txt);
                    //message.setText("Error Purchasing Content\nPlease try again");
                    message.setText(getStringRes(R.string.error_unknown));
                    sdialog.findViewById(R.id.dialog_onebtn_btn).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            sdialog.dismiss();
                        }
                    });
                    sdialog.show();

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    Log.e("PURCHASE RES", "Sukses : " + responseString);
                    if (running) {
                        Gson gson = new GsonBuilder().create();
                        JsonParser parser = new JsonParser();
                        JsonArray jArray = parser.parse(responseString).getAsJsonArray();

                        ArrayList<PurchaseReturn> responseObj = new ArrayList<PurchaseReturn>();
                        for (JsonElement obj : jArray) {
                            PurchaseReturn temp = gson.fromJson(obj, PurchaseReturn.class);
                            responseObj.add(temp);
                        }

                        Log.e("PURCHASE RESPONSE", "Size array : " + responseObj.size());

                        if (responseObj.size() > 0) {
                            if (responseObj.get(0).getStatus().equalsIgnoreCase("success")) {
                                updateWallet(getArguments().getString("price"), view, dialog);

                                Product product = new Product()
                                        .setId("tmi-vid_" + getArguments().getString("content_id"))
                                        .setName(getArguments().getString("video_name"))
                                        .setCategory("Video")
                                        .setPrice(Integer.parseInt(getArguments().getString("price")))
                                        .setQuantity(1);

                                ProductAction productAction = new ProductAction(ProductAction.ACTION_PURCHASE)
                                        .setTransactionId("TV-" + getArguments().getString("content_id"))
                                        .setTransactionCouponCode("COINS+" + getArguments().getString("price") + "_" +
                                                getArguments().getString("content_id"));

                                HitBuilders.ScreenViewBuilder builder = new HitBuilders.ScreenViewBuilder()
                                        .addImpression(product, "Video")
                                        .setProductAction(productAction);

                                Tracker t = global.getDefaultTracker(TopModelGlobal.TrackerName.ECOMMERCE_TRACKER);
                                t.setScreenName("Buying Video");
                                t.send(builder.build());
                            }
                        } else {
                            Log.e("PURCHASE RESPONSE", "Error false size");
                            dialog.dismiss();
                            final Dialog sdialog = new Dialog(getActivity(), R.style.ThemeDialogCustom);
                            sdialog.setContentView(R.layout.dialog_single_btn);
                            TextView message = (TextView) sdialog.findViewById(R.id.dialog_txt);
                            //message.setText("An Error has occurred, Please try again later");
                            message.setText(R.string.error_unknown);
                            sdialog.findViewById(R.id.dialog_onebtn_btn).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    sdialog.dismiss();
                                }
                            });
                            sdialog.show();
                        }
                    }
                }
            });
        } else {
            dialog.dismiss();
            final Dialog sdialog = new Dialog(getActivity(), R.style.ThemeDialogCustom);
            sdialog.setContentView(R.layout.dialog_single_btn);
            TextView message = (TextView) sdialog.findViewById(R.id.dialog_txt);
            message.setText(getStringRes(R.string.error_no_internet));
            sdialog.findViewById(R.id.dialog_onebtn_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sdialog.dismiss();
                }
            });
            sdialog.show();
        }
    }

    private void updateWallet(final String price, final View view, final Dialog dialog) {
        String url = "";
        String LOGIN_TYPE = "";
        if (global.isGoogleLogin()) {
            LOGIN_TYPE = "gmail";
        } else if (global.isFacebookLogin()) {
            LOGIN_TYPE = "facebook";
        } else {
            LOGIN_TYPE = "email";
        }

        try {
            url = "http://210.5.41.103/tuneapp/api/wallet/" +
                    //"{username}/" +
                    URLEncoder.encode(global.getUser_data().getUsername(), "UTF-8") + "/" +
                    //"{value}";
                    "%2D" +
                    //URLEncoder.encode(getArguments().getString("price"), "UTF-8");
                    URLEncoder.encode(price, "UTF-8") + "/" +
                    "top%20model%20indo/" +
                    LOGIN_TYPE;

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            showSnack(getStringRes(R.string.error_wallet_save));
        }

        Log.e("UPDATE WALLET", url);

        if (isNetworkAvailable()) {
            client.get(url, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    //showSnack("Error Saving Wallet, please try again");
                    dialog.dismiss();
                    final Dialog sdialog = new Dialog(getActivity(), R.style.ThemeDialogCustom);
                    sdialog.setContentView(R.layout.dialog_single_btn);
                    TextView message = (TextView) sdialog.findViewById(R.id.dialog_txt);
                    //message.setText("Error Purchasing Content\nPlease try again");
                    message.setText(getStringRes(R.string.error_unknown));
                    sdialog.findViewById(R.id.dialog_onebtn_btn).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            sdialog.dismiss();
                        }
                    });
                    sdialog.show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    if (running) {
                        int newWallet = Integer.parseInt(global.getUser_data().getWallet()) -
                                //Integer.parseInt(getArguments().getString("price"));
                                Integer.parseInt(price);

                        //Add to downloads, remove download button
                        TextView downloadCount = (TextView) view.findViewById(R.id.featured_wallpaper_downloadcount);
                        int downloadCountnew = Integer.parseInt(getArguments().getString("total_download")) + 1;
                        //downloadCount.setText(String.valueOf(downloadCountnew) + " Downloads");
                        downloadCount.setText(String.valueOf(downloadCountnew) + " " + getStringRes(R.string.download_count));
                        //view.findViewById(R.id.video_download_btn).setVisibility(View.GONE);
                        Button download_btn = (Button) mView.findViewById(R.id.video_download_btn);
                        /*download_btn.setBackground(getActivity().getApplicationContext()
                                .getResources().getDrawable(R.drawable.button_blue));
                        download_btn.setText(getStringRes(R.string.download_btn));*/
                        download_btn.setVisibility(View.INVISIBLE);

                        global.getUser_data().setWallet(String.valueOf(newWallet));

                        //update shared
                        updateShared();

                        //update user data
                        updateSharedPrefUser();

                        view.findViewById(R.id.wallpaper_thumb_image).setClickable(true);
                        dialog.dismiss();
                        final Dialog sdialog = new Dialog(getActivity(), R.style.ThemeDialogCustom);
                        sdialog.setContentView(R.layout.dialog_single_btn);
                        TextView message = (TextView) sdialog.findViewById(R.id.dialog_txt);
                        //message.setText(getArguments().getString("video_name") + " - video purchased successfully!");
                        message.setText(getStringRes(R.string.purchase_done_dialog_1) + " "
                                + getArguments().getString("video_name") + getStringRes(R.string.purchase_done_dialog_2_new));
                        message.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                        sdialog.findViewById(R.id.dialog_onebtn_btn).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                sdialog.dismiss();
                            }
                        });
                        sdialog.show();
                        showSnack(global.getUser_data().getWallet() + " " + getStringRes(R.string.notif_coins_remaining));

                        //update purchases
                        //((MainActivity) getActivity()).updatePurchaseHistory();

                    }
                }
            });
        } else {
            dialog.dismiss();
            final Dialog sdialog = new Dialog(getActivity(), R.style.ThemeDialogCustom);
            sdialog.setContentView(R.layout.dialog_single_btn);
            TextView message = (TextView) sdialog.findViewById(R.id.dialog_txt);
            //message.setText("Error Purchasing Content\nPlease try again");
            message.setText(getStringRes(R.string.error_no_internet));
            sdialog.findViewById(R.id.dialog_onebtn_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sdialog.dismiss();
                }
            });
            sdialog.show();
        }
    }

    private void updateSharedPrefUser() {
        ArrayList<Login> loginList = new ArrayList<Login>();
        loginList.add(global.getUser_data());

        //String json = new Gson().toJson(userData);
        String json = new Gson().toJson(loginList);
        SharedPreferences settings = getActivity().getSharedPreferences("sharedpref", 0);
        settings.edit().putString("user_data", json).commit();
    }

    private void updateShared() {
        SharedPreferences settings = getActivity().getSharedPreferences("sharedpref", 0);
        ArrayList<PurchaseHistory> purchaseObj = new ArrayList<PurchaseHistory>();
        String json = settings.getString("user_purchase_history", "");

        PurchaseHistory newEntry = new PurchaseHistory();
        newEntry.setContent_id(getArguments().getString("content_id"));
        newEntry.setContent_type("Video");
        newEntry.setDate_purchase("000000");
        newEntry.setMedia_name(getArguments().getString("video_name"));
        newEntry.setPrice(getArguments().getString("price"));

        if (!json.equalsIgnoreCase("")) {
            Gson gson = new GsonBuilder().create();
            JsonParser parser = new JsonParser();
            JsonArray jArray = parser.parse(json).getAsJsonArray();

            for (JsonElement obj : jArray) {
                PurchaseHistory temp = gson.fromJson(obj, PurchaseHistory.class);

                if (!checkDupe(purchaseObj, temp)) {
                    purchaseObj.add(temp);
                }
            }
        }

        purchaseObj.add(newEntry);
        String newjson = new Gson().toJson(purchaseObj);
        settings.edit().putString("user_purchase_history", newjson).commit();
        global.setPurchaseHistory(purchaseObj);
    }

    private boolean checkDupe(ArrayList<PurchaseHistory> array, PurchaseHistory obj) {
        if (array.size() > 0) {
            for (int i = 0; i < array.size(); i++) {
                if (array.get(i).getContent_id().equalsIgnoreCase(obj.getContent_id())) {
                    return true;
                }
            }
        } else {
            return false;
        }
        return false;
    }

    private void downloadContentService() {
        ((MainActivity) getActivity()).startDownloadService(getArguments().getString("video_name"),
                getArguments().getString("video_url"));
    }

    /*private void downloadContent(){
        //Toast.makeText(getActivity().getApplicationContext(), "Yo downloading i am", Toast.LENGTH_SHORT).show();
        String downloadurl = "";
        try {
            downloadurl = "http://210.5.41.103/CmsAdminPanel/content/wallpaper/" +
                    //"35868e7f-e9b2-4c6d-b5f2-a5fa84085ce1/DSC_0786.JPG"
                    URLEncoder.encode(getArguments().getString("video_url"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            showSnack(getStringRes(R.string.error_encoding));
        }

        //
        try {
            File root = android.os.Environment.getExternalStorageDirectory();

            File dir = new File (root.getAbsolutePath() + "/xmls");
            if(dir.exists()==false) {
                dir.mkdirs();
            }

            String[] split = getArguments().getString("video_url").split("/");
            String fileName = split[split.length-1];
            URL url = new URL(downloadurl); //you can write here any link
            File file = new File(dir, fileName);

            long startTime = System.currentTimeMillis();
            Log.d("DownloadManager", "download begining");
            Log.d("DownloadManager", "download url:" + url);
            Log.d("DownloadManager", "downloaded file name:" + fileName);

           *//* Open a connection to that URL. *//*
            URLConnection ucon = url.openConnection();

           *//*
            * Define InputStreams to read from the URLConnection.
            *//*
            InputStream is = ucon.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);

           *//*
            * Read bytes to the Buffer until there is nothing more to read(-1).
            *//*
            ByteArrayBuffer baf = new ByteArrayBuffer(5000);
            int current = 0;
            while ((current = bis.read()) != -1) {
                baf.append((byte) current);
            }


           *//* Convert the Bytes read to a String. *//*
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(baf.toByteArray());
            fos.flush();
            fos.close();
            Log.d("DownloadManager", "download ready in" + ((System.currentTimeMillis() - startTime) / 1000) + " sec");

        } catch (IOException e) {
            Log.d("DownloadManager", "Error: " + e);
        }
    }*/

    private void downloadContent() {
        id = (int) (System.currentTimeMillis() & 0xfffffff);
        mNotifyManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(getActivity().getApplicationContext());
        mBuilder.setContentTitle(getArguments().getString("video_name"))
                .setContentText(getStringRes(R.string.notif_downloading_video))
                .setSmallIcon(R.mipmap.ic_launcher);

        //String[] allowedTypes = new String[] { "image/png", "video/MP4" };
        String[] allowedTypes = new String[]{".*"};
        String downloadurl = "";
        try {
            downloadurl = "http://210.5.41.103/CmsAdminPanel/" +
                    //"35868e7f-e9b2-4c6d-b5f2-a5fa84085ce1/DSC_0786.JPG"
                    URLEncoder.encode(getArguments().getString("video_url"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            showSnack(getStringRes(R.string.error_encoding));
        }
        Log.d("error", "url = " + downloadurl);

        final Dialog sdialog = new Dialog(getActivity(), R.style.ThemeDialogCustom);
        sdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        sdialog.setContentView(R.layout.dialog_progress_update);
        TextView dialog_text = (TextView) sdialog.findViewById(R.id.dialog_progress_text);
        dialog_text.setText(getStringRes(R.string.notif_downloading_video));
        final CircularProgressBar circularProgressBar = (CircularProgressBar) sdialog.findViewById(R.id.dialog_progress_bar);
        final int animationDuration = 350;

        client.get(downloadurl, new BinaryHttpResponseHandler(allowedTypes) {
            @Override
            public void onStart() {
                super.onStart();
                //sdialog.show();
                mBuilder.setProgress(100, 0, false);
                mNotifyManager.notify(id, mBuilder.build());
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                super.onProgress(bytesWritten, totalSize);
                /*float progress = bytesWritten/totalSize;
                float progresspercent = progress*100;
                Log.d("progress", "progress +"+progress+"\n"+progresspercent);*/
                circularProgressBar.setProgressWithAnimation((float) ((bytesWritten * 1.0 / totalSize) * 100), animationDuration);

                mBuilder.setProgress(100, (int) ((bytesWritten * 1.0 / totalSize) * 100), false);
                mNotifyManager.notify(id, mBuilder.build());
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] binaryData) {
                OutputStream f = null;
                Uri uri = null;
                try {
                    String root = Environment.getExternalStorageDirectory().toString();
                    File myDir = new File(root + "/Top Model");
                    if (!myDir.exists()) {
                        myDir.mkdirs();
                    }
                    //String fname = "Test.MP4";
                    /*String[] split = getArguments().getString("video_url").split("/");
                    String fname = split[split.length-1];*/
                    String fname = getArguments().getString("video_name") + "." + getFileNameExtension();
                    File file = new File(myDir, fname);
                    if (file.exists()) {
                        file.delete();
                    } else {
                        file.createNewFile();
                    }
                    f = new FileOutputStream(file);
                    f.write(binaryData); //your bytes
                    f.close();
                    Log.d("error", "done");
                    Intent intent =
                            new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    intent.setData(Uri.fromFile(file));
                    getActivity().sendBroadcast(intent);
                    uri = Uri.fromFile(file);

                    sdialog.dismiss();
                    showSnack(getStringRes(R.string.notif_download_complete));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Log.d("error", e.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.d("error", e.toString());
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    Log.d("error", e.toString());
                }

                //Null Pointer to activity here
                Intent fileIntent = new Intent(Intent.ACTION_VIEW);
                fileIntent.setDataAndType(uri, "video/*");
                fileIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                PendingIntent pendingIntent = PendingIntent.getActivity(getActivity().getApplicationContext(), id, fileIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
                //
                mBuilder.setContentIntent(pendingIntent);
                mBuilder.setAutoCancel(true);
                mBuilder.setContentText(getStringRes(R.string.notif_download_complete));
                // Removes the progress bar
                mBuilder.setProgress(0, 0, false);
                mNotifyManager.notify(id, mBuilder.build());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] binaryData, Throwable error) {
                Log.d("error", error.toString() + "\n" + statusCode);
                sdialog.dismiss();
                showSnack(getStringRes(R.string.error_unknown));

                mBuilder.setContentText(getStringRes(R.string.error_unknown));
                // Removes the progress bar
                mBuilder.setProgress(0, 0, false);
                mNotifyManager.notify(id, mBuilder.build());
            }
        });
    }

    private String getFileNameExtension() {
        String[] split = getArguments().getString("video_url").split("\\.");
        String fname = split[split.length - 1];
        return fname;
    }

    //Remove T
    private String getPublishDateActual(String pubdate) {
        //return publishDate;
        String workingdate = pubdate;
        int index = 10;
        StringBuilder sb = new StringBuilder(workingdate);
        //sb.deleteCharAt(index);
        //return sb.toString();
        String[] split = workingdate.split("T");
        return split[0];
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
