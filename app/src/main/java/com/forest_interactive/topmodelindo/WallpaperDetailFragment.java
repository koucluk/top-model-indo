package com.forest_interactive.topmodelindo;

import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.drawable.ProgressBarDrawable;
import com.facebook.drawee.view.SimpleDraweeView;
import com.forest_interactive.topmodelindo.Adapters.CommentRecyclerViewAdapter;
import com.forest_interactive.topmodelindo.Models.Comment;
import com.forest_interactive.topmodelindo.Models.Login;
import com.forest_interactive.topmodelindo.Models.PurchaseHistory;
import com.forest_interactive.topmodelindo.Models.PurchaseReturn;
import com.forest_interactive.topmodelindo.Widgets.CustomBaseFragment;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.BinaryHttpResponseHandler;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link WallpaperDetailFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link WallpaperDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WallpaperDetailFragment extends CustomBaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    TopModelGlobal global;
    AsyncHttpClient client;
    View mView;
    int id = 1;
    private ArrayList<Comment> commentObj;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private String sWallet;
    private boolean running;
    private NotificationManager mNotifyManager;
    private Builder mBuilder;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public WallpaperDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment WallpaperDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static WallpaperDetailFragment newInstance(String param1, String param2) {
        WallpaperDetailFragment fragment = new WallpaperDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_wallpaper_detail, container, false);
        //View view = inflater.inflate(R.layout.fragment_wallpaper_detail, container, false);
        final View view = inflater.inflate(R.layout.fragment_wallpaper_detail, container, false);

        mView = view;

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        global = (TopModelGlobal) getActivity().getApplicationContext();
        running = true;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setToolBarTitle(getStringRes(R.string.title_wallpaper));
        client = new AsyncHttpClient();
        client.setTimeout(40000);

        initOnClickListeners(mView);

        setViews(mView);

        getComments(mView);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        running = false;

    }

    @Override
    public void onPause() {
        super.onPause();
        client.cancelAllRequests(true);
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    public void initOnClickListeners(final View view) {
        //final String sWallet = global.getUser_data().getWallet();
        //final String sPrice = getArguments().getString("price");

        if (global.getLoggedIn()) {
            sWallet = global.getUser_data().getWallet();
        } else {
            sWallet = "0";
        }

        view.findViewById(R.id.wallpaper_downloadbtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isPurchased(getArguments().getString("content_id"))) {
                    try {
                        //if (global.getUser_data().getWallet().isEmpty()) {
                        if (!global.getLoggedIn()) {
                            //showSnack("Please Login to Download Content");
                            //showSnack(getStringRes(R.string.error_login_to_download));
                            showSnack(getStringRes(R.string.error_login_to_download),
                                    getStringRes(R.string.title_login),
                                    new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            changeFragment(new LoginFragment(), "login");
                                        }
                                    });
                        } else if ((Integer.parseInt(global.getUser_data().getWallet()) >=
                                Integer.parseInt(getArguments().getString("price"))) &&
                                !isPurchased(getArguments().getString("content_id"))) {

                            final Dialog tdialog = new Dialog(getActivity(), R.style.ThemeDialogCustom);
                            tdialog.setContentView(R.layout.dialog_red_green_btn);
                            //No
                            tdialog.findViewById(R.id.dialog_twobtn_no).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    tdialog.dismiss();
                                }
                            });
                            //Yes
                            tdialog.findViewById(R.id.dialog_twobtn_yes).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //tdialog.dismiss();
                                    //Purchase method
                                    crossFadeViews(tdialog.findViewById(R.id.dialog_content), tdialog.findViewById(R.id.dialog_progress));
                                    makePurchase(tdialog, view);
                                }
                            });

                        /*SpannableString ss = new SpannableString("Anda akan membeli konten ini dengan (C) 8," +
                                " pastikan koin Anda mencukupi");*/
                            SpannableString ss = new SpannableString(getStringRes(R.string.purchase_dialog_1) + " " +
                                    getArguments().getString("price") + " " + getStringRes(R.string.purchase_dialog_2));
                            Drawable d = getResources().getDrawable(R.drawable.icon_coin_01);
                            d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                            ImageSpan span = new ImageSpan(d, ImageSpan.ALIGN_BASELINE);
                            int coinIndex = ss.toString().indexOf("(");
                            ss.setSpan(span, coinIndex, coinIndex + 3, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                            TextView dialogText = (TextView) tdialog.findViewById(R.id.dialog_txt);
                            dialogText.setText(ss);
                            tdialog.show();
                            //} else if (!(Integer.parseInt(global.getUser_data().getWallet()) >=
                        } else if (!(Integer.parseInt(sWallet) >=
                                Integer.parseInt(getArguments().getString("price")))) {
                            //showSnack("Insufficient Coins\nPlease Top Up Your Wallet");
                            showSnack(getStringRes(R.string.error_insufficient_coins));
                        } else if (isPurchased(getArguments().getString("content_id"))) {
                            fadeOutView(v);
                            //download stuff?
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                        /*Toast.makeText(getActivity().getApplicationContext(),
                                "Number Format Exception\n" + e.toString(), Toast.LENGTH_SHORT).show();*/
                        showSnack(getStringRes(R.string.error_unknown));
                    } catch (Resources.NotFoundException e) {
                        e.printStackTrace();
                        /*Toast.makeText(getActivity().getApplicationContext(),
                                "Resources Not Found Exception\n" + e.toString(), Toast.LENGTH_SHORT).show();*/
                        showSnack(getStringRes(R.string.error_unknown));
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                        //showSnack("Please Login to Download Content");
                        showSnack(getStringRes(R.string.error_login_to_download));
                    }
                } else {
                    //downloadContent();
                    downloadContentService();
                    //testFileName();
                }
            }

        });

        final Button publishButton = (Button) view.findViewById(R.id.publish_comment_btn);
        publishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (publishButton.getAlpha() != 0 && global.getUser_data().getUsername() == null) {
                        //showSnack("Please Login");
                        showSnack(getStringRes(R.string.error_login_to_comment));
                    } else if (publishButton.getAlpha() != 0) {
                        //fadeOutView(publishButton);
                        publishComment(view);
                    }
                } catch (NullPointerException e) {
                    //showSnack("Please Login to Post Comments");
                    showSnack(getStringRes(R.string.error_login_to_comment));
                    e.printStackTrace();
                }
            }
        });

        EditText commentBoxTv = (EditText) view.findViewById(R.id.comment_self_et);
        commentBoxTv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    if (publishButton.getAlpha() != 0) {
                        fadeOutView(publishButton);
                    }
                } else {
                    if (publishButton.getAlpha() == 0) {
                        fadeInView(publishButton);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        //Show Full Screen Wallpaper
        view.findViewById(R.id.wallpaper_thumb_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getActivity(), R.style.ThemeDialogCustom);
                dialog.setContentView(R.layout.dialog_wallpaper_preview);

                SimpleDraweeView preview_image = (SimpleDraweeView) dialog.findViewById(R.id.wallpaper_preview_image);
                final RelativeLayout closeLayout = (RelativeLayout) dialog.findViewById(R.id.close_layout);

                closeLayout.setAlpha(0.0f);

                preview_image.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/" +
                        getArguments().getString("wallpaper_url")));
                preview_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (closeLayout.getAlpha() == 0) {
                            closeLayout.animate()
                                    .alpha(1.0f)
                                    .start();
                        } else {
                            closeLayout.animate()
                                    .alpha(0.0f)
                                    .start();
                        }
                    }
                });
                closeLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (closeLayout.getAlpha() == 1) {
                            dialog.dismiss();
                        }
                    }
                });

                if (isPurchased(getArguments().getString("content_id"))) {
                    dialog.show();
                } else {
                    //showSnack("Please purchase the content to view");
                    showSnack(getStringRes(R.string.error_purchase_content_to_view));
                }


            }
        });

        view.findViewById(R.id.share_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);

                // change the type of data you need to share,
                // for image use "image*//*"
                //intent.setType("text/plain");

                intent.setType("text/plain");
                /*intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("http://210.5.41.103/CmsAdminPanel/" +
                        getArguments().getString("wallpaper_url")));*/
                intent.putExtra(Intent.EXTRA_SUBJECT, "Top Model Indo");
                intent.putExtra(Intent.EXTRA_TEXT,
                        "https://play.google.com/store/apps/details?id=com.forest_interactive.topmodelindo");
                startActivity(Intent.createChooser(intent, "Share"));


                /*Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_TEXT,
                        "https://play.google.com/store/apps/details?id=com.forest_interactive.topmodelindo");
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("http://210.5.41.103/CmsAdminPanel/" +
                        getArguments().getString("wallpaper_url")));
                shareIntent.setType("image*//*");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(shareIntent, "Share"));*/
            }
        });

    }

    private void setViews(View view) {
        //can remove try catch later
        try {
            if (!getArguments().getString("content_id").equalsIgnoreCase("")) {
                TextView wallpaper_title = (TextView) view.findViewById(R.id.featured_wallpaper_title);
                TextView wallpaper_downloads = (TextView) view.findViewById(R.id.featured_wallpaper_downloadcount);
                TextView wallpaper_publishDate = (TextView) view.findViewById(R.id.featured_wallpaper_publishdate);
                TextView wallpaper_price = (TextView) view.findViewById(R.id.featured_wallpaper_price);
                TextView wallpaper_author = (TextView) view.findViewById(R.id.featured_wallpaper_author);
                TextView wallpaper_description = (TextView) view.findViewById(R.id.wallpaper_description);
                SimpleDraweeView wallpaper_thumbnail = (SimpleDraweeView) view.findViewById(R.id.wallpaper_thumb_image);
                Button download_btn = (Button) view.findViewById(R.id.wallpaper_downloadbtn);

                wallpaper_title.setText(getArguments().getString("wallpaper_name"));
                //wallpaper_downloads.setText(getArguments().getString("total_download") + " Downloads");
                wallpaper_downloads.setText(getArguments().getString("total_download") + " "
                        + getStringRes(R.string.download_count));
                if (!getArguments().getString("publishdate").contains("T")) {
                    wallpaper_publishDate.setText(getArguments().getString("publishdate"));
                } else {
                    wallpaper_publishDate.setText(getPublishDateActual(getArguments().getString("publishdate")));
                }
                wallpaper_price.setText(getArguments().getString("price"));
                wallpaper_author.setText(getArguments().getString("artist_name"));
                wallpaper_description.setText(getArguments().getString("wallpaper_description"));


                ProgressBarDrawable pgd = new ProgressBarDrawable();
                pgd.setColor(Color.parseColor("#80FFFFFF"));

                wallpaper_thumbnail.getHierarchy().setProgressBarImage(pgd);
                /*wallpaper_thumbnail.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/" +
                        getArguments().getString("wallpaper_url")));*/
                if (isPurchased(getArguments().getString("content_id"))) {
                    //fadeOutView(download_btn);
                    //wallpaper_thumbnail.setClickable(true);
                    wallpaper_thumbnail.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/" +
                            getArguments().getString("wallpaper_url")));

                    /*download_btn.setBackground(getActivity().getApplicationContext()
                            .getResources().getDrawable(R.drawable.button_blue));
                    download_btn.setText(getStringRes(R.string.download_btn));*/

                    download_btn.setVisibility(View.INVISIBLE);
                } else {
                    //wallpaper_thumbnail.setClickable(false);
                    /*wallpaper_thumbnail.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/" +
                            getArguments().getString("thumbnail_url")));*/
                    wallpaper_thumbnail.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/" +
                            getArguments().getString("wallpaper_url")));

                    Log.e("WALLPAPER IMAGE", getArguments().getString("wallpaper_url"));
                    setWaterMark(wallpaper_thumbnail, true);
                    download_btn.setText(getStringRes(R.string.purchase_btn));

                }

            }
        } catch (NullPointerException e) {
            //Do something
        }
    }

    public void getComments(final View view) {
        String url = "";
        try {
            url = "http://210.5.41.103/tuneapp/api/RetrieveUserComment/" +
                    URLEncoder.encode(getArguments().getString("content_id")+"/top%20model%20indo", "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            //showSnack("Encoding Error");
            showSnack(getStringRes(R.string.error_encoding));
            return;
        } catch (NullPointerException e) {
            //showSnack("Error Retrieving Comments");
            showSnack(getStringRes(R.string.error_unknown));
            return;
        }


        if (isNetworkAvailable()) {
            client.get(url, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    //Toast.makeText(getActivity().getApplicationContext(), "Failed", Toast.LENGTH_SHORT).show();
                    if (running) {
                        TextView error_tv = (TextView) mView.findViewById(R.id.wallpaper_comment_error_tv);
                        error_tv.setText(getStringRes(R.string.error_unknown));
                        crossFadeViews(mView.findViewById(R.id.wallpaper_comment_progress),
                                mView.findViewById(R.id.wallpaper_comment_error_tv));
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    //Toast.makeText(getActivity().getApplicationContext(), ""+responseString, Toast.LENGTH_SHORT).show();
                    if (running) {
                        Gson gson = new GsonBuilder().create();
                        JsonParser parser = new JsonParser();
                        JsonArray jArray = parser.parse(responseString).getAsJsonArray();

                        commentObj = new ArrayList<Comment>();
                        for (JsonElement obj : jArray) {
                            Comment temp = gson.fromJson(obj, Comment.class);
                            commentObj.add(temp);
                        }
                        mAdapter = new CommentRecyclerViewAdapter(commentObj);
                        //mAdapter = new CommentRecyclerViewAdapter(getData());
                        mRecyclerView.setAdapter(mAdapter);
                        ((CommentRecyclerViewAdapter) mAdapter).setOnItemClickListener(new CommentRecyclerViewAdapter.ListClickListener() {
                            @Override
                            public void onItemClick(int position, View v) {
                                //Toast.makeText(getActivity().getApplicationContext(), "Clicked " + position, Toast.LENGTH_SHORT).show();
                            }
                        });
                        //Toast.makeText(getActivity().getApplicationContext(), "" + commentObj.get(0).getFullname(), Toast.LENGTH_SHORT).show();
                        crossFadeViews(view.findViewById(R.id.wallpaper_comment_progress), mRecyclerView);

                        //Disable RecyclerView Scroll - Set Static Height
                        RelativeLayout commentContainer = (RelativeLayout) view.findViewById(R.id.comment_holder);
                        ViewGroup.LayoutParams params = commentContainer.getLayoutParams();
                        params.height = convertToDp(70 * commentObj.size());
                        commentContainer.setLayoutParams(params);
                    }
                }
            });
        } else {
            TextView error_tv = (TextView) mView.findViewById(R.id.wallpaper_comment_error_tv);
            error_tv.setText(getStringRes(R.string.error_no_internet));
            crossFadeViews(mView.findViewById(R.id.wallpaper_comment_progress),
                    mView.findViewById(R.id.wallpaper_comment_error_tv));
        }
        mRecyclerView = (RecyclerView) view.findViewById(R.id.wallpaper_comment_rv);
        mRecyclerView.setHasFixedSize(false);
        mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

    }

    public void publishComment(final View view) {
        final EditText comment_et = (EditText) view.findViewById(R.id.comment_self_et);
        String comment = comment_et.getText().toString();
        //String userId = global.getUser_data().getUsername();
        String contentId = getArguments().getString("content_id");

        String url = "";
        String LOGIN_TYPE = "";
        if (global.isFacebookLogin()) {
            LOGIN_TYPE = "facebook";
        } else if (global.isGoogleLogin()) {
            LOGIN_TYPE = "gmail";
        } else {
            LOGIN_TYPE = "email";
        }

        try {
            url = "http://210.5.41.103/tuneapp/api/AddComment/" +
                    //"{username}/" +
                    URLEncoder.encode(global.getUser_data().getUsername(), "UTF-8") + "/" +
                    //"{contentid}/" +
                    URLEncoder.encode(contentId, "UTF-8") + "/" +
                    //"{description}";
                    URLEncoder.encode(comment, "UTF-8") + "/" +
                    LOGIN_TYPE;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            //showSnack("Encoding Error\nPlease try again");
            showSnack(getStringRes(R.string.error_encoding));
            return;
        } catch (NullPointerException e) {
            e.printStackTrace();
            //showSnack("Error Occured\nPlease try again");
            showSnack(getStringRes(R.string.error_unknown));
            return;
        }

        //AsyncHttpClient client = new AsyncHttpClient();
        if (isNetworkAvailable()) {
            client.get(url, new TextHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    crossFadeViews(view.findViewById(R.id.publish_comment_btn), view.findViewById(R.id.publish_comment_progress));
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    //showSnack("Error Posting Comment\nPlease try again");
                    if (running) {
                        showSnack(getStringRes(R.string.error_unknown));
                        fadeOutView(view.findViewById(R.id.publish_comment_progress));
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    if (running) {
                        comment_et.setText("");
                        //showSnack("Comment Posted Successfully");
                        showSnack(getStringRes(R.string.notif_comment_post_success));
                        fadeOutView(view.findViewById(R.id.publish_comment_progress));
                        crossFadeViews(mRecyclerView, view.findViewById(R.id.wallpaper_comment_progress));
                        getComments(view);
                    }
                }
            });
        } else {
            showSnack(getStringRes(R.string.error_no_internet));
        }

    }

    @Override
    public void onStart() {
        super.onStart();

        if (global.getLoggedIn() == true) {
            GoogleAnalytics.getInstance(getActivity()).reportActivityStart(getActivity());

            global.getDefaultTracker(TopModelGlobal.TrackerName.APP_TRACKER)
                    .setScreenName("View Wallpaper");
            global.getDefaultTracker(TopModelGlobal.TrackerName.APP_TRACKER)
                    .send(new HitBuilders.ScreenViewBuilder().build());
        }
    }

    private void makePurchase(final Dialog dialog, final View view) {
        String url = "";
        String LOGIN_TYPE = "";
        if (global.isFacebookLogin()) {
            LOGIN_TYPE = "facebook";
        } else if (global.isGoogleLogin()) {
            LOGIN_TYPE = "gmail";
        } else {
            LOGIN_TYPE = "email";
        }

        try {
            url = "http://210.5.41.103/tuneapp/api/PurchaseContent/" +
                    //"{username}/" +
                    URLEncoder.encode(global.getUser_data().getUsername(), "UTF-8") + "/" +
                    //"{contentid}";
                    URLEncoder.encode(getArguments().getString("content_id"), "UTF-8") + "/"
                    + LOGIN_TYPE;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            //showSnack("Encoding Error");
            showSnack(getStringRes(R.string.error_encoding));
            return;
        } catch (NullPointerException e) {
            //showSnack("Error Occured\nPlease try again");
            showSnack(getStringRes(R.string.error_unknown));
            return;
        }

        Log.e("PURCHASE URL", "url : " + url);
        if (isNetworkAvailable()) {
            client.get(url, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    //showSnack("Error Purchasing Content\nPlease try again");
                    dialog.dismiss();
                    final Dialog sdialog = new Dialog(getActivity(), R.style.ThemeDialogCustom);
                    sdialog.setContentView(R.layout.dialog_single_btn);
                    TextView message = (TextView) sdialog.findViewById(R.id.dialog_txt);
                    //message.setText("Error Purchasing Content\nPlease try again");
                    message.setText(getStringRes(R.string.error_unknown));
                    sdialog.findViewById(R.id.dialog_onebtn_btn).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            sdialog.dismiss();
                        }
                    });
                    sdialog.show();

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    if (running) {
                        Gson gson = new GsonBuilder().create();
                        JsonParser parser = new JsonParser();
                        JsonArray jArray = parser.parse(responseString).getAsJsonArray();

                        ArrayList<PurchaseReturn> responseObj = new ArrayList<PurchaseReturn>();
                        for (JsonElement obj : jArray) {
                            PurchaseReturn temp = gson.fromJson(obj, PurchaseReturn.class);
                            responseObj.add(temp);
                        }

                        if (responseObj.size() > 0) {
                            if (responseObj.get(0).getStatus().equalsIgnoreCase("success")) {
                                updateWallet(getArguments().getString("price"), view, dialog);

                                Product product = new Product()
                                        .setId(getArguments().getString("content_id"))
                                        .setName(getArguments().getString("wallpaper_name"))
                                        .setCategory("Wallpaper")
                                        .setPrice(Integer.parseInt(getArguments().getString("price")))
                                        .setQuantity(1);

                                ProductAction productAction = new ProductAction(ProductAction.ACTION_PURCHASE)
                                        .setTransactionId("TW-" + getArguments().getString("content_id"))
                                        .setTransactionCouponCode("COINS+" + getArguments().getString("price") + "_" +
                                                getArguments().getString("content_id"));

                                HitBuilders.ScreenViewBuilder builder = new HitBuilders.ScreenViewBuilder()
                                        .addImpression(product, "Wallpaper")
                                        .setProductAction(productAction);

                                Tracker t = global.getDefaultTracker(TopModelGlobal.TrackerName.ECOMMERCE_TRACKER);
                                t.setScreenName("Buy Wallpaper");
                                t.send(builder.build());
                            }
                        } else {
                            final Dialog sdialog = new Dialog(getActivity(), R.style.ThemeDialogCustom);
                            sdialog.setContentView(R.layout.dialog_single_btn);
                            TextView message = (TextView) sdialog.findViewById(R.id.dialog_txt);
                            //message.setText("An Error has occurred, Please try again later");
                            message.setText(getStringRes(R.string.error_unknown));
                            sdialog.findViewById(R.id.dialog_onebtn_btn).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    sdialog.dismiss();
                                }
                            });
                            sdialog.show();
                        }
                    }
                }
            });
        } else {
            dialog.dismiss();
            final Dialog sdialog = new Dialog(getActivity(), R.style.ThemeDialogCustom);
            sdialog.setContentView(R.layout.dialog_single_btn);
            TextView message = (TextView) sdialog.findViewById(R.id.dialog_txt);
            //message.setText("Error Purchasing Content\nPlease try again");
            message.setText(getStringRes(R.string.error_no_internet));
            sdialog.findViewById(R.id.dialog_onebtn_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sdialog.dismiss();
                }
            });
            sdialog.show();
        }
    }

    private void updateWallet(final String price, final View view, final Dialog dialog) {
        String url = "";
        String LOGIN_TYPE = "";

        if (global.isGoogleLogin()) {
            LOGIN_TYPE = "gmail";
        } else if (global.isFacebookLogin()) {
            LOGIN_TYPE = "facebook";
        } else {
            LOGIN_TYPE = "email";
        }

        try {
            url = "http://210.5.41.103/tuneapp/api/wallet/" +
                    //"{username}/" +
                    URLEncoder.encode(global.getUser_data().getUsername(), "UTF-8") + "/" +
                    //"{value}";
                    "-" +
                    //URLEncoder.encode(getArguments().getString("price"), "UTF-8");
                    URLEncoder.encode(price, "UTF-8") + "/" +
                    "top%20model%20indo/" +
                    LOGIN_TYPE;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            //showSnack("Error Saving Wallet, please try again");
            showSnack(getStringRes(R.string.error_wallet_save));
        }

        Log.e("WALLPAPER BUY", "url : " + url);
        if (isNetworkAvailable()) {
            client.get(url, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    //showSnack("Error Saving Wallet, please try again");
                    dialog.dismiss();
                    final Dialog sdialog = new Dialog(getActivity(), R.style.ThemeDialogCustom);
                    sdialog.setContentView(R.layout.dialog_single_btn);
                    TextView message = (TextView) sdialog.findViewById(R.id.dialog_txt);
                    message.setText(getStringRes(R.string.error_unknown));
                    sdialog.findViewById(R.id.dialog_onebtn_btn).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            sdialog.dismiss();
                        }
                    });
                    sdialog.show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    if (running) {
                        int newWallet = Integer.parseInt(global.getUser_data().getWallet()) -
                                //Integer.parseInt(getArguments().getString("price"));
                                Integer.parseInt(price);
                        //Add to downloads, remove download button
                        TextView downloadCount = (TextView) view.findViewById(R.id.featured_wallpaper_downloadcount);
                        int downloadCountnew = Integer.parseInt(getArguments().getString("total_download")) + 1;
                        //downloadCount.setText(String.valueOf(downloadCountnew) + " Downloads");
                        downloadCount.setText(String.valueOf(downloadCountnew) + " "
                                + getStringRes(R.string.download_count));
                        //view.findViewById(R.id.wallpaper_downloadbtn).setVisibility(View.GONE);
                        Button download_btn = (Button) mView.findViewById(R.id.wallpaper_downloadbtn);
                        /*download_btn.setBackground(getActivity().getApplicationContext()
                                .getResources().getDrawable(R.drawable.button_blue));*/
                        download_btn.setVisibility(View.INVISIBLE);

                        global.getUser_data().setWallet(String.valueOf(newWallet));

                        //update shared
                        updateShared();

                        //update user data
                        updateSharedPrefUser();

                        view.findViewById(R.id.wallpaper_thumb_image).setClickable(true);
                        dialog.dismiss();
                        final Dialog sdialog = new Dialog(getActivity(), R.style.ThemeDialogCustom);
                        sdialog.setContentView(R.layout.dialog_single_btn);
                        TextView message = (TextView) sdialog.findViewById(R.id.dialog_txt);
                        //message.setText(getArguments().getString("wallpaper_name") + " - wallpaper purchased successfully!");
                        message.setText(getStringRes(R.string.purchase_done_dialog_1) + " "
                                + getArguments().getString("wallpaper_name") + getStringRes(R.string.purchase_done_dialog_2_new));
                        message.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                        sdialog.findViewById(R.id.dialog_onebtn_btn).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                sdialog.dismiss();
                            }
                        });
                        sdialog.show();
                        showSnack(global.getUser_data().getWallet() + " " + getStringRes(R.string.notif_coins_remaining));

                        //update purchases
                        //((MainActivity) getActivity()).updatePurchaseHistory();

                    }
                }
            });
        } else {
            dialog.dismiss();
            final Dialog sdialog = new Dialog(getActivity(), R.style.ThemeDialogCustom);
            sdialog.setContentView(R.layout.dialog_single_btn);
            TextView message = (TextView) sdialog.findViewById(R.id.dialog_txt);
            //message.setText("Error Purchasing Content\nPlease try again");
            message.setText(getStringRes(R.string.error_no_internet));
            sdialog.findViewById(R.id.dialog_onebtn_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sdialog.dismiss();
                }
            });
            sdialog.show();
        }

    }

    private void updateSharedPrefUser() {
        ArrayList<Login> loginList = new ArrayList<Login>();
        loginList.add(global.getUser_data());

        //String json = new Gson().toJson(userData);
        String json = new Gson().toJson(loginList);
        SharedPreferences settings = getActivity().getSharedPreferences("sharedpref", 0);
        settings.edit().putString("user_data", json).commit();
    }

    private void updateShared() {
        SharedPreferences settings = getActivity().getSharedPreferences("sharedpref", 0);
        ArrayList<PurchaseHistory> purchaseObj = new ArrayList<PurchaseHistory>();

        PurchaseHistory newEntry = new PurchaseHistory();
        newEntry.setContent_id(getArguments().getString("content_id"));
        newEntry.setContent_type("Wallpaper");
        newEntry.setDate_purchase("000000");
        newEntry.setMedia_name(getArguments().getString("wallpaper_name"));
        newEntry.setPrice(getArguments().getString("price"));

        String json = settings.getString("user_purchase_history", "");
        if (!json.equalsIgnoreCase("")) {
            Gson gson = new GsonBuilder().create();
            JsonParser parser = new JsonParser();
            JsonArray jArray = parser.parse(json).getAsJsonArray();


            for (JsonElement obj : jArray) {
                PurchaseHistory temp = gson.fromJson(obj, PurchaseHistory.class);

                if (!checkDupe(purchaseObj, temp)) {
                    purchaseObj.add(temp);
                }
            }
        }

        purchaseObj.add(newEntry);
        String newjson = new Gson().toJson(purchaseObj);
        settings.edit().putString("user_purchase_history", newjson).commit();
        global.setPurchaseHistory(purchaseObj);
        setViews(mView);

    }

    private boolean checkDupe(ArrayList<PurchaseHistory> array, PurchaseHistory obj) {
        if (array.size() > 0) {
            for (int i = 0; i < array.size(); i++) {
                if (array.get(i).getContent_id().equalsIgnoreCase(obj.getContent_id())) {
                    return true;
                }
            }
        } else {
            return false;
        }
        return false;
    }

    private void downloadContentService() {
        ((MainActivity) getActivity()).startDownloadService(getArguments().getString("wallpaper_name"),
                getArguments().getString("wallpaper_url"));
    }

    private void downloadContent() {
        id = (int) (System.currentTimeMillis() & 0xfffffff);
        mNotifyManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(getActivity().getApplicationContext());
        mBuilder.setContentTitle(getArguments().getString("wallpaper_name"))
                .setContentText(getStringRes(R.string.notif_downloading_wallpaper))
                .setSmallIcon(R.mipmap.ic_launcher);


        String[] allowedTypes = new String[]{".*"};
        String downloadurl = "";
        try {
            downloadurl = "http://210.5.41.103/CmsAdminPanel/" +
                    URLEncoder.encode(getArguments().getString("wallpaper_url"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            showSnack(getStringRes(R.string.error_encoding));
        }
        Log.d("error", "url = " + downloadurl);

        final Dialog sdialog = new Dialog(getActivity(), R.style.ThemeDialogCustom);
        sdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        sdialog.setContentView(R.layout.dialog_progress_update);
        TextView dialog_text = (TextView) sdialog.findViewById(R.id.dialog_progress_text);
        dialog_text.setText(getStringRes(R.string.notif_downloading_wallpaper));
        final CircularProgressBar circularProgressBar = (CircularProgressBar) sdialog.findViewById(R.id.dialog_progress_bar);
        final int animationDuration = 350;

        client.get(downloadurl, new BinaryHttpResponseHandler(allowedTypes) {
            @Override
            public void onStart() {
                super.onStart();

                mBuilder.setProgress(100, 0, false);
                mNotifyManager.notify(id, mBuilder.build());

                //sdialog.show();
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                super.onProgress(bytesWritten, totalSize);
                circularProgressBar.setProgressWithAnimation((float) ((bytesWritten * 1.0 / totalSize) * 100), animationDuration);

                mBuilder.setProgress(100, (int) ((bytesWritten * 1.0 / totalSize) * 100), false);
                mNotifyManager.notify(id, mBuilder.build());
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] binaryData) {
                OutputStream f = null;
                Uri uri = null;
                try {
                    String root = Environment.getExternalStorageDirectory().toString();
                    //File myDir = new File(root + "/topmodel");
                    File myDir = new File(root + "/Pictures/Top Model");
                    if (!myDir.exists()) {
                        myDir.mkdirs();
                    }
                    //String fname = "Test.MP4";
                    /*String[] split = getArguments().getString("wallpaper_url").split("/");
                    String fname = split[split.length-1];*/
                    String fname = getArguments().getString("wallpaper_name") + "." + getFileNameExtension();
                    File file = new File(myDir, fname);
                    if (file.exists()) {
                        file.delete();
                    } else {
                        file.createNewFile();
                    }
                    f = new FileOutputStream(file);
                    f.write(binaryData); //your bytes
                    f.close();
                    Log.d("error", "done");
                    Intent intent =
                            new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    intent.setData(Uri.fromFile(file));
                    getActivity().sendBroadcast(intent);
                    uri = Uri.fromFile(file);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Log.d("error", e.toString());
                    showSnack(getStringRes(R.string.error_unknown));
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.d("error", e.toString());
                    showSnack(getStringRes(R.string.error_unknown));
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    Log.d("error", e.toString());
                }
                sdialog.dismiss();
                showSnack(getStringRes(R.string.notif_download_complete));

                Intent fileIntent = new Intent(Intent.ACTION_VIEW);
                if (getFileNameExtension().equalsIgnoreCase("gif")) {
                    Log.d("fileformat", "gif");
                    fileIntent.setDataAndType(uri, "image/gif");
                } else if (getFileNameExtension().equalsIgnoreCase("jpg") ||
                        getFileNameExtension().equalsIgnoreCase("jpeg") ||
                        getFileNameExtension().equalsIgnoreCase("png")) {
                    Log.d("fileformat", "jpeg");
                    fileIntent.setDataAndType(uri, "image/jpeg");
                    //fileIntent.setDataAndType(uri, "image/*");
                }
                fileIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                PendingIntent pendingIntent = PendingIntent.getActivity(getActivity().getApplicationContext(), id, fileIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
                mBuilder.setContentIntent(pendingIntent);
                mBuilder.setAutoCancel(true);
                mBuilder.setContentText(getStringRes(R.string.notif_download_complete));
                mBuilder.setProgress(0, 0, false);
                mNotifyManager.notify(id, mBuilder.build());

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] binaryData, Throwable error) {
                Log.d("error", error.toString() + "\n" + statusCode);
                sdialog.dismiss();
                showSnack(getStringRes(R.string.error_unknown));

                mBuilder.setContentText(getStringRes(R.string.error_unknown));
                // Removes the progress bar
                mBuilder.setProgress(0, 0, false);
                mNotifyManager.notify(id, mBuilder.build());
            }
        });
    }

    private String getFileNameExtension() {
        String[] split = getArguments().getString("wallpaper_url").split("\\.");
        String fname = split[split.length - 1];
        return fname;
    }

    //Remove T
    private String getPublishDateActual(String pubdate) {
        //return publishDate;
        String workingdate = pubdate;
        int index = 10;
        StringBuilder sb = new StringBuilder(workingdate);
        //sb.deleteCharAt(index);
        //sb.delete(10, workingdate.length());
        String[] parts = workingdate.split("T");
        //return sb.toString();
        return parts[0];
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
