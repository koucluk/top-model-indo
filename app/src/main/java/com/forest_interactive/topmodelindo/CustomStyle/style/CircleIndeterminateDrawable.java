package com.forest_interactive.topmodelindo.CustomStyle.style;

import android.animation.ValueAnimator;

import com.forest_interactive.topmodelindo.CustomStyle.sprite.CircleSpriteGroup;
import com.forest_interactive.topmodelindo.CustomStyle.sprite.CircleSprite;
import com.forest_interactive.topmodelindo.CustomStyle.sprite.Sprite;
import com.forest_interactive.topmodelindo.CustomStyle.animation.SpriteAnimatorBuilder;

/**
 * Created by abdulla.hainum on 24/3/2016.
 */
public class CircleIndeterminateDrawable extends CircleSpriteGroup {

    @Override
    public Sprite[] onCreateChild() {
        Dot[] dots = new Dot[12];
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new Dot();
            dots[i].setAnimationDelay(1200 / 12 * i + -1200);
        }
        return dots;
    }

    class Dot extends CircleSprite {
        @Override
        public ValueAnimator getAnimation() {
            float fractions[] = new float[]{0f, 0.5f, 1f};
            return new SpriteAnimatorBuilder(this).
                    scale(fractions, 0f, 1f, 0f).
                    duration(1200).
                    easeInOut(fractions)
                    .build();
        }
    }
}
