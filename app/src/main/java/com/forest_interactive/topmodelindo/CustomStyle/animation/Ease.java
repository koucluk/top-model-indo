package com.forest_interactive.topmodelindo.CustomStyle.animation;

/**
 * Created by abdulla.hainum on 24/3/2016.
 */
import android.view.animation.Interpolator;

public class Ease {
    public static Interpolator inOut() {
        return PathInterpolatorCompat.create(0.42f, 0f, 0.58f, 1f);
    }
}
