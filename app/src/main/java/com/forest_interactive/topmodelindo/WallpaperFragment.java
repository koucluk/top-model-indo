package com.forest_interactive.topmodelindo;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.forest_interactive.topmodelindo.Adapters.WallpaperRecyclerViewAdapter;
import com.forest_interactive.topmodelindo.Models.Wallpaper;
import com.forest_interactive.topmodelindo.Widgets.CustomBaseFragment;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

import cz.msebera.android.httpclient.Header;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link WallpaperFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link WallpaperFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WallpaperFragment extends CustomBaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    ArrayList<Wallpaper> wallpaperObj;
    ArrayList<Wallpaper> recObj;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    TopModelGlobal global;

    AsyncHttpClient client;

    View mView;

    private boolean running;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public WallpaperFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment WallpaperFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static WallpaperFragment newInstance(String param1, String param2) {
        WallpaperFragment fragment = new WallpaperFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_wallpaper, container, false);
        fragmentManager = getActivity().getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        //View view = inflater.inflate(R.layout.fragment_wallpaper, container, false);
        final View view = inflater.inflate(R.layout.fragment_wallpaper, container, false);

        mView = view;

        return view;
    }

    private void initOnClick(final View view){
        /*RelativeLayout topWallpaperContainer = (RelativeLayout)view.findViewById(R.id.topwallpaper_container);
        topWallpaperContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                *//*fragmentTransaction
                        .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                        .replace(R.id.fragment_container, new WallpaperDetailFragment()).commit();*//*
                changeFragment(new WallpaperDetailFragment(), "wallpaperdetail");
            }
        });*/

        Toolbar tb = (Toolbar)getActivity().findViewById(R.id.toolbar);
        RelativeLayout title_container = (RelativeLayout) tb.findViewById(R.id.toolbar_title_container);
        title_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getActivity().getApplicationContext(), "works", Toast.LENGTH_SHORT).show();
                PopupMenu popup = new PopupMenu(getActivity().getApplicationContext(), v);
                popup.getMenuInflater()
                        .inflate(R.menu.sort_dropdown, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case (R.id.bydate):
                                //Toast.makeText(getActivity().getApplicationContext(), "bydate", Toast.LENGTH_SHORT).show();
                                sortByDate();
                                break;
                            case (R.id.bydownload):
                                //Toast.makeText(getActivity().getApplicationContext(), "bydl", Toast.LENGTH_SHORT).show();
                                sortByDownloads();
                                break;
                            default:
                                //This shouldn't get called
                                Toast.makeText(getActivity().getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                                break;
                        }
                        return true;
                    }
                });

                popup.show();
            }
        });
    }

    private void setUpViews(final View view){
        final SharedPreferences settings = getActivity().getSharedPreferences("sharedpref", 0);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.wallpaper_rv);
        mRecyclerView.setHasFixedSize(false);
        mLayoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 2);
        mRecyclerView.setLayoutManager(mLayoutManager);

        if (settings.getString("wallpaper_data", "").equalsIgnoreCase("")) {
            if (isNetworkAvailable()) {
                client.get("http://210.5.41.103/tuneapp/api/Wallpaper/top%20model%20indo", new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        if (running) {
                            //Toast.makeText(getActivity().getApplicationContext(), "Fail "+statusCode, Toast.LENGTH_SHORT).show();
                            //showSnack("Error Occurred");
                            //showSnack(getStringRes(R.string.error_unknown));
                            final TextView error_tv = (TextView) view.findViewById(R.id.top_wallpaper_error_tv);
                            error_tv.setText(getStringRes(R.string.error_no_internet));
                            crossFadeViews(view.findViewById(R.id.top_wallpaper_progress), error_tv);
                        }

                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        if (running) {
                            Gson gson = new GsonBuilder().create();
                            JsonParser parser = new JsonParser();
                            JsonArray jArray = parser.parse(responseString).getAsJsonArray();

                            wallpaperObj = new ArrayList<Wallpaper>();

                            for (JsonElement obj : jArray) {
                                Wallpaper temp = gson.fromJson(obj, Wallpaper.class);
                                wallpaperObj.add(temp);
                            }

                            settings.edit().putString("wallpaper_data", responseString).commit();

                            //mAdapter = new WallpaperRecyclerViewAdapter(wallpaperObj);
                            //mAdapter = new WallpaperRecyclerViewAdapter(wallpaperObj, global.getPurchaseHistory());

                            //sorting
                            sortByDate();
                            //sortByDownloads();

                    /*mAdapter = new WallpaperRecyclerViewAdapter(wallpaperObj, global.getPurchaseHistory(),
                            (MainActivity)getActivity());*/
                            //mAdapter = new WallpaperRecyclerViewAdapter(getDataSet());
                            //mRecyclerView.setAdapter(mAdapter);

                            ((WallpaperRecyclerViewAdapter) mAdapter).setOnItemClickListener
                                    (new WallpaperRecyclerViewAdapter.ListClickListener() {
                                        @Override
                                        public void onItemClick(int position, View v) {
                                            Bundle arg = new Bundle();
                                            arg.putString("content_id", wallpaperObj.get(position).getContent_id());
                                            arg.putString("artist_name", wallpaperObj.get(position).getArtist_name());
                                            arg.putString("wallpaper_name", wallpaperObj.get(position).getWallpaper_name());
                                            arg.putString("wallpaper_url", wallpaperObj.get(position).getWallpaper_url());
                                            arg.putString("wallpaper_description", wallpaperObj.get(position).getWallpaper_description());
                                            arg.putString("publishdate", wallpaperObj.get(position).getPublishDate());
                                            arg.putString("total_download", wallpaperObj.get(position).getTotal_download());
                                            arg.putString("total_count", wallpaperObj.get(position).getTotal_count());
                                            arg.putString("price", wallpaperObj.get(position).getPrice());
                                            arg.putString("thumbnail_url", wallpaperObj.get(position).getThumbnail());

                                            changeFragment(new WallpaperDetailFragment(), "wallpaperdetail", arg);

                                        }
                                    });

                            crossFadeViews(view.findViewById(R.id.video_progress), mRecyclerView);

                            //Disable RecyclerView Scroll - Set Static Height
                            RelativeLayout commentContainer = (RelativeLayout) view.findViewById(R.id.wallpaper_rv_container);
                            ViewGroup.LayoutParams params = commentContainer.getLayoutParams();
                            //double divide = 5/2.0;
                            double divide = wallpaperObj.size() / 2.0;
                            int factor = (int) Math.ceil(divide);
                            //Toast.makeText(getActivity().getApplicationContext(), ""+factor, Toast.LENGTH_SHORT).show();
                            //params.height = convertToDp(250*wallpaperObj.size());
                            params.height = convertToDp(260 * factor);
                            commentContainer.setLayoutParams(params);

                        }
                    }
                });
            } else {
                //showSnack("No Internet Connection Detected");
                fadeOutView(view.findViewById(R.id.video_progress));
            }
        } else {
            String json = settings.getString("wallpaper_data", "");
            Gson gson = new GsonBuilder().create();
            JsonParser parser = new JsonParser();
            JsonArray jArray = parser.parse(json).getAsJsonArray();

            wallpaperObj = new ArrayList<Wallpaper>();

            for (JsonElement obj : jArray) {
                Wallpaper temp = gson.fromJson(obj, Wallpaper.class);
                wallpaperObj.add(temp);
            }

            //sorting
            sortByDate();

            ((WallpaperRecyclerViewAdapter) mAdapter).setOnItemClickListener
                    (new WallpaperRecyclerViewAdapter.ListClickListener() {
                        @Override
                        public void onItemClick(int position, View v) {
                            Bundle arg = new Bundle();
                            arg.putString("content_id", wallpaperObj.get(position).getContent_id());
                            arg.putString("artist_name", wallpaperObj.get(position).getArtist_name());
                            arg.putString("wallpaper_name", wallpaperObj.get(position).getWallpaper_name());
                            arg.putString("wallpaper_url", wallpaperObj.get(position).getWallpaper_url());
                            arg.putString("wallpaper_description", wallpaperObj.get(position).getWallpaper_description());
                            arg.putString("publishdate", wallpaperObj.get(position).getPublishDate());
                            arg.putString("total_download", wallpaperObj.get(position).getTotal_download());
                            arg.putString("total_count", wallpaperObj.get(position).getTotal_count());
                            arg.putString("price", wallpaperObj.get(position).getPrice());
                            arg.putString("thumbnail_url", wallpaperObj.get(position).getThumbnail());

                            changeFragment(new WallpaperDetailFragment(), "wallpaperdetail", arg);

                        }
                    });

            crossFadeViews(view.findViewById(R.id.video_progress), mRecyclerView);

            //Disable RecyclerView Scroll - Set Static Height
            RelativeLayout commentContainer = (RelativeLayout) view.findViewById(R.id.wallpaper_rv_container);
            ViewGroup.LayoutParams params = commentContainer.getLayoutParams();
            //double divide = 5/2.0;
            double divide = wallpaperObj.size() / 2.0;
            int factor = (int) Math.ceil(divide);
            params.height = convertToDp(260 * factor);
            commentContainer.setLayoutParams(params);

            doBackgroundWallpaperGet(view);
        }

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        global = (TopModelGlobal) getActivity().getApplicationContext();
        running = true;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        client = new AsyncHttpClient();
        client.setTimeout(40000);
        setToolBarTitle(getStringRes(R.string.title_wallpaper));
        setUpViews(mView);

        //initOnClick(view);

        setupRecommendedWallpaper(mView);
        initOnClick(mView);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        client.cancelAllRequests(true);

        running = false;
    }

    @Override
    public void onResume(){
        super.onResume();
        Toolbar tb = (Toolbar)getActivity().findViewById(R.id.toolbar);
        tb.findViewById(R.id.toolbar_triangle).setVisibility(View.VISIBLE);
    }

    @Override
    public void onPause(){
        super.onPause();
        Toolbar tb = (Toolbar)getActivity().findViewById(R.id.toolbar);
        RelativeLayout title_container = (RelativeLayout) tb.findViewById(R.id.toolbar_title_container);
        tb.findViewById(R.id.toolbar_triangle).setVisibility(View.GONE);
        title_container.setOnClickListener(null);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    //Testing
    private ArrayList<Wallpaper> getDataSet() {
        ArrayList results = new ArrayList<Wallpaper>();
        for (int index = 0; index < 5; index++) {
            Wallpaper obj = new Wallpaper();
            obj.setContent_id("" + index);
            obj.setWallpaper_name("Test " + index);
            obj.setWallpaper_description("Description");
            obj.setArtist_name("Artist");
            obj.setTotal_download("1000");
            obj.setTotal_count("9000");
            obj.setPublishDate("Date");

            results.add(obj);
        }

        return results;
    }

    private void setupRecommendedWallpaper(final View view){
        final TextView error_tv = (TextView) view.findViewById(R.id.top_wallpaper_error_tv);
        final SharedPreferences settings = getActivity().getSharedPreferences("sharedpref", 0);
        if (settings.getString("rec_wallpaper_data", "").equalsIgnoreCase("")) {
            if (isNetworkAvailable()) {
                //client.get("http://210.5.41.103/tuneapp/api/FeaturedWallpaper/top%20model%20indo/1", new TextHttpResponseHandler() {
                client.get("http://210.5.41.103/tuneapp/api/FeaturedWallpaper/top%20model%20indo/3", new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        if (running) {
                            //Toast.makeText(getActivity().getApplicationContext(), "Failed", Toast.LENGTH_SHORT).show();

                            if (statusCode == 503) {
                        /*error_tv.setText("An error has occured\n" +
                                "Please check your internet connection or contact your service provider");*/
                                error_tv.setText(getStringRes(R.string.error_no_internet));
                                crossFadeViews(view.findViewById(R.id.top_wallpaper_progress), error_tv);
                            } else {
                                error_tv.setText(getStringRes(R.string.error_unknown));
                                crossFadeViews(view.findViewById(R.id.top_wallpaper_progress), error_tv);
                            }
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        if (running) {
                            Gson gson = new GsonBuilder().create();
                            JsonParser parser = new JsonParser();
                            JsonArray jArray = parser.parse(responseString).getAsJsonArray();

                            settings.edit().putString("rec_wallpaper_data", responseString).commit();

                            recObj = new ArrayList<Wallpaper>();
                            for (JsonElement obj : jArray) {
                                Wallpaper temp = gson.fromJson(obj, Wallpaper.class);
                                recObj.add(temp);
                            }

                            if (recObj.size() > 0) {
                                SimpleDraweeView featuredWallpaperImg = (SimpleDraweeView) view.findViewById(R.id.wallpaper_thumb_image);
                                TextView featuredWallpaperTitle = (TextView) view.findViewById(R.id.topwallpaper_title);
                                TextView featuredWallpaperArtist = (TextView) view.findViewById(R.id.topwallpaper_author);
                                TextView featuredWallpaperDate = (TextView) view.findViewById(R.id.topwallpaper_publishdate);
                                TextView featuredWallpaperDownloadCount = (TextView) view.findViewById(R.id.topwallpaper_downloadcount);
                                TextView featuredWallpaperPrice = (TextView) view.findViewById(R.id.topwallpaper_price);
                                RelativeLayout topWallpaperLayout = (RelativeLayout) view.findViewById(R.id.topwallpaper_container);

                                if (isPurchased(recObj.get(0).getContent_id())) {
                                    featuredWallpaperImg.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/" +
                                            recObj.get(0).getWallpaper_url()));
                                } else {
                                /*featuredWallpaperImg.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/" +
                                        recObj.get(0).getThumbnail()));*/
                                    featuredWallpaperImg.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/"
                                            + recObj.get(0).getWallpaper_url()));
                                    setWaterMark(featuredWallpaperImg, true);
                                }

                        /*featuredWallpaperImg.getHierarchy().setControllerOverlay(getActivity().getResources()
                                .getDrawable(R.drawable.overlay_landscape));*/


                                featuredWallpaperTitle.setText(recObj.get(0).getWallpaper_name());
                                featuredWallpaperArtist.setText("by " + recObj.get(0).getArtist_name());
                                featuredWallpaperDate.setText(recObj.get(0).getPublishDate());
                                //featuredWallpaperDownloadCount.setText(recObj.get(0).getTotal_download() + " Downloads");
                                featuredWallpaperDownloadCount.setText(recObj.get(0).getTotal_download() + " "
                                        + getStringRes(R.string.download_count));
                                //featuredWallpaperPrice.setText(recObj.get(0).getPrice());
                                featuredWallpaperPrice.setText(recObj.get(0).getPrice());
                                //topWallpaperLayout.setOnClickListener(new View.OnClickListener() {
                                featuredWallpaperImg.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Bundle arg = new Bundle();
                                        arg.putString("content_id", recObj.get(0).getContent_id());
                                        arg.putString("artist_name", recObj.get(0).getArtist_name());
                                        arg.putString("wallpaper_name", recObj.get(0).getWallpaper_name());
                                        arg.putString("wallpaper_url", recObj.get(0).getWallpaper_url());
                                        arg.putString("wallpaper_description", Html.fromHtml(recObj.get(0).getWallpaper_description()).toString().trim());
                                        arg.putString("publishdate", recObj.get(0).getPublishDate());
                                        arg.putString("total_download", recObj.get(0).getTotal_download());
                                        arg.putString("total_count", recObj.get(0).getTotal_count());
                                        arg.putString("price", recObj.get(0).getPrice());
                                        arg.putString("thumbnail_url", recObj.get(0).getThumbnail());

                                        changeFragment(new WallpaperDetailFragment(), "wallpaperdetail", arg);
                                    }
                                });
                                //crossFadeViews(view.findViewById(R.id.top_wallpaper_progress), topWallpaperLayout);
                                crossFadeViews(view.findViewById(R.id.top_wallpaper_progress), view.findViewById(R.id.top_wallpaper_image_container));
                            } else {
                                crossFadeViews(view.findViewById(R.id.top_wallpaper_progress), view.findViewById(R.id.top_wallpaper_error_tv));
                            }

                        }
                    }
                });
            } else {
            /*error_tv.setText("An error has occured\n" +
                    "Please check your internet connection or contact your service provider");*/
                error_tv.setText(getStringRes(R.string.error_no_internet));
                crossFadeViews(view.findViewById(R.id.top_wallpaper_progress), error_tv);
            }
        } else {
            String json = settings.getString("rec_wallpaper_data", "");
            Gson gson = new GsonBuilder().create();
            JsonParser parser = new JsonParser();
            JsonArray jArray = parser.parse(json).getAsJsonArray();

            recObj = new ArrayList<Wallpaper>();
            for (JsonElement obj : jArray) {
                Wallpaper temp = gson.fromJson(obj, Wallpaper.class);
                recObj.add(temp);
            }

            if (recObj.size() > 0) {
                SimpleDraweeView featuredWallpaperImg = (SimpleDraweeView) view.findViewById(R.id.wallpaper_thumb_image);
                TextView featuredWallpaperTitle = (TextView) view.findViewById(R.id.topwallpaper_title);
                TextView featuredWallpaperArtist = (TextView) view.findViewById(R.id.topwallpaper_author);
                TextView featuredWallpaperDate = (TextView) view.findViewById(R.id.topwallpaper_publishdate);
                TextView featuredWallpaperDownloadCount = (TextView) view.findViewById(R.id.topwallpaper_downloadcount);
                TextView featuredWallpaperPrice = (TextView) view.findViewById(R.id.topwallpaper_price);
                RelativeLayout topWallpaperLayout = (RelativeLayout) view.findViewById(R.id.topwallpaper_container);

                if (isPurchased(recObj.get(0).getContent_id())) {
                    featuredWallpaperImg.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/" +
                            recObj.get(0).getWallpaper_url()));
                } else {
                                /*featuredWallpaperImg.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/" +
                                        recObj.get(0).getThumbnail()));*/
                    featuredWallpaperImg.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/"
                            + recObj.get(0).getWallpaper_url()));
                    setWaterMark(featuredWallpaperImg, true);
                }

                        /*featuredWallpaperImg.getHierarchy().setControllerOverlay(getActivity().getResources()
                                .getDrawable(R.drawable.overlay_landscape));*/


                featuredWallpaperTitle.setText(recObj.get(0).getWallpaper_name());
                featuredWallpaperArtist.setText("by " + recObj.get(0).getArtist_name());
                featuredWallpaperDate.setText(recObj.get(0).getPublishDate());
                //featuredWallpaperDownloadCount.setText(recObj.get(0).getTotal_download() + " Downloads");
                featuredWallpaperDownloadCount.setText(recObj.get(0).getTotal_download() + " "
                        + getStringRes(R.string.download_count));
                //featuredWallpaperPrice.setText(recObj.get(0).getPrice());
                featuredWallpaperPrice.setText(recObj.get(0).getPrice());
                //topWallpaperLayout.setOnClickListener(new View.OnClickListener() {
                featuredWallpaperImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Bundle arg = new Bundle();
                        arg.putString("content_id", recObj.get(0).getContent_id());
                        arg.putString("artist_name", recObj.get(0).getArtist_name());
                        arg.putString("wallpaper_name", recObj.get(0).getWallpaper_name());
                        arg.putString("wallpaper_url", recObj.get(0).getWallpaper_url());
                        arg.putString("wallpaper_description", Html.fromHtml(recObj.get(0).getWallpaper_description()).toString().trim());
                        arg.putString("publishdate", recObj.get(0).getPublishDate());
                        arg.putString("total_download", recObj.get(0).getTotal_download());
                        arg.putString("total_count", recObj.get(0).getTotal_count());
                        arg.putString("price", recObj.get(0).getPrice());
                        arg.putString("thumbnail_url", recObj.get(0).getThumbnail());

                        changeFragment(new WallpaperDetailFragment(), "wallpaperdetail", arg);
                    }
                });
                //crossFadeViews(view.findViewById(R.id.top_wallpaper_progress), topWallpaperLayout);
                crossFadeViews(view.findViewById(R.id.top_wallpaper_progress), view.findViewById(R.id.top_wallpaper_image_container));
                doBackgroundRecommendedWallpaper(view);
            }
        }
    }

    private void doBackgroundWallpaperGet(final View view){
        if (isNetworkAvailable()){
            client.get("http://210.5.41.103/tuneapp/api/Wallpaper/top%20model%20indo", new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    if (running) {
                        Gson gson = new GsonBuilder().create();
                        JsonParser parser = new JsonParser();
                        JsonArray jArray = parser.parse(responseString).getAsJsonArray();

                        wallpaperObj = new ArrayList<Wallpaper>();

                        for (JsonElement obj : jArray) {
                            Wallpaper temp = gson.fromJson(obj, Wallpaper.class);
                            wallpaperObj.add(temp);
                        }
                        SharedPreferences settings = getActivity().getSharedPreferences("sharedpref", 0);
                        settings.edit().putString("wallpaper_data", responseString).commit();



                        //sorting
                        sortByDate();

                        ((WallpaperRecyclerViewAdapter) mAdapter).setOnItemClickListener
                                (new WallpaperRecyclerViewAdapter.ListClickListener() {
                                    @Override
                                    public void onItemClick(int position, View v) {
                                        Bundle arg = new Bundle();
                                        arg.putString("content_id", wallpaperObj.get(position).getContent_id());
                                        arg.putString("artist_name", wallpaperObj.get(position).getArtist_name());
                                        arg.putString("wallpaper_name", wallpaperObj.get(position).getWallpaper_name());
                                        arg.putString("wallpaper_url", wallpaperObj.get(position).getWallpaper_url());
                                        arg.putString("wallpaper_description", wallpaperObj.get(position).getWallpaper_description());
                                        arg.putString("publishdate", wallpaperObj.get(position).getPublishDate());
                                        arg.putString("total_download", wallpaperObj.get(position).getTotal_download());
                                        arg.putString("total_count", wallpaperObj.get(position).getTotal_count());
                                        arg.putString("price", wallpaperObj.get(position).getPrice());
                                        arg.putString("thumbnail_url", wallpaperObj.get(position).getThumbnail());

                                        changeFragment(new WallpaperDetailFragment(), "wallpaperdetail", arg);

                                    }
                                });

                        crossFadeViews(view.findViewById(R.id.video_progress), mRecyclerView);

                        //Disable RecyclerView Scroll - Set Static Height
                        RelativeLayout commentContainer = (RelativeLayout) view.findViewById(R.id.wallpaper_rv_container);
                        ViewGroup.LayoutParams params = commentContainer.getLayoutParams();
                        //double divide = 5/2.0;
                        double divide = wallpaperObj.size() / 2.0;
                        int factor = (int) Math.ceil(divide);

                        params.height = convertToDp(260 * factor);
                        commentContainer.setLayoutParams(params);
                    }

                }
            });

        }
    }

    private void doBackgroundRecommendedWallpaper(final View view){
        final SharedPreferences settings = getActivity().getSharedPreferences("sharedpref", 0);
        if (isNetworkAvailable()){
            client.get("http://210.5.41.103/tuneapp/api/FeaturedWallpaper/top%20model%20indo/3", new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    if (running) {
                        Gson gson = new GsonBuilder().create();
                        JsonParser parser = new JsonParser();
                        JsonArray jArray = parser.parse(responseString).getAsJsonArray();

                        settings.edit().putString("rec_wallpaper_data", responseString).commit();

                        recObj = new ArrayList<Wallpaper>();
                        for (JsonElement obj : jArray) {
                            Wallpaper temp = gson.fromJson(obj, Wallpaper.class);
                            recObj.add(temp);
                        }

                        if (recObj.size() > 0) {
                            SimpleDraweeView featuredWallpaperImg = (SimpleDraweeView) view.findViewById(R.id.wallpaper_thumb_image);
                            TextView featuredWallpaperTitle = (TextView) view.findViewById(R.id.topwallpaper_title);
                            TextView featuredWallpaperArtist = (TextView) view.findViewById(R.id.topwallpaper_author);
                            TextView featuredWallpaperDate = (TextView) view.findViewById(R.id.topwallpaper_publishdate);
                            TextView featuredWallpaperDownloadCount = (TextView) view.findViewById(R.id.topwallpaper_downloadcount);
                            TextView featuredWallpaperPrice = (TextView) view.findViewById(R.id.topwallpaper_price);
                            RelativeLayout topWallpaperLayout = (RelativeLayout) view.findViewById(R.id.topwallpaper_container);

                            if (isPurchased(recObj.get(0).getContent_id())) {
                                featuredWallpaperImg.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/" +
                                        recObj.get(0).getWallpaper_url()));
                            } else {
                                /*featuredWallpaperImg.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/" +
                                        recObj.get(0).getThumbnail()));*/
                                featuredWallpaperImg.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/"
                                        + recObj.get(0).getWallpaper_url()));
                                setWaterMark(featuredWallpaperImg, true);
                            }

                        /*featuredWallpaperImg.getHierarchy().setControllerOverlay(getActivity().getResources()
                                .getDrawable(R.drawable.overlay_landscape));*/


                            featuredWallpaperTitle.setText(recObj.get(0).getWallpaper_name());
                            featuredWallpaperArtist.setText("by " + recObj.get(0).getArtist_name());
                            featuredWallpaperDate.setText(recObj.get(0).getPublishDate());
                            //featuredWallpaperDownloadCount.setText(recObj.get(0).getTotal_download() + " Downloads");
                            featuredWallpaperDownloadCount.setText(recObj.get(0).getTotal_download() + " "
                                    + getStringRes(R.string.download_count));
                            //featuredWallpaperPrice.setText(recObj.get(0).getPrice());
                            featuredWallpaperPrice.setText(recObj.get(0).getPrice());
                            //topWallpaperLayout.setOnClickListener(new View.OnClickListener() {
                            featuredWallpaperImg.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Bundle arg = new Bundle();
                                    arg.putString("content_id", recObj.get(0).getContent_id());
                                    arg.putString("artist_name", recObj.get(0).getArtist_name());
                                    arg.putString("wallpaper_name", recObj.get(0).getWallpaper_name());
                                    arg.putString("wallpaper_url", recObj.get(0).getWallpaper_url());
                                    arg.putString("wallpaper_description", Html.fromHtml(recObj.get(0).getWallpaper_description()).toString().trim());
                                    arg.putString("publishdate", recObj.get(0).getPublishDate());
                                    arg.putString("total_download", recObj.get(0).getTotal_download());
                                    arg.putString("total_count", recObj.get(0).getTotal_count());
                                    arg.putString("price", recObj.get(0).getPrice());
                                    arg.putString("thumbnail_url", recObj.get(0).getThumbnail());

                                    changeFragment(new WallpaperDetailFragment(), "wallpaperdetail", arg);
                                }
                            });
                            //crossFadeViews(view.findViewById(R.id.top_wallpaper_progress), topWallpaperLayout);
                            crossFadeViews(view.findViewById(R.id.top_wallpaper_progress), view.findViewById(R.id.top_wallpaper_image_container));
                        }
                    }
                }
            });
        }
    }

    private void sortByDate(){
        Collections.sort(wallpaperObj, new Comparator<Wallpaper>() {
            @Override
            public int compare(/*Wallpaper lhs, Wallpaper rhs*/ Wallpaper rhs, Wallpaper lhs) {
                //return 0;
                return lhs.getPublishDateFormatted().compareTo(rhs.getPublishDateFormatted());
            }
        });
        mAdapter = new WallpaperRecyclerViewAdapter(wallpaperObj, global.getPurchaseHistory(),
                (MainActivity)getActivity());
        mRecyclerView.setAdapter(mAdapter);
    }

    private void sortByDownloads(){
        Collections.sort(wallpaperObj, new Comparator<Wallpaper>() {
            @Override
            public int compare(Wallpaper lhs, Wallpaper rhs) {
                //return 0;
                return lhs.getDownloadCountInt() > rhs.getDownloadCountInt() ? -1 : 1;
            }
        });

        mAdapter = new WallpaperRecyclerViewAdapter(wallpaperObj, global.getPurchaseHistory(),
                (MainActivity)getActivity());
        mRecyclerView.setAdapter(mAdapter);
    }

}
