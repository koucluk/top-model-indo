package com.forest_interactive.topmodelindo.Utils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.forest_interactive.topmodelindo.MainActivity;
import com.forest_interactive.topmodelindo.R;
import com.google.android.gms.gcm.GcmListenerService;

/**
 * Created by abdulla.hainum on 22/4/2016.
 */
public class GcmIntentService extends GcmListenerService {
    @Override
    public void onMessageReceived(String from, Bundle data) {
        //String message = data.getString("message");
        String type = data.getString("content_type");
        String content = data.getString("content_data");
        String title=  "";
        if (type.equalsIgnoreCase("video")){
            title = data.getString("video_name");
        } else if (type.equalsIgnoreCase("wallpaper")){
            title = data.getString("wallpaper_name");
        }

        int uniqueInt = (int) (System.currentTimeMillis() & 0xfffffff);

        Intent notificationIntent = new Intent(this, MainActivity.class);
        //notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notificationIntent.putExtra("sport_id", "sport"+uniqueInt);
        notificationIntent.putExtra("data", data);
        notificationIntent.setAction(String.valueOf(uniqueInt));

        PendingIntent pendingIntent = PendingIntent.getActivity(this, uniqueInt, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationManager notificationManager= (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Top Model")
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .setContentText(title);
        notificationManager.notify(uniqueInt, mBuilder.build());
        Log.d("Notification", ""+type+"\n"+content);
    }

    //
    /*private void sendNotification(String message) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 *//* Request code *//*, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_stat_ic_notification)
                .setContentTitle("GCM Message")
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 *//* ID of notification *//*, notificationBuilder.build());
    }*/
}
