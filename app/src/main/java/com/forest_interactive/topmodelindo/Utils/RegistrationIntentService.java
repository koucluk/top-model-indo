package com.forest_interactive.topmodelindo.Utils;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.forest_interactive.topmodelindo.R;
import com.forest_interactive.topmodelindo.TopModelGlobal;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.Base64;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import cz.msebera.android.httpclient.Header;

/**
 * Created by abdulla.hainum on 22/4/2016.
 */
public class RegistrationIntentService extends IntentService {
    private static final String TAG = "RegIntentService";

    AsyncHttpClient client;
    TopModelGlobal global;

    public RegistrationIntentService() {
        super(TAG);
        global = (TopModelGlobal) getApplicationContext();
        client = new AsyncHttpClient();
        client.setTimeout(40000);
    }

    @Override
    public void onHandleIntent(Intent intent) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        InstanceID instanceID = InstanceID.getInstance(this);
        try {
            String token = instanceID.getToken(getString(R.string.sender_id),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
//            sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, true).apply();

            Toast.makeText(RegistrationIntentService.this, "Token " + token, Toast.LENGTH_LONG).show();
            Log.d("TOKEN", token);

            sendRegistrationToServer(token);
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(RegistrationIntentService.this, "IOException in service", Toast.LENGTH_SHORT).show();
        }


    }

    private void sendRegistrationToServer(final String token) {
        // Add custom implementation, as needed.
        String LOGIN_TYPE;

        if (global.isGoogleLogin()) {
            LOGIN_TYPE = "gmail";
        } else if (global.isFacebookLogin()) {
            LOGIN_TYPE = "facebook";
        } else {
            LOGIN_TYPE = "email";
        }

        String email = global.getUser_data().getUsername();
//        http://210.5.41.103/tuneapp/api/UpdateNotificationPush/email/status/base64token/reg_type
        String url = null;
        try {
            url = "http://210.5.41.103/tuneapp/api/UpdateNotificationPush/" +
                    //"{username}/" +
                    URLEncoder.encode(email, "UTF-8") + "/true" +
                    //"{description}";
                    URLEncoder.encode(new String(Base64.encodeToString(token.getBytes(), Base64.NO_WRAP)), "UTF-8") + "/" +
                    LOGIN_TYPE;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Log.d("Sent Token", token);
            }
        });
    }
}
