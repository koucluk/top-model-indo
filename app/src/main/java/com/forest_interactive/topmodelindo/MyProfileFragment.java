package com.forest_interactive.topmodelindo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.forest_interactive.topmodelindo.Widgets.CustomBaseFragment;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyProfileFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyProfileFragment extends CustomBaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    TopModelGlobal global;

    private View mView;

    private GoogleSignInAccount acct;

    private OnFragmentInteractionListener mListener;

    public MyProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyProfileFragment newInstance(String param1, String param2) {
        MyProfileFragment fragment = new MyProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_my_profile, container, false);
        View view = inflater.inflate(R.layout.fragment_my_profile, container, false);

        mView = view;

        setUpOnClickListeners(view);

        setViews();
        setToolBarTitle(getStringRes(R.string.title_my_profile));

        SharedPreferences settings = getActivity().getSharedPreferences("sharedpref", 0);
        //Toast.makeText(getActivity().getApplicationContext(), ""+settings.getString("locale", "in"), Toast.LENGTH_SHORT).show();

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        global = (TopModelGlobal) getActivity().getApplicationContext();

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        setProfilePicture();
    }

    private void setProfilePicture(){
        SimpleDraweeView profile_pic = (SimpleDraweeView) mView.findViewById(R.id.main_profile_pic);

        if (global.isFacebookLogin()) {
            profile_pic.setImageURI(Uri.parse(global.getFacebook_profile_pic_url()));
        } else if(global.isGoogleLogin()) {
            //Toast.makeText(MainActivity.this, "Google Set Profile Triggered", Toast.LENGTH_SHORT).show();
            acct = ((MainActivity)getActivity()).getAcct();
            try {
                if (acct.getPhotoUrl().toString().length() > 0) {
                    profile_pic.setImageURI(acct.getPhotoUrl());
                } else {
                    profile_pic.setImageURI(Uri.parse("res:///" + R.drawable.avatar_default));
                }
            } catch (NullPointerException e) {
                profile_pic.setImageURI(Uri.parse("res:///" + R.drawable.avatar_default));
            }
        } else {
            profile_pic.setImageURI(Uri.parse("res:///" + R.drawable.avatar_default));
        }

        RoundingParams roundingParams = RoundingParams.fromCornersRadius(5f);
        roundingParams.setBorder(Color.WHITE, 5.0f);
        roundingParams.setRoundAsCircle(true);
        profile_pic.getHierarchy().setRoundingParams(roundingParams);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void osetUpOnClickListeners(View view){
        //OnClicks
        //Notification
        view.findViewById(R.id.notification_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(getActivity().getApplicationContext(), NotificationActivity.class));
                //showSnack("Hi Novan, I'm SnackBar");
            }
        });

        //EditProfile
        view.findViewById(R.id.edit_profile_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragment(new EditProfileFragment(), "editprofile");
            }
        });

        //MyVideo
        view.findViewById(R.id.my_video_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragment(new MyVideoFragment(), "myvideo");
            }
        });

        //My Wallpaper
        view.findViewById(R.id.my_wallpaper_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragment(new MyWallpaperFragment(), "mywallpaper");
            }
        });

        //BuyCoin
        view.findViewById(R.id.buy_coin_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragment(new CoinStoreFragment(), "coinstore");
            }
        });

        //ContactUs
        view.findViewById(R.id.contact_us).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragment(new ContactUsFragment(), "contactus");
            }
        });

        //Free Coin
        view.findViewById(R.id.free_coin_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).showadColony();
            }
        });

        //Rate Us
        view.findViewById(R.id.rate_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent playDirect = new Intent(Intent.ACTION_VIEW);
                playDirect.setData(Uri.parse("market://"));*/
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + "com.forest_interactive.topmodelindo")));
            }
        });

        //Help & About
        view.findViewById(R.id.help_and_about_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragment(new HelpAboutFragment(), "helpabout");
            }
        });

    }

    public void setUpOnClickListeners(View view){
        //Edit Profile
        view.findViewById(R.id.edit_profile_layout).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP){
                    delayedFragmentChange(new EditProfileFragment(), "editprofile");
                }
                return false;
            }
        });

        //My Video
        view.findViewById(R.id.my_video_layout).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP){
                    delayedFragmentChange(new MyVideoFragment(), "myvideo");
                }
                return false;
            }
        });

        //My Wallpaper
        view.findViewById(R.id.my_wallpaper_layout).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP){
                    delayedFragmentChange(new MyWallpaperFragment(), "mywallpaper");
                }
                return false;
            }
        });

        //Buy Coin
        view.findViewById(R.id.buy_coin_layout).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP){
                    delayedFragmentChange(new CoinStoreFragment(), "coinstore");
                }
                return false;
            }
        });

        //GetFreeCoins
        view.findViewById(R.id.free_coin_layout).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //((MainActivity)getActivity()).showadColony();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    delayedShowAdColony();
                }
                return false;
            }
        });

        //Help & About
        view.findViewById(R.id.help_and_about_layout).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP){
                    delayedFragmentChange(new HelpAboutFragment(), "helpandabout");
                }
                return false;
            }
        });

        //Rate
        view.findViewById(R.id.rate_layout).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP){
                    /*startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("market://details?id=" + "com.forest_interactive.topmodelindo")));*/
                    delayedRateUs();
                }
                return false;
            }
        });

        //ContactUs
        view.findViewById(R.id.contact_us).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP){
                    delayedFragmentChange(new ContactUsFragment(), "contactus");
                }
                return false;
            }
        });


    }

    public void setViews(){
        mView.findViewById(R.id.main_profile_location).setVisibility(View.GONE);
        mView.findViewById(R.id.notification_layout).setVisibility(View.GONE);
        TextView profileName_tv = (TextView) mView.findViewById(R.id.main_profile_name);
        TextView wallet_tv = (TextView) mView.findViewById(R.id.myprofile_wallet_tv);

        profileName_tv.setText(global.getUser_data().getFullname());
        wallet_tv.setText(global.getUser_data().getWallet());
    }

    private void delayedFragmentChange(final Fragment fragment, final String tag){
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        changeFragment(fragment, tag);
                    }
                }, 475);
            }
        }).start();
    }

    private void delayedShowAdColony(){
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ((MainActivity)getActivity()).showadColony();
                    }
                }, 475);
            }
        }).start();
    }

    private void delayedRateUs(){
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(Intent.ACTION_VIEW,
                                Uri.parse("market://details?id=" + "com.forest_interactive.topmodelindo")));
                    }
                }, 475);
            }
        }).start();
    }

}
