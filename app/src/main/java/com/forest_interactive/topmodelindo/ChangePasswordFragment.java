package com.forest_interactive.topmodelindo;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.forest_interactive.topmodelindo.Models.Response;
import com.forest_interactive.topmodelindo.Widgets.CustomBaseFragment;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ChangePasswordFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ChangePasswordFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChangePasswordFragment extends CustomBaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private boolean running;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ArrayList<Response> responseObj;

    AsyncHttpClient client;

    View mView;

    TopModelGlobal global;

    private OnFragmentInteractionListener mListener;

    public ChangePasswordFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ChangePasswordFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChangePasswordFragment newInstance(String param1, String param2) {
        ChangePasswordFragment fragment = new ChangePasswordFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_change_password, container, false);

        mView = view;

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

        global = (TopModelGlobal) getActivity().getApplicationContext();
        running = true;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        client = new AsyncHttpClient();
        client.setTimeout(40000);
        setToolBarTitle(getStringRes(R.string.title_change_password));
        setUpViews(mView);


    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        client.cancelAllRequests(true);
        running = false;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void setUpViews(final View view){
        final EditText oldPassword_et = (EditText) view.findViewById(R.id.old_password_et);
        final EditText newPassword_et = (EditText) view.findViewById(R.id.new_password_et);
        final EditText confirmPassword_et = (EditText) view.findViewById(R.id.password_confirm_et);
        Button change_btn = (Button) view.findViewById(R.id.change_btn);


        change_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oldPassword = oldPassword_et.getText().toString();
                String newPassword = newPassword_et.getText().toString();
                String confirmPassword = confirmPassword_et.getText().toString();

                if (oldPassword.isEmpty() || newPassword.isEmpty() || confirmPassword.isEmpty()){
                    //showSnack("Please enter all the required details");
                    showSnack(getStringRes(R.string.error_empty_email_password));
                } else if (newPassword.equalsIgnoreCase(confirmPassword)){
                    doChangePassword(oldPassword, newPassword, view);
                    //Toast.makeText(getActivity().getApplicationContext(), "Good to go", Toast.LENGTH_SHORT).show();
                    crossFadeViews(view.findViewById(R.id.change_password_content_layout),
                            view.findViewById(R.id.change_password_progress));
                } else {
                    /*showSnack("Passwords do not match\n" +
                            "Please try again");*/
                    showSnack(getStringRes(R.string.error_password_mismatch));
                }
            }
        });
    }

    private void doChangePassword(String oldP, String newP, final View view){
        String url = null;
        try {
            url = "http://210.5.41.103/tuneapp/api/ChangePassword/" +
                    //"{username}/" +
                    URLEncoder.encode(global.getUser_data().getUsername(), "UTF-8") + "/" +
                    //"{oldpassword}/" +
                    URLEncoder.encode(oldP, "UTF-8") + "/" +
                    //"{newpassword}";
                    URLEncoder.encode(newP, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            //showSnack("Encoding Error, please try again");
            showSnack(getStringRes(R.string.error_encoding));
        }

        if (isNetworkAvailable()) {
            client.get(url, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    if (running) {
                        //showSnack("Error Occured\nPlease try again");
                        showSnack(getStringRes(R.string.error_unknown));
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    if (running) {
                        Gson gson = new GsonBuilder().create();
                        JsonParser parser = new JsonParser();
                        JsonArray jArray = parser.parse(responseString).getAsJsonArray();

                        responseObj = new ArrayList<Response>();
                        for (JsonElement obj : jArray) {
                            Response temp = gson.fromJson(obj, Response.class);
                            responseObj.add(temp);
                        }

                        if (responseObj.size() > 0) {
                            if (responseObj.get(0).getStatus().equalsIgnoreCase("success")) {
                                //showSnack("Password changed successfully");
                                showSnack(getStringRes(R.string.notif_change_password_success));
                                crossFadeViews(view.findViewById(R.id.change_password_progress),
                                        view.findViewById(R.id.change_password_content_layout));
                            } else if (!responseObj.get(0).getStatus().equalsIgnoreCase("success")) {
                                //showSnack("Error\nPlease check your password");
                                showSnack(getStringRes(R.string.error_unknown));
                                crossFadeViews(view.findViewById(R.id.change_password_progress),
                                        view.findViewById(R.id.change_password_content_layout));
                            }
                        } else {
                            //showSnack("Unexpected Error\nPlease try again");
                            showSnack(getStringRes(R.string.error_unknown));
                        }
                    }
                }
            });
        } else {
            showSnack(getStringRes(R.string.error_no_internet));
        }

    }
}
