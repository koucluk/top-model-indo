package com.forest_interactive.topmodelindo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.forest_interactive.topmodelindo.Models.Login;
import com.forest_interactive.topmodelindo.Models.Response;
import com.forest_interactive.topmodelindo.Widgets.CustomBaseFragment;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EditProfileFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EditProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditProfileFragment extends CustomBaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    TopModelGlobal global;

    View mView;

    AsyncHttpClient client;

    ArrayList<Response> responseObj;

    private boolean savedNotifStatus;
    private boolean newNotifStatus;
    private boolean savedLangStatus;
    private boolean newLangStatus;

    private boolean running;

    private OnFragmentInteractionListener mListener;

    public EditProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EditProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EditProfileFragment newInstance(String param1, String param2) {
        EditProfileFragment fragment = new EditProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_profile, container, false);

        mView = view;

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        global = (TopModelGlobal) getActivity().getApplicationContext();
        running = true;

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        client.cancelAllRequests(true);
        running = false;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        client = new AsyncHttpClient();
        client.setTimeout(40000);
        setToolBarTitle(getStringRes(R.string.title_edit_profile));
        initViews(mView);

    }

    public void initViews(final View view){
        EditText profileNameTv = (EditText)view.findViewById(R.id.profile_name_et);
        profileNameTv.setText(global.getUser_data().getFullname());
        profileNameTv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //view.findViewById(R.id.save_changes_button).setAlpha(1);
                //Toast.makeText(getActivity().getApplicationContext(), "Text Changed", Toast.LENGTH_SHORT).show();
                //fadeInView(view.findViewById(R.id.save_changes_button));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        final Button saveButton = (Button)view.findViewById(R.id.save_changes_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (saveButton.getAlpha()!=0) {
                    //fadeOutView(saveButton);
                    //Toast.makeText(getActivity(), "hit", Toast.LENGTH_SHORT).show();
                    try {
                        if (newNotifStatus != savedNotifStatus){
                            crossFadeViews(saveButton, view.findViewById(R.id.edit_profile_progress));
                            //updateNotificationStatus(newNotifStatus, view);
                            ((MainActivity)getActivity()).getGCM();
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }


                    if (newLangStatus){
                        global.setLocale("in");
                        //((MainActivity) getActivity()).setLocaleShared("in");
                        ((MainActivity) getActivity()).setLocale("in");
                        ((MainActivity) getActivity()).setLoginAdapter();
                        ((MainActivity) getActivity()).setDrawerMainList();
                        fadeOutView(saveButton);
                        refreshTexts();
                        savedLangStatus = newLangStatus;
                    } else {
                        global.setLocale("en");
                        //((MainActivity) getActivity()).setLocaleShared("en");
                        ((MainActivity) getActivity()).setLocale("en");
                        ((MainActivity) getActivity()).setLoginAdapter();
                        ((MainActivity) getActivity()).setDrawerMainList();
                        fadeOutView(saveButton);
                        refreshTexts();
                        savedLangStatus = newLangStatus;
                    }
                    //fade out may cause some issue with updatenotif
                }

            }
        });

        SwitchCompat notifSwitch = (SwitchCompat) view.findViewById(R.id.edit_profile_notif_switch);
        savedNotifStatus = global.getUser_data().getNotification_statusBool();
        notifSwitch.setChecked(global.getUser_data().getNotification_statusBool());
        notifSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //Toast.makeText(getActivity().getApplicationContext(), "Status: " + isChecked, Toast.LENGTH_SHORT).show();
                if (savedNotifStatus != isChecked) {
                    if (view.findViewById(R.id.save_changes_button).getAlpha() == 0) {
                        fadeInView(view.findViewById(R.id.save_changes_button));
                        newNotifStatus = isChecked;
                    }
                } else {
                    if (view.findViewById(R.id.save_changes_button).getAlpha() != 0) {
                        fadeOutView(view.findViewById(R.id.save_changes_button));
                    }
                }

            }
        });

        SwitchCompat langSwitch = (SwitchCompat) view.findViewById(R.id.edit_profile_lang_switch);
        boolean lang;
        if (global.getLocale().equalsIgnoreCase("in")){
            lang = true;
        } else {
            lang = false;
        }
        langSwitch.setChecked(lang);
        savedLangStatus = lang;
        langSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //SharedPreferences settings = getActivity().getSharedPreferences("sharedpref", 0);
                /*if (isChecked){
                    global.setLocale("in");
                    //settings.edit().putString("locale", "in").commit();
                    ((MainActivity) getActivity()).setLocale("in");
                    ((MainActivity) getActivity()).setLoginAdapter();
                    ((MainActivity) getActivity()).setDrawerMainList();
                } else {
                    global.setLocale("en");
                    //settings.edit().putString("locale", "en").commit();
                    ((MainActivity) getActivity()).setLocale("en");
                    ((MainActivity) getActivity()).setLoginAdapter();
                    ((MainActivity) getActivity()).setDrawerMainList();
                }*/
                if (savedLangStatus != isChecked) {
                    if (view.findViewById(R.id.save_changes_button).getAlpha() == 0) {
                        fadeInView(view.findViewById(R.id.save_changes_button));
                        newLangStatus = isChecked;
                    }
                } else {
                    if (view.findViewById(R.id.save_changes_button).getAlpha() != 0) {
                        fadeOutView(view.findViewById(R.id.save_changes_button));
                    }
                }
            }
        });

        view.findViewById(R.id.edit_profile_change_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragment(new ChangePasswordFragment(), "changepassword");

                /*Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);

                // change the type of data you need to share,
                // for image use "image*//*"
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, "Top Model Indo");
                intent.putExtra(Intent.EXTRA_TEXT,
                        "https://play.google.com/store/apps/details?id=com.forest_interactive.topmodelindo");
                startActivity(Intent.createChooser(intent, "Share"));*/
            }
        });

        view.findViewById(R.id.display_profile_picture_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).doCamera();
            }
        });

        if (global.isFacebookLogin() || global.isGoogleLogin()){
            view.findViewById(R.id.forgot_password_layout).setVisibility(View.GONE);
            view.findViewById(R.id.display_profile_picture_layout).setVisibility(View.GONE);
        }
    }

    public void receiveGcmToken(String token){
        updateNotificationStatus(newNotifStatus, mView, token);
    }

    public void updateNotificationStatus(final boolean status, final View view, String token){
        String url = "";

        try {
            byte[] data = token.getBytes("UTF-8");
            String base64 = Base64.encodeToString(data, Base64.NO_WRAP);

            url = "http://210.5.41.103/tuneapp/api/UpdateNotificationPush/" +
                    //"{username}/" +
                    URLEncoder.encode(global.getUser_data().getUsername(), "UTF-8") + "/" +
                    //"{notificationstatus}";
                    URLEncoder.encode(String.valueOf(status), "UTF-8") + "/" +
                    //URLEncoder.encode("true", "UTF-8") + "/" +
                    //URLEncoder.encode(token, "UTF-8");
                    URLEncoder.encode(base64, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        //Toast.makeText(getActivity().getApplicationContext(), ""+url, Toast.LENGTH_SHORT).show();

        if (isNetworkAvailable()) {
            Log.d("GCM", "URL: "+url);
            client.get(url, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    if (running) {
                        //showSnack("Error Saving Changes\nPlease try again");
                        showSnack(getStringRes(R.string.error_unknown));
                        fadeOutView(view.findViewById(R.id.edit_profile_progress));
                        Log.d("GCM", "Error: "+statusCode+"\n"+responseString);
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    if (running) {
                        Gson gson = new GsonBuilder().create();
                        JsonParser parser = new JsonParser();
                        JsonArray jArray = parser.parse(responseString).getAsJsonArray();

                        responseObj = new ArrayList<Response>();
                        for (JsonElement obj : jArray) {
                            Response temp = gson.fromJson(obj, Response.class);
                            responseObj.add(temp);
                        }
                        if (responseObj.size() > 0) {
                            if (responseObj.get(0).getStatus().equalsIgnoreCase("success")) {
                                updateShared(status, view);
                                //fadeOutView(view.findViewById(R.id.save_changes_button));
                                //fadeOutView(view.findViewById(R.id.edit_profile_progress));
                            }
                        } else {
                            //showSnack("Unexpected Error\nPlease try again");
                            showSnack(getStringRes(R.string.error_unknown));
                            fadeOutView(view.findViewById(R.id.edit_profile_progress));
                        }
                    }
                }
            });
        } else {
            showSnack(getStringRes(R.string.error_no_internet));
            fadeOutView(view.findViewById(R.id.edit_profile_progress));
        }
    }

    private void updateShared(boolean status, final View view){
        SharedPreferences settings = getActivity().getSharedPreferences("sharedpref", 0);
        String json = settings.getString("user_data", "");
        Gson gson = new GsonBuilder().create();
        JsonParser parser = new JsonParser();
        JsonArray jArray = parser.parse(json).getAsJsonArray();

        ArrayList<Login> loginObj = new ArrayList<Login>();
        for (JsonElement obj : jArray) {
            Login temp = gson.fromJson(obj, Login.class);
            loginObj.add(temp);
        }

        if (loginObj.size()>0){
            loginObj.get(0).setNotification_status(String.valueOf(status));
            //String newjson = gson.toJson(loginObj);
            String newjson = new Gson().toJson(loginObj);
            settings.edit().putString("user_data", newjson).commit();
            savedNotifStatus = status;
            global.setUser_data(loginObj.get(0));
            //showSnack("Changes Updated Successfully");
            showSnack(getStringRes(R.string.notif_change_success));
            fadeOutView(view.findViewById(R.id.edit_profile_progress));
        } else {
            //showSnack("Unexpected Error\nPlease try again");
            showSnack(getStringRes(R.string.error_unknown));
            fadeOutView(view.findViewById(R.id.edit_profile_progress));
        }
    }

    private void refreshTexts(){
        setToolBarTitle(getStringRes(R.string.title_edit_profile));
        TextView dpp_tv = (TextView) mView.findViewById(R.id.edit_profile_dpp_text);
        TextView dpn_tv = (TextView) mView.findViewById(R.id.display_profile_text);
        TextView notif_tv = (TextView) mView.findViewById(R.id.display_notification_text);
        TextView lang_tv = (TextView) mView.findViewById(R.id.toggle_language_text);
        TextView change_pw_tv = (TextView) mView.findViewById(R.id.edit_profile_change_password_tv);
        Button change_pw_btn = (Button) mView.findViewById(R.id.edit_profile_change_password);
        Button save_btn = (Button) mView.findViewById(R.id.save_changes_button);

        dpp_tv.setText(getStringRes(R.string.display_profile_picture));
        dpn_tv.setText(getStringRes(R.string.display_profile_name));
        notif_tv.setText(getStringRes(R.string.notifications));
        lang_tv.setText(getStringRes(R.string.toggle_language));
        change_pw_tv.setText(getStringRes(R.string.change_password));
        change_pw_btn.setText(getStringRes(R.string.change_password_btn));
        save_btn.setText(getStringRes(R.string.save_btn));
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
