package com.forest_interactive.topmodelindo;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.forest_interactive.topmodelindo.Models.Response;
import com.forest_interactive.topmodelindo.Widgets.CustomBaseFragment;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ForgotPassFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ForgotPassFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ForgotPassFragment extends CustomBaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    View mView;
    AsyncHttpClient client;
    ArrayList<Response> responseObj;

    private boolean running;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ForgotPassFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ForgotPassFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ForgotPassFragment newInstance(String param1, String param2) {
        ForgotPassFragment fragment = new ForgotPassFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_forgot_pass, container, false);

        mView = view;

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        running = true;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        client = new AsyncHttpClient();
        client.setTimeout(40000);
        initOnClickListeners(mView);
        setToolBarTitle(getStringRes(R.string.title_forgot_password));

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        client.cancelAllRequests(true);
        running = false;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void initOnClickListeners(View view){
        view.findViewById(R.id.si_multiply_view).getBackground()
                .setColorFilter(Color.parseColor("#99231F20"), PorterDuff.Mode.MULTIPLY);

        view.findViewById(R.id.su_multiply_view).getBackground()
                .setColorFilter(Color.parseColor("#99231F20"), PorterDuff.Mode.MULTIPLY);

        view.findViewById(R.id.new_user_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragment(new RegisterFragment(), "register");
            }
        });

        view.findViewById(R.id.login_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //changeFragment(new LoginFragment(), "login");
                getFragmentManager().popBackStackImmediate();
            }
        });


        final EditText email_et = (EditText) view.findViewById(R.id.email_et);


        view.findViewById(R.id.forgot_submit_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = email_et.getText().toString();

                if (email.isEmpty()){
                    //showSnack("Please Enter Your Registered E-Mail");
                    showSnack(getStringRes(R.string.error_empty_email));
                } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                    showSnack(getStringRes(R.string.error_invalid_email));
                } else {
                    if (isNetworkAvailable()) {
                        progressUiStatus(true);
                        //Log.d("forgot", "Timeout " + client.getConnectTimeout());
                        requestPassReset(email);

                    } else {
                        showSnack(getStringRes(R.string.error_no_internet));
                    }
                }
            }
        });
    }

    private void requestPassReset(String email){
        String url = "";
        try {
            url = "http://210.5.41.103/tuneapp/api/ForgotPassword/" +
                    //"{UserName@Email}/" +
                    URLEncoder.encode(email, "UTF-8") + "/" +
                    //"{AppsName}";
                    URLEncoder.encode("top model indo", "UTF-8");;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            progressUiStatus(false);
            showSnack(getStringRes(R.string.error_encoding));
        }

        if (isNetworkAvailable()) {
            client.get(url, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    if (running) {
                        progressUiStatus(false);
                        showSnack(getStringRes(R.string.error_unknown));
                        Log.d("Forgot", "status: "+statusCode+"\nresponse: "+responseString);
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    if (running) {
                        Gson gson = new GsonBuilder().create();
                        JsonParser parser = new JsonParser();
                        JsonArray jArray = parser.parse(responseString).getAsJsonArray();

                        responseObj = new ArrayList<Response>();
                        for (JsonElement obj : jArray) {
                            Response temp = gson.fromJson(obj, Response.class);
                            responseObj.add(temp);
                        }

                        if (responseObj.get(0).getStatus().equalsIgnoreCase("success")) {
                            progressUiStatus(false);
                            showSnack(getStringRes(R.string.notif_reset_success));
                        } else {
                            progressUiStatus(false);
                            showSnack(getStringRes(R.string.error_unknown));
                        }
                    }
                }
            });
        } else {
            showSnack(getStringRes(R.string.error_no_internet));
        }
    }

    private void progressUiStatus(boolean status){
        if (status){
            crossFadeViews(mView.findViewById(R.id.forgot_submit_btn), mView.findViewById(R.id.forgot_progress));
        } else {
            crossFadeViews(mView.findViewById(R.id.forgot_progress), mView.findViewById(R.id.forgot_submit_btn));
        }
    }
}
