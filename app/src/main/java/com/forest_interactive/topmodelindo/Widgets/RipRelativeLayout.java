package com.forest_interactive.topmodelindo.Widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

/**
 * Created by abdulla.hainum on 29/1/2016.
 */
public class RipRelativeLayout extends RelativeLayout {

    private TouchEffectAnimator touchEffectAnimator;

    public RipRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init(){
        touchEffectAnimator = new TouchEffectAnimator(this);
        touchEffectAnimator.setHasRippleEffect(false);
        touchEffectAnimator.setEffectColor(Color.WHITE);
        touchEffectAnimator.setAnimDuration(300);
        touchEffectAnimator.setClipRadius(30);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }

    @Override
    public boolean onTouchEvent(final MotionEvent event) {
        touchEffectAnimator.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        touchEffectAnimator.onDraw(canvas);
        super.onDraw(canvas);

    }
}
