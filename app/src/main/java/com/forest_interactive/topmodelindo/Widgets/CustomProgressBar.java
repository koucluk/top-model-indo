package com.forest_interactive.topmodelindo.Widgets;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import com.forest_interactive.topmodelindo.CustomStyle.style.CircleIndeterminateDrawable;


/**
 * Created by abdulla.hainum on 24/3/2016.
 */
public class CustomProgressBar extends ProgressBar {
    public CustomProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        CircleIndeterminateDrawable custom = new CircleIndeterminateDrawable();
        custom.setColor(Color.WHITE);
        this.setIndeterminateDrawable(custom);
    }
}
