package com.forest_interactive.topmodelindo.Widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

/**
 * Created by User on 14/1/2016.
 */
public class SourceSansProRegularEditText extends EditText {
    TouchEffectAnimator touchEffectAnimator;
    public SourceSansProRegularEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "Fonts/SourceSansPro-Regular.otf"));
        init();
    }

    private void init(){
        touchEffectAnimator = new TouchEffectAnimator(this);
        touchEffectAnimator.setHasRippleEffect(false);
        touchEffectAnimator.setEffectColor(Color.WHITE);
        touchEffectAnimator.setAnimDuration(300);
        touchEffectAnimator.setClipRadius(50);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }

    @Override
    public boolean onTouchEvent(final MotionEvent event) {
        touchEffectAnimator.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        touchEffectAnimator.onDraw(canvas);
        super.onDraw(canvas);

    }
}
