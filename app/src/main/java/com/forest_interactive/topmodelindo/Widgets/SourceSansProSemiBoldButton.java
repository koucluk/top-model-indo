package com.forest_interactive.topmodelindo.Widgets;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

/**
 * Created by User on 19/1/2016.
 */
public class SourceSansProSemiBoldButton extends Button {
    public SourceSansProSemiBoldButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "Fonts/SourceSansPro-Semibold.otf"));
        final Button thisButton = (Button) this;
        this.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN){
                    //thisButton.setTextColor(Color.GRAY);
                    textColorTransition(thisButton, Color.parseColor("#FFFFFFFF"), Color.parseColor("#FF3A3A3A"), true);
                } else if (event.getAction() == MotionEvent.ACTION_UP){
                    //thisButton.setTextColor(Color.WHITE);
                    textColorTransition(thisButton, Color.parseColor("#FF3A3A3A"), Color.parseColor("#FFFFFFFF"), false);
                }
                return false;
            }
        });
    }

    public void textColorTransition(final Button view, int initial, int end, final boolean pressed){
        final float[] from = new float[3];
        final float[] to =   new float[3];

        /*final float[] bFrom = new float[3];
        final float[] bTo =   new float[3];*/

        /*Color.colorToHSV(Color.parseColor("#FFFFFFFF"), from);
        Color.colorToHSV(Color.parseColor("#FFFF0000"), to);*/
        Color.colorToHSV(initial, from);
        Color.colorToHSV(end, to);

        /*Color.colorToHSV(Color.parseColor("#00FFFFFF"), bFrom);
        Color.colorToHSV(Color.parseColor("#803A3A3A"), bTo);*/

        ValueAnimator anim = ValueAnimator.ofFloat(0, 1);
        //anim.setDuration(300);
        anim.setDuration(150);

        final float[] hsv  = new float[3];
        final float[] bHsv = new float[3];
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(){
            @Override public void onAnimationUpdate(ValueAnimator animation) {
                // Transition along each axis of HSV (hue, saturation, value)
                hsv[0] = from[0] + (to[0] - from[0])*animation.getAnimatedFraction();
                hsv[1] = from[1] + (to[1] - from[1])*animation.getAnimatedFraction();
                hsv[2] = from[2] + (to[2] - from[2])*animation.getAnimatedFraction();

                /*bHsv[0] = bFrom[0] + (bTo[0] - bFrom[0])*animation.getAnimatedFraction();
                bHsv[1] = bFrom[1] + (bTo[1] - bFrom[1])*animation.getAnimatedFraction();
                bHsv[2] = bFrom[2] + (bTo[2] - bFrom[2])*animation.getAnimatedFraction();*/

                //view.setBackgroundColor(Color.HSVToColor(hsv));
                view.setTextColor(Color.HSVToColor(hsv));
                if (pressed) {
                    view.setShadowLayer(10 * animation.getAnimatedFraction(), 0, 0, Color.HSVToColor(hsv));
                } else {
                    view.setShadowLayer((1/10)*animation.getAnimatedFraction(), 0, 0, Color.HSVToColor(hsv));
                }
                //view.getBackground().setColorFilter(Color.HSVToColor(bHsv), PorterDuff.Mode.OVERLAY);

            }
        });

        anim.start();
    }
}
