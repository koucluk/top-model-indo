package com.forest_interactive.topmodelindo.Widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

/**
 * Created by User on 20/1/2016.
 */
public class SourceSansProItalicEditText extends EditText {

    TouchEffectAnimator touchEffectAnimator;

    public SourceSansProItalicEditText(final Context context, AttributeSet attrs) {
        super(context, attrs);
        final EditText self = this;
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "Fonts/SourceSansPro-LightIt.otf"));
        this.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0){
                    self.setTypeface(Typeface.createFromAsset(context.getAssets(), "Fonts/SourceSansPro-LightIt.otf"));
                } else {
                    self.setTypeface(Typeface.createFromAsset(context.getAssets(), "Fonts/SourceSansPro-Regular.otf"));
                }
            }
        });
        init();
    }

    private void init(){
        touchEffectAnimator = new TouchEffectAnimator(this);
        touchEffectAnimator.setHasRippleEffect(false);
        touchEffectAnimator.setEffectColor(Color.WHITE);
        touchEffectAnimator.setAnimDuration(300);
        touchEffectAnimator.setClipRadius(30);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }

    @Override
    public boolean onTouchEvent(final MotionEvent event) {
        touchEffectAnimator.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        touchEffectAnimator.onDraw(canvas);
        super.onDraw(canvas);

    }
}
