package com.forest_interactive.topmodelindo.Widgets;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.forest_interactive.topmodelindo.MainActivity;
import com.forest_interactive.topmodelindo.R;
import com.forest_interactive.topmodelindo.TopModelGlobal;

/**
 * Created by abdulla.hainum on 2/2/2016.
 */
public class CustomBaseFragment extends android.support.v4.app.Fragment {

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    DrawerLayout slidingPane;
    ActionBarDrawerToggle actionBarDrawerToggle;

    Toolbar tb;

    public void changeFragment(final android.support.v4.app.Fragment fragment, final String tag, final Bundle bundle) {
        ThreadTask fragmentTask = new ThreadTask();
        fragmentTask.fragment = fragment;
        fragmentTask.tag = tag;
        fragmentTask.bundle = bundle;
        fragmentTask.execute();
    }


    public void changeFragment(final android.support.v4.app.Fragment fragment, final String tag) {
        /*final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        *//*fragmentManager.beginTransaction()
                                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                                .replace(R.id.fragment_container, fragment).commit();*//*
                        fragmentManager = getFragmentManager();
                        fragmentTransaction = fragmentManager.beginTransaction();
                        Fragment checkFrag = fragmentManager.findFragmentByTag(tag);
                        if (checkFrag == null) {
                            fragmentTransaction
                                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                                    .replace(R.id.fragment_container, fragment, tag);
                            fragmentTransaction.addToBackStack(null);
                            fragmentTransaction.commit();
                        } else {
                            fragmentTransaction
                                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                                    .replace(R.id.fragment_container, checkFrag, tag);
                            fragmentTransaction.addToBackStack(null);
                            fragmentTransaction.commit();
                        }
                    }
                }, 225);
            }
        }).start();*/

        //if (fragment == fragmentManager.findFragmentById(R.id.fragment_container)){

        ThreadTask fragmentTask = new ThreadTask();
        fragmentTask.fragment = fragment;
        fragmentTask.tag = tag;
        fragmentTask.execute();
    }

    public void changeFragment(final android.support.v4.app.Fragment fragment) {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        /*fragmentManager.beginTransaction()
                                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                                .replace(R.id.fragment_container, fragment).commit();*/
                        fragmentManager = getFragmentManager();
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction
                                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                                .replace(R.id.fragment_container, fragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                    }
                }, 225);
            }
        }).start();
    }

    public void setToolBarTitle(final String title) {
        final Handler handler = new Handler();

        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        tb = (Toolbar) getActivity().findViewById(R.id.toolbar);
                        if (title.isEmpty()) {
                            tb.findViewById(R.id.toolbar_search_et).setVisibility(View.VISIBLE);
                            tb.findViewById(R.id.toolbar_logo_icon).setVisibility(View.VISIBLE);
                            tb.findViewById(R.id.toolbar_title).setVisibility(View.GONE);
                            setDrawerArrowDrawableState(true);
                        } else if (title.equalsIgnoreCase(getStringRes(R.string.title_forgot_password))
                                || title.equalsIgnoreCase(getStringRes(R.string.title_login))
                                || title.equalsIgnoreCase(getStringRes(R.string.title_register))
                                || title.equalsIgnoreCase(getStringRes(R.string.title_my_profile))
                                || title.equalsIgnoreCase(getStringRes(R.string.title_edit_profile))
                                || title.equalsIgnoreCase(getStringRes(R.string.title_my_wallpapers))
                                || title.equalsIgnoreCase(getStringRes(R.string.title_my_videos))
                                || title.equalsIgnoreCase(getStringRes(R.string.title_coin_store))
                                || title.equalsIgnoreCase(getStringRes(R.string.title_contact_us))
                                || title.equalsIgnoreCase(getStringRes(R.string.title_change_password))
                                || title.equalsIgnoreCase(getStringRes(R.string.help_and_about))) {
                            tb.findViewById(R.id.toolbar_search_et).setVisibility(View.GONE);
                            tb.findViewById(R.id.toolbar_logo_icon).setVisibility(View.GONE);
                            TextView toolbarTitle = (TextView) tb.findViewById(R.id.toolbar_title);
                            toolbarTitle.setVisibility(View.VISIBLE);
                            toolbarTitle.setText(title);
                            if (title.length() > 7) {
                                toolbarTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
                            }
                            setDrawerArrowDrawableState(false);
                        } else if (title.equalsIgnoreCase(getStringRes(R.string.title_search)) /*|| title.equalsIgnoreCase("CARI")*/) {
                            tb.findViewById(R.id.toolbar_logo_icon).setVisibility(View.GONE);
                            tb.findViewById(R.id.toolbar_title).setVisibility(View.GONE);
                            setDrawerArrowDrawableState(false);
                        } else {
                            tb.findViewById(R.id.toolbar_search_et).setVisibility(View.VISIBLE);
                            tb.findViewById(R.id.toolbar_logo_icon).setVisibility(View.GONE);
                            TextView toolbarTitle = (TextView) tb.findViewById(R.id.toolbar_title);
                            toolbarTitle.setVisibility(View.VISIBLE);
                            toolbarTitle.setText(title);
                            /*ImageView nav_menu_toggle = (ImageView) tb.findViewById(R.id.nav_icon);
                            DrawerArrowDrawable drawerArrowDrawable = (DrawerArrowDrawable)nav_menu_toggle.getDrawable();
                            drawerArrowDrawable.setParameter(1);*/

                            if (title.length() > 7) {
                                toolbarTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
                            }

                            setDrawerArrowDrawableState(false);
                        }
                        //handle triangle visibility
                        /*if (title.equalsIgnoreCase(getStringRes(R.string.title_video))
                                || title.equalsIgnoreCase(getStringRes(R.string.title_wallpaper))){
                            tb.findViewById(R.id.toolbar_triangle).setVisibility(View.VISIBLE);
                        } else {
                            tb.findViewById(R.id.toolbar_triangle).setVisibility(View.GONE);
                        }*/
                    }
                }, 50);
            }
        }).start();

    }

    /*public void setToolBarTitle(final String title){
        final Handler handler = new Handler();

        //this might need a try catch for null pointer on getActivity
        // add to on attach? onActivityCreated
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        tb = (Toolbar)getActivity().findViewById(R.id.toolbar);
                        if (title.isEmpty()) {
                            tb.findViewById(R.id.toolbar_search_et).setVisibility(View.VISIBLE);
                            tb.findViewById(R.id.toolbar_logo_icon).setVisibility(View.VISIBLE);
                            tb.findViewById(R.id.toolbar_title).setVisibility(View.GONE);
                            setDrawerArrowDrawableState(true);

                        } else if (title.equalsIgnoreCase("FORGOT PASSWORD") || title.equalsIgnoreCase("LOGIN") ||
                                title.equalsIgnoreCase("REGISTER") || title.equalsIgnoreCase("CHANGE PASSWORD")) {
                            tb.findViewById(R.id.toolbar_search_et).setVisibility(View.GONE);
                            tb.findViewById(R.id.toolbar_logo_icon).setVisibility(View.GONE);
                            TextView toolbarTitle = (TextView) tb.findViewById(R.id.toolbar_title);
                            toolbarTitle.setVisibility(View.VISIBLE);
                            toolbarTitle.setText(title);
                            if (title.length() > 7) {
                                toolbarTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
                            }
                            if (title.length() > 12) {
                                toolbarTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                            }
                            setDrawerArrowDrawableState(false);
                        } else if (title.equalsIgnoreCase("SEARCH") || title.equalsIgnoreCase("CARI")) {
                            tb.findViewById(R.id.toolbar_logo_icon).setVisibility(View.GONE);
                            tb.findViewById(R.id.toolbar_title).setVisibility(View.GONE);
                            setDrawerArrowDrawableState(false);
                        }else if (title.equalsIgnoreCase("CONTACT US")) {
                            tb.findViewById(R.id.toolbar_search_et).setVisibility(View.GONE);
                            tb.findViewById(R.id.toolbar_logo_icon).setVisibility(View.GONE);
                            TextView toolbarTitle = (TextView) tb.findViewById(R.id.toolbar_title);
                            toolbarTitle.setVisibility(View.VISIBLE);
                            toolbarTitle.setText(title);
                            setDrawerArrowDrawableState(false);
                        } else {
                            tb.findViewById(R.id.toolbar_search_et).setVisibility(View.VISIBLE);
                            tb.findViewById(R.id.toolbar_logo_icon).setVisibility(View.GONE);
                            TextView toolbarTitle = (TextView) tb.findViewById(R.id.toolbar_title);
                            toolbarTitle.setVisibility(View.VISIBLE);
                            toolbarTitle.setText(title);
                            *//*ImageView nav_menu_toggle = (ImageView) tb.findViewById(R.id.nav_icon);
                            DrawerArrowDrawable drawerArrowDrawable = (DrawerArrowDrawable)nav_menu_toggle.getDrawable();
                            drawerArrowDrawable.setParameter(1);*//*
                            if (title.equalsIgnoreCase("MY WALLPAPERS")){
                                toolbarTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
                            }
                            setDrawerArrowDrawableState(false);
                        }
                        //Thread.currentThread().interrupt();
                    }
                }, 50);
            }
        }).start();

    }*/

    public void setDrawerArrowDrawableState(final boolean isHome) {
        tb = (Toolbar) getActivity().findViewById(R.id.toolbar);
        slidingPane = (DrawerLayout) getActivity().findViewById(R.id.sliding_Panel);
        ImageView nav_menu_toggle = (ImageView) tb.findViewById(R.id.nav_icon);
        final DrawerArrowDrawable drawerArrowDrawable = (DrawerArrowDrawable) nav_menu_toggle.getDrawable();
        if (isHome) {
            drawerArrowDrawable.setFlip(false);
            drawerArrowDrawable.setParameter(0);
            nav_menu_toggle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (slidingPane.isDrawerOpen(Gravity.LEFT)) {
                        slidingPane.closeDrawer(Gravity.LEFT);
                    } else
                        slidingPane.openDrawer(Gravity.LEFT);
                }
            });
        } else {
            drawerArrowDrawable.setFlip(false);
            drawerArrowDrawable.setParameter(1);
            nav_menu_toggle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getFragmentManager().getBackStackEntryCount() > 0) {
                        if (((MainActivity) getActivity()).getExpanded()) {
                            ((MainActivity) getActivity()).collapseSearch();
                            getFragmentManager().popBackStackImmediate();
                        } else {
                            getFragmentManager().popBackStackImmediate();
                        }
                    }

                }
            });
        }
        /*slidingPane.setPanelSlideListener(new SlidingPaneLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                float offset;
                boolean flipped;
                offset = slideOffset;

                // Sometimes slideOffset ends up so close to but not quite 1 or 0.
                if (slideOffset >= .995) {
                    flipped = true;
                    drawerArrowDrawable.setFlip(flipped);
                } else if (slideOffset <= .005) {
                    flipped = false;
                    drawerArrowDrawable.setFlip(flipped);
                }

                if (isHome) {
                    drawerArrowDrawable.setParameter(offset);
                } else {
                    drawerArrowDrawable.setParameter(1);
                }
            }

            @Override
            public void onPanelOpened(View panel) {


            }

            @Override
            public void onPanelClosed(View panel) {
                *//*if (((MainActivity)getActivity()).haveToChange){
                    ((MainActivity)getActivity()).changeFragment(((MainActivity)getActivity()).fragToChange,
                            (((MainActivity)getActivity()).tagToChange));
                    ((MainActivity)getActivity()).haveToChange = false;
                    ((MainActivity)getActivity()).fragToChange = null;
                    ((MainActivity)getActivity()).tagToChange = "";
                }*//*
            }
        });*/

        actionBarDrawerToggle = new ActionBarDrawerToggle(getActivity(), slidingPane, R.drawable.icon_navdraw, 0, 0) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                float offset;
                boolean flipped;
                offset = slideOffset;

                // Sometimes slideOffset ends up so close to but not quite 1 or 0.
                if (slideOffset >= .995) {
                    flipped = true;
                    drawerArrowDrawable.setFlip(flipped);
                } else if (slideOffset <= .005) {
                    flipped = false;
                    drawerArrowDrawable.setFlip(flipped);
                }

                if (isHome) {
                    drawerArrowDrawable.setParameter(offset);
                } else {
                    drawerArrowDrawable.setParameter(1);
                }
            }
        };

        slidingPane.setDrawerListener(actionBarDrawerToggle);
    }

    public void fadeInView(final View view) {
        AlphaAnimation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setDuration(250);
        fadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setClickable(true);
                view.setAlpha(1);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(fadeIn);

    }

    public void fadeOutView(final View view) {
        AlphaAnimation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setDuration(250);
        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setClickable(false);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setAlpha(0);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(fadeOut);
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        //return super.onCreateAnimation(transit, enter, nextAnim);
        Animation anim = super.onCreateAnimation(transit, enter, nextAnim);

        if (anim == null && nextAnim != 0) {
            anim = AnimationUtils.loadAnimation(getActivity(), nextAnim);

        } else {
            Log.d("anim", "null");
        }

        if (anim != null) {
            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    if (slidingPane.isDrawerOpen(Gravity.LEFT)) {
                        slidingPane.closeDrawer(Gravity.LEFT);
                    }
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        } else {
            if (slidingPane.isDrawerOpen(Gravity.LEFT)) {
                slidingPane.closeDrawer(Gravity.LEFT);
            }
            Log.d("anim", "null");
        }

        return anim;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        slidingPane = (DrawerLayout) getActivity().findViewById(R.id.sliding_Panel);
        super.onActivityCreated(savedInstanceState);

    }

    public void crossFadeViews(final View first, final View second) {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        fadeOutView(first);
                        fadeInView(second);
                    }
                }, 50);
            }
        }).start();

    }

    public void showSnack(String message) {
        ((MainActivity) getActivity()).showSnack(message);
    }

    public void showSnack(String message, String actionMessage, View.OnClickListener listener) {
        ((MainActivity) getActivity()).showSnack(message, actionMessage, listener);

    }

    public int convertToDp(int dps) {
        /*final float scale = getActivity().getResources().getDisplayMetrics().density;
        int pixels = (int) (dps * scale + 0.5f);
        return pixels;*/
        return ((MainActivity) getActivity()).convertToDp(dps);
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public boolean isPurchased(String contentID) {
        TopModelGlobal global = (TopModelGlobal) getActivity().getApplicationContext();
        try {
            if (global.getPurchaseHistory().size() > 0) {
                for (int i = 0; i < global.getPurchaseHistory().size(); i++) {
                    if (global.getPurchaseHistory().get(i).getContent_id().equalsIgnoreCase(contentID)) {
                        return true;
                    }
                }
            } else {
                return false;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    public String getStringRes(int res) {
        return ((MainActivity) getActivity()).getStringRes(res);
    }

    public void setWaterMark(SimpleDraweeView drawee, boolean isLandscape) {
        if (isLandscape) {
            //drawee.getHierarchy().setControllerOverlay(getActivity().getResources().getDrawable(R.drawable.watermark_landscape));
            //drawee.getHierarchy().setControllerOverlay(getActivity().getResources().getDrawable(R.drawable.watermark_landscape_1));
            drawee.getHierarchy().setControllerOverlay(getActivity().getResources().getDrawable(R.drawable.watermark_landscape));
        } else {
            //drawee.getHierarchy().setControllerOverlay(getActivity().getResources().getDrawable(R.drawable.watermark_portrait));
            //drawee.getHierarchy().setControllerOverlay(getActivity().getResources().getDrawable(R.drawable.watermark_portrait_1));
            drawee.getHierarchy().setControllerOverlay(getActivity().getResources().getDrawable(R.drawable.watermark_portrait));
        }
    }

    private class ThreadTask extends AsyncTask<Void, Void, Void> {
        String type;
        String tag;
        //
        Bundle bundle;
        Fragment fragment;


        @Override
        protected Void doInBackground(Void... params) {
            fragmentManager = getFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            Fragment checkFrag = fragmentManager.findFragmentByTag(tag);
            if (checkFrag == null && bundle == null) {
                fragmentTransaction
                        .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                        .replace(R.id.fragment_container, fragment, tag);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            } else if (bundle != null) {
                fragment.setArguments(bundle);
                fragmentTransaction
                        .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                        .replace(R.id.fragment_container, fragment, tag);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            } else {
                fragmentTransaction
                        .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                        .replace(R.id.fragment_container, checkFrag, tag);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
            return null;
        }
    }

}
