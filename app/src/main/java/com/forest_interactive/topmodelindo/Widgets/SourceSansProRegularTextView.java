package com.forest_interactive.topmodelindo.Widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by User on 14/1/2016.
 */
public class SourceSansProRegularTextView extends TextView {
    public SourceSansProRegularTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "Fonts/SourceSansPro-Regular.otf"));
    }
}
