package com.forest_interactive.topmodelindo;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

public class VideoPlayerActivity extends AppCompatActivity {

    private VideoView myVideoView;
    private int position = 0;
    //private ProgressDialog progressDialog;
    private Dialog progressDialog;
    private MediaController mediaControls;

    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);
        //setContentView(R.layout.activity_video_player_test);
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);

        if (mediaControls == null) {
            mediaControls = new MediaController(VideoPlayerActivity.this);
        }

        myVideoView = (VideoView) findViewById(R.id.video_view);

        /*progressDialog = new ProgressDialog(VideoPlayerActivity.this);
        progressDialog.setTitle("Downloading Video");
        progressDialog.setMessage("Loading...");*/

        progressDialog = new Dialog(VideoPlayerActivity.this, R.style.ThemeDialogCustom);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);;
        TextView dialog_text = (TextView) progressDialog.findViewById(R.id.dialog_progress_text);
        dialog_text.setText("Downloading Video\nLoading...");

        //progressDialog.setCancelable(false);
        progressDialog.show();

        progressBar = (ProgressBar) findViewById(R.id.progressbar);

        try {
            myVideoView.setMediaController(mediaControls);
            //myVideoView.setVideoPath("http://www.sample-videos.com/video/mp4/240/big_buck_bunny_240p_1mb.mp4");
            //myVideoView.setVideoPath("http://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_2mb.mp4");
            myVideoView.setVideoPath("http://210.5.41.103/CmsAdminPanel/"+
                    getIntent().getStringExtra("video_url"));
            //myVideoView.setVideoPath("http://www.sample-videos.com/video/3gp/144/big_buck_bunny_144p_5mb.3gp");
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        myVideoView.requestFocus();
        myVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mediaPlayer) {
                progressDialog.dismiss();
                //if we have a position on savedInstanceState, the video playback should start from here
                myVideoView.seekTo(position);
                if (position == 0) {
                    myVideoView.start();
                    progressBar.setVisibility(View.VISIBLE);

                    mediaPlayer.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                        @Override
                        public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
                            progressBar.setVisibility(View.GONE);
                            mp.start();
                        }
                    });
                } else {
                    //if we come from a resumed activity, video playback will be paused
                    myVideoView.pause();
                }

            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        //we use onSaveInstanceState in order to store the video playback position for orientation change
        savedInstanceState.putInt("Position", myVideoView.getCurrentPosition());
        myVideoView.pause();
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        //we use onRestoreInstanceState in order to play the video playback from the stored position
        position = savedInstanceState.getInt("Position");
        myVideoView.seekTo(position);
    }
}
