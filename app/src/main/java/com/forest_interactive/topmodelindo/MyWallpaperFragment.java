package com.forest_interactive.topmodelindo;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.forest_interactive.topmodelindo.Adapters.WallpaperRecyclerViewAdapter;
import com.forest_interactive.topmodelindo.Models.Wallpaper;
import com.forest_interactive.topmodelindo.Widgets.CustomBaseFragment;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyWallpaperFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyWallpaperFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyWallpaperFragment extends CustomBaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    TopModelGlobal global;

    AsyncHttpClient client;

    private boolean running;

    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private View mView;
    ArrayList<Wallpaper> wallpaperObj;

    public MyWallpaperFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyWallpaperFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyWallpaperFragment newInstance(String param1, String param2) {
        MyWallpaperFragment fragment = new MyWallpaperFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_wallpaper, container, false);
        mView = view;

        /*GridLayout containerLayout = (GridLayout)view.findViewById(R.id.my_wallpaper_list_container);
        ArrayList<LinearLayout> templates = new ArrayList<LinearLayout>();
        for (int i = 0; i < 3; i++){
            LinearLayout layout = (LinearLayout)View.inflate(getActivity().getApplicationContext(), R.layout.template_wallpaper_thumbnail,null);
            removePrice(layout);
            templates.add(layout);
        }

        for (int i = 0; i < templates.size(); i++) {
            containerLayout.addView(templates.get(i));
        }*/

        return view;
    }

    private void setUpViews(final View view){
        SharedPreferences settings = getActivity().getApplicationContext().getSharedPreferences("sharedpref", 0);;
        mRecyclerView = (RecyclerView) view.findViewById(R.id.wallpaper_rv);
        mRecyclerView.setHasFixedSize(false);
        mLayoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 2);
        mRecyclerView.setLayoutManager(mLayoutManager);

        //if (settings.getString("all_wallpaper","").isEmpty()){
        if (settings.getString("wallpaper_data", "").equalsIgnoreCase("")) {
            getWallpaperData(view);
        } else {
            String json = settings.getString("wallpaper_data", "");
            Gson gson = new GsonBuilder().create();
            JsonParser parser = new JsonParser();
            JsonArray jArray = parser.parse(json).getAsJsonArray();

            wallpaperObj = new ArrayList<Wallpaper>();
            for (JsonElement obj : jArray) {
                Wallpaper temp = gson.fromJson(obj, Wallpaper.class);
                if (isPurchased(temp.getContent_id())) {
                    wallpaperObj.add(temp);
                }
            }

            if (wallpaperObj.size() > 0) {
                mAdapter = new WallpaperRecyclerViewAdapter(wallpaperObj, global.getPurchaseHistory(),
                        (MainActivity) getActivity());
                mRecyclerView.setAdapter(mAdapter);
                ((WallpaperRecyclerViewAdapter) mAdapter).setOnItemClickListener(new WallpaperRecyclerViewAdapter.ListClickListener() {
                    @Override
                    public void onItemClick(int position, View v) {
                        Bundle arg = new Bundle();
                        arg.putString("content_id", wallpaperObj.get(position).getContent_id());
                        arg.putString("artist_name", wallpaperObj.get(position).getArtist_name());
                        arg.putString("wallpaper_name", wallpaperObj.get(position).getWallpaper_name());
                        arg.putString("wallpaper_url", wallpaperObj.get(position).getWallpaper_url());
                        arg.putString("wallpaper_description", wallpaperObj.get(position).getWallpaper_description());
                        arg.putString("publishdate", wallpaperObj.get(position).getPublishDate());
                        arg.putString("total_download", wallpaperObj.get(position).getTotal_download());
                        arg.putString("total_count", wallpaperObj.get(position).getTotal_count());
                        arg.putString("price", wallpaperObj.get(position).getPrice());

                        changeFragment(new WallpaperDetailFragment(), "wallpaperdetail", arg);
                    }
                });

                crossFadeViews(view.findViewById(R.id.wallpaper_progress), mRecyclerView);

            }else {
                crossFadeViews(view.findViewById(R.id.wallpaper_progress), view.findViewById(R.id.wallpaper_rv_error_tv));
            }
        }
    }

    private void getWallpaperData(final View view){
        if (isNetworkAvailable()) {
            client.get("http://210.5.41.103/tuneapp/api/Wallpaper/top%20model%20indo", new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    if (running) {
                        TextView error_tv = (TextView) view.findViewById(R.id.wallpaper_rv_error_tv);
                        error_tv.setText(getStringRes(R.string.error_unknown));
                        crossFadeViews(view.findViewById(R.id.wallpaper_progress), view.findViewById(R.id.wallpaper_rv_error_tv));
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    if (running) {
                        Gson gson = new GsonBuilder().create();
                        JsonParser parser = new JsonParser();
                        JsonArray jArray = parser.parse(responseString).getAsJsonArray();

                        wallpaperObj = new ArrayList<Wallpaper>();
                        for (JsonElement obj : jArray) {
                            Wallpaper temp = gson.fromJson(obj, Wallpaper.class);
                            if (isPurchased(temp.getContent_id())) {
                                wallpaperObj.add(temp);
                            }
                        }

                        if (wallpaperObj.size() > 0) {
                            mAdapter = new WallpaperRecyclerViewAdapter(wallpaperObj, global.getPurchaseHistory(),
                                    (MainActivity) getActivity());
                            mRecyclerView.setAdapter(mAdapter);
                            ((WallpaperRecyclerViewAdapter) mAdapter).setOnItemClickListener(new WallpaperRecyclerViewAdapter.ListClickListener() {
                                @Override
                                public void onItemClick(int position, View v) {
                                    Bundle arg = new Bundle();
                                    arg.putString("content_id", wallpaperObj.get(position).getContent_id());
                                    arg.putString("artist_name", wallpaperObj.get(position).getArtist_name());
                                    arg.putString("wallpaper_name", wallpaperObj.get(position).getWallpaper_name());
                                    arg.putString("wallpaper_url", wallpaperObj.get(position).getWallpaper_url());
                                    arg.putString("wallpaper_description", wallpaperObj.get(position).getWallpaper_description());
                                    arg.putString("publishdate", wallpaperObj.get(position).getPublishDate());
                                    arg.putString("total_download", wallpaperObj.get(position).getTotal_download());
                                    arg.putString("total_count", wallpaperObj.get(position).getTotal_count());
                                    arg.putString("price", wallpaperObj.get(position).getPrice());

                                    changeFragment(new WallpaperDetailFragment(), "wallpaperdetail", arg);
                                }
                            });

                            crossFadeViews(view.findViewById(R.id.wallpaper_progress), mRecyclerView);

                        } else {
                            crossFadeViews(view.findViewById(R.id.wallpaper_progress), view.findViewById(R.id.wallpaper_rv_error_tv));
                        }
                    }
                }

            });
        } else {
            showSnack(getStringRes(R.string.error_no_internet));
        }

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        client = new AsyncHttpClient();
        client.setTimeout(40000);
        setToolBarTitle(getStringRes(R.string.title_my_wallpapers));
        setUpViews(mView);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        global = (TopModelGlobal) getActivity().getApplicationContext();
        running = true;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        client.cancelAllRequests(true);
        running = false;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void removePrice(View rootView){
        ImageView coinImage = (ImageView) rootView.findViewById(R.id.wallpaper_coin_img);
        TextView priceText = (TextView) rootView.findViewById(R.id.featured_wallpaper_price);
        coinImage.setVisibility(View.GONE);
        priceText.setVisibility(View.GONE);
    }
}
