package com.forest_interactive.topmodelindo;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.forest_interactive.topmodelindo.Utils.Mail;
import com.forest_interactive.topmodelindo.Widgets.CustomBaseFragment;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ContactUsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ContactUsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ContactUsFragment extends CustomBaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private View mView;

    private String name;
    private String fromEmail;
    private String subject;
    private String body;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ContactUsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ContactUsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ContactUsFragment newInstance(String param1, String param2) {
        ContactUsFragment fragment = new ContactUsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_contact_us, container, false);
        mView = inflater.inflate(R.layout.fragment_contact_us, container, false);
        return mView;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setToolBarTitle(getStringRes(R.string.title_contact_us));
        initOnClick();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void initOnClick() {
        mView.findViewById(R.id.contact_send_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*MailService mailer = new MailService("from@mydomain.com","to@domain.com","Subject",
                        "TextBody", "<b>HtmlBody</b>"*//*, (Attachment) null*//*);*/
/*
                MailService mailer = new MailService("yuliana@forest-interactive.com",
                        "abdulla.hainum@forest-interactive.com","Subject",
                        "TextBody", "<b>HtmlBody</b>"*//*, (Attachment) null*//*);

                try {
                    mailer.sendAuthenticated();
                } catch (Exception e) {
                    //Log.e(AskTingTing.APP, "Failed sending email.", e);
                    Toast.makeText(getActivity().getApplicationContext(), "Fail: "+e.toString(), Toast.LENGTH_SHORT).show();
                }
                Toast.makeText(getActivity().getApplicationContext(), "Sent", Toast.LENGTH_SHORT).show();*/


                /*try {
                    //GMailSender sender = new GMailSender("username@gmail.com", "password");
                    GMailSender sender = new GMailSender("kecoh.apps@gmail.com", "kecoh2@14");
                    sender.sendMail("This is Subject",
                            "This is Body",
                            //"user@gmail.com",
                            "kecoh.apps@gmail.com",
                            //"user@yahoo.com");
                            "abdulla.hainum@forest-interactive.com");
                } catch (Exception e) {
                    //Log.e("SendMail", e.getMessage(), e);
                    Toast.makeText(getActivity().getApplicationContext(), "Fail: "+e.toString(), Toast.LENGTH_SHORT).show();
                }
                Toast.makeText(getActivity().getApplicationContext(), "Sent", Toast.LENGTH_SHORT).show();*/
                EditText name_et = (EditText) mView.findViewById(R.id.name_et);
                EditText fromAddress_et = (EditText) mView.findViewById(R.id.email_et);
                EditText subject_et = (EditText) mView.findViewById(R.id.subject_et);
                EditText body_et = (EditText) mView.findViewById(R.id.body_et);

                name = name_et.getText().toString();
                fromEmail = fromAddress_et.getText().toString();
                subject = subject_et.getText().toString();
                body = body_et.getText().toString();

                new SendMail().execute();

            }
        });
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class SendMail extends AsyncTask<String, Integer, Void> {
        String msg = "";

        /*protected void onPreExecute() {
            //called before doInBackground() is started
        }*/

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            crossFadeViews(mView.findViewById(R.id.contact_content_layout), mView.findViewById(R.id.contact_progress));
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //Toast.makeText(getActivity().getApplicationContext(), ""+msg, Toast.LENGTH_LONG).show();

            //empty the edit texts
            if (msg.contains("Success")) {
                //showSnack("Message Sent Successfully");
                showSnack(getStringRes(R.string.notif_contact_success));
            } else if (msg.contains("Fail")) {
                showSnack(getStringRes(R.string.error_contact_fail));
            } else if (msg.contains("Error")) {
                //showSnack("Error Sending Message");
                showSnack(getStringRes(R.string.error_contact_fail));
            } else {
                /*showSnack("Unknown Error\n" +
                        "Please try again later");*/
                showSnack(getStringRes(R.string.error_unknown));
            }

            crossFadeViews(mView.findViewById(R.id.contact_progress), mView.findViewById(R.id.contact_content_layout));
        }

        protected void onProgressUpdate() {
            //called when the background task makes any progress
        }

        @Override
        protected Void doInBackground(String... params) {
            //return null;
            //Mail m = new Mail("abdulla.hainum@forest-interactive.com", "hai7768445");
            Mail m = new Mail("developerforest@gmail.com", "D3veloper9@9@");
            //Mail m = new Mail("support@lawaapp.com", "server9@9@");

            //String[] toArr = {"abdulla.hainum@forest-interactive.com"}; // This is an array, you can add more emails, just separate them with a coma
            //m.setTo(toArr); // load array to setTo function
            //m.setFrom("abdulla.hainum@forest-interactive.com"); // who is sending the email
            //m.setSubject("Top Model");
            //m.setBody("About to rip my hair out");

            String[] toArr = {"info@forestniaga.com", "dwiyasko@forestniaga.com"};
            //String[] toArr = {"novanda@forestniaga.com"};
            m.setTo(toArr);
            m.setFrom(fromEmail);
            m.setSubject(subject);
            //perhaps add fromEmail as a footer?
            String updatedBody = body + "\n\n\n\n\n" +
                    Html.fromHtml("Reply to: " +
                            name + " " +
                            "<a href=\"mailto:" + fromEmail + "?Subject=" + subject + "\" target=\"_top\">" + fromEmail + "</a>");
            //m.setBody(body);
            m.setBody(updatedBody);

            try {
                if (m.send()) {
                    // success
                    //Toast.makeText(getActivity().getApplicationContext(), "Email was sent successfully.", Toast.LENGTH_LONG).show();
                    msg = "Success";
                } else {
                    // failure
                    //Toast.makeText(getActivity().getApplicationContext(), "Email was not sent.", Toast.LENGTH_LONG).show();
                    msg = "Fail";
                }
            } catch (Exception e) {
                //Toast.makeText(getActivity().getApplicationContext(), "There was a problem sending the email."+e.toString(), Toast.LENGTH_LONG).show();
                msg = "Error " + e.toString();
                Log.e("Mail", e.toString());
            }
            return null;
        }

        //does not work
        protected void onPostExecute() {
            //called after doInBackground() has finished
            //Toast.makeText(getActivity().getApplicationContext(), ""+msg, Toast.LENGTH_SHORT).show();

        }
    }
}
