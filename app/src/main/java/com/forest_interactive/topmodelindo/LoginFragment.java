package com.forest_interactive.topmodelindo;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.Profile;
import com.forest_interactive.topmodelindo.Models.Login;
import com.forest_interactive.topmodelindo.Models.PurchaseHistory;
import com.forest_interactive.topmodelindo.Models.Registration;
import com.forest_interactive.topmodelindo.Widgets.CustomBaseFragment;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LoginFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginFragment extends CustomBaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    TopModelGlobal global;

    AsyncHttpClient client;

    View mView;

    ArrayList<Login> loginObj;
    ArrayList<Registration> regObj;

    GoogleSignInAccount acct;

    Profile profile;

    private boolean running;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public LoginFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LoginFragment newInstance(String param1, String param2) {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        mView = view;

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        global = (TopModelGlobal) getActivity().getApplicationContext();
        running = true;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        client = new AsyncHttpClient();
        client.setTimeout(30000);
        setToolBarTitle(getStringRes(R.string.title_login));
        initOnClickListeners(mView);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        client.cancelAllRequests(true);
        running = false;
    }

    public void initOnClickListeners(final View view) {
        /*view.findViewById(R.id.topvideo_large_container).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragment(new VideoFragment());
            }
        });*/
        TextView forgotText = (TextView) view.findViewById(R.id.login_forgot_password);
        //SpannableString content = new SpannableString("Lupa Kata Sandi");
        SpannableString content = new SpannableString(getStringRes(R.string.forgot_password));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        forgotText.setText(content);

        view.findViewById(R.id.fb_multiply_view).getBackground()
                .setColorFilter(Color.parseColor("#99231F20"), PorterDuff.Mode.MULTIPLY);
        view.findViewById(R.id.gp_multiply_view).getBackground()
                .setColorFilter(Color.parseColor("#99231F20"), PorterDuff.Mode.MULTIPLY);
        view.findViewById(R.id.su_multiply_view).getBackground()
                .setColorFilter(Color.parseColor("#99231F20"), PorterDuff.Mode.MULTIPLY);

        view.findViewById(R.id.new_user_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragment(new RegisterFragment());
            }
        });

        view.findViewById(R.id.login_forgot_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragment(new ForgotPassFragment());
            }
        });

        view.findViewById(R.id.sign_in_btn).setOnClickListener(new View.OnClickListener() {
            EditText email_et = (EditText) view.findViewById(R.id.email_et);
            EditText pass_et = (EditText) view.findViewById(R.id.password_et);

            @Override
            public void onClick(View v) {
                if (!email_et.getText().toString().isEmpty() && !pass_et.getText().toString().isEmpty()) {
                    crossFadeViews(view.findViewById(R.id.login_content_layout_top), view.findViewById(R.id.login_progress));
                    fadeOutView(view.findViewById(R.id.login_content_layout_bottom));
                    doLogin(email_et.getText().toString(), pass_et.getText().toString(), view);
                } else {
                    /*Toast.makeText(getActivity().getApplicationContext(), "Please enter e-mail and password"
                            , Toast.LENGTH_SHORT).show();*/
                    showSnack(getStringRes(R.string.error_empty_email_password));
                }
            }
        });

        view.findViewById(R.id.google_sign_in_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                crossFadeViews(view.findViewById(R.id.login_content_layout_top), view.findViewById(R.id.login_progress));
                fadeOutView(view.findViewById(R.id.login_content_layout_bottom));
                ((MainActivity) getActivity()).GoogleSignIn();
                //((MainActivity)getActivity()).GoogleSignOut();
            }
        });

        view.findViewById(R.id.facebook_sign_in_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                crossFadeViews(view.findViewById(R.id.login_content_layout_top), view.findViewById(R.id.login_progress));
                fadeOutView(view.findViewById(R.id.login_content_layout_bottom));
                ((MainActivity) getActivity()).FacebookSignIn();
                //((MainActivity)getActivity()).FacebookSignOut();
            }
        });
    }

    /*public void setToolBarTitle(final String title){
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (title.isEmpty()){
                            tb.findViewById(R.id.toolbar_search_et).setVisibility(View.VISIBLE);
                            tb.findViewById(R.id.toolbar_logo_icon).setVisibility(View.VISIBLE);
                            tb.findViewById(R.id.toolbar_title).setVisibility(View.GONE);
                        } else {
                            tb.findViewById(R.id.toolbar_search_et).setVisibility(View.GONE);
                            tb.findViewById(R.id.toolbar_logo_icon).setVisibility(View.GONE);
                            TextView toolbarTitle = (TextView) tb.findViewById(R.id.toolbar_title);
                            toolbarTitle.setVisibility(View.VISIBLE);
                            toolbarTitle.setText(title);
                            *//*ImageView nav_menu_toggle = (ImageView) tb.findViewById(R.id.nav_icon);
                            DrawerArrowDrawable drawerArrowDrawable = (DrawerArrowDrawable)nav_menu_toggle.getDrawable();
                            drawerArrowDrawable.setParameter(1);*//*
                        }
                    }
                }, 50);
            }
        }).start();

    }*/

    public void onFacebookSignIn(boolean status) {
        if (status) {
            /*showSnack("Login Successful");
            changeFragment(new HomeFrag(), "home");*/
            profile = ((MainActivity) getActivity()).getFacebookProfile();
            //Toast.makeText(getActivity().getApplicationContext(), "E-mail " + global.getFacebook_email(), Toast.LENGTH_SHORT).show();
            //((MainActivity)getActivity()).FacebookSignOut();
            doFacebookLogin(global.getFacebook_email(), "facebook", mView);
        } else {
            showSnack(getStringRes(R.string.error_failed_login));
            crossFadeViews(mView.findViewById(R.id.login_progress), mView.findViewById(R.id.login_content_layout_top));
            fadeInView(mView.findViewById(R.id.login_content_layout_bottom));
        }
    }

    public void onGoogleSignIn(boolean status) {
        if (status) {
            //Toast.makeText(getActivity().getApplicationContext(), "Callback Success", Toast.LENGTH_SHORT).show();
            acct = ((MainActivity) getActivity()).getAcct();

            doGoogleLogin(acct.getEmail(), "gmail", mView);
            //doGoogleRegister(mView);
        } else {
            showSnack(getStringRes(R.string.error_unknown));
            crossFadeViews(mView.findViewById(R.id.login_progress), mView.findViewById(R.id.login_content_layout_top));
            fadeInView(mView.findViewById(R.id.login_content_layout_bottom));
        }
    }

    private void doFacebookLogin(String email, String password, final View view) {
        String url = "";
        try {
            url = "http://210.5.41.103/tuneapp/api/login/" +
                    URLEncoder.encode(email, "UTF-8") + "/" +
                    URLEncoder.encode(password, "UTF-8") + "/" +
                    "top%20model%20indo";
        } catch (UnsupportedEncodingException e) {
            showSnack(getStringRes(R.string.error_encoding));
            e.printStackTrace();
        }

        Log.e("URL GOOGLE", url);

        if (isNetworkAvailable()) {
            client.get(url, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    if (running) {
                        showSnack(getStringRes(R.string.error_unknown));
                        crossFadeViews(view.findViewById(R.id.login_progress), view.findViewById(R.id.login_content_layout_top));
                        fadeInView(view.findViewById(R.id.login_content_layout_bottom));
                    }

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    if (running) {
                        Gson gson = new GsonBuilder().create();
                        JsonParser parser = new JsonParser();
                        JsonArray jArray = parser.parse(responseString).getAsJsonArray();
                        loginObj = new ArrayList<Login>();
                        for (JsonElement obj : jArray) {
                            Login temp = gson.fromJson(obj, Login.class);
                            loginObj.add(temp);
                        }
                        if (loginObj.size() > 0) {
                            Log.d("Test", "Login: " + loginObj.get(0).getAccount_status());
                            if (loginObj.get(0).getAccount_status().trim().equalsIgnoreCase("true")) {
                                SharedPreferences settings = getActivity().getSharedPreferences("sharedpref", 0);
                                global.setLoggedIn(true);
                                settings.edit().putBoolean("loggedIn", true).commit();
                                settings.edit().putString("user_data", responseString).commit();
                                global.setUser_data(loginObj.get(0));
                                global.setIsFacebookLogin(true);
                                settings.edit().putBoolean("facebook_login", true).commit();
                                settings.edit().putString("facebook_pic", global.getFacebook_profile_pic_url()).commit();
                                settings.edit().putString("facebook_email", global.getFacebook_email()).commit();
                                //Purchase History
                                getPurchasedContent(view);
                            } else if (loginObj.get(0).getAccount_status().equalsIgnoreCase("false")) {
                                //doGoogleRegister(mView);
                                doFacebookRegister(mView);
                            } else {
                                showSnack(getStringRes(R.string.error_failed_login));
                                crossFadeViews(view.findViewById(R.id.login_progress), view.findViewById(R.id.login_content_layout_top));
                                fadeInView(view.findViewById(R.id.login_content_layout_bottom));
                            }
                        } else {
                            showSnack(getStringRes(R.string.error_unknown));
                            crossFadeViews(view.findViewById(R.id.login_progress), view.findViewById(R.id.login_content_layout_top));
                            fadeInView(view.findViewById(R.id.login_content_layout_bottom));
                        }
                    }
                }
            });
        } else {
            showSnack(getStringRes(R.string.error_no_internet));
            crossFadeViews(view.findViewById(R.id.login_progress), view.findViewById(R.id.login_content_layout_top));
            fadeInView(view.findViewById(R.id.login_content_layout_bottom));
        }


    }

    private void doFacebookRegister(final View view) {
        profile = ((MainActivity) getActivity()).getFacebookProfile();
        String url = "";
        Log.e("FACEBOOK REGISTER", "TRIGGERED");
        try {
            url = "http://210.5.41.103/tuneapp/api/Registration/" +
                    //"{FullName}/" +
                    profile.getFirstName() + "%20" + profile.getLastName() + "/" +
                    //"{UserName@Email}/" +
                    URLEncoder.encode(global.getFacebook_email(), "UTF-8") + "/" +
                    //"{Password}/" +
                    URLEncoder.encode("facebook", "UTF-8") + "/" +
                    //"{RegisterType}/" +
                    URLEncoder.encode("facebook", "UTF-8") + "/" +
                    //"{RegisterKey}/" +
                    URLEncoder.encode(profile.getId(), "UTF-8") + "/" +
                    //"{AppsName}";
                    "top%20model%20indo";
        } catch (Exception e) {
            e.printStackTrace();
            showSnack(getStringRes(R.string.error_encoding));
        }

        if (isNetworkAvailable()) {
            Log.d("Register", "Facebook\nURL:" + url);
            client.get(url, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    if (running) {
                        showSnack(getStringRes(R.string.error_unknown));
                        crossFadeViews(view.findViewById(R.id.login_progress), view.findViewById(R.id.login_content_layout_top));
                        fadeInView(view.findViewById(R.id.login_content_layout_bottom));
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    if (running) {
                        Gson gson = new GsonBuilder().create();
                        JsonParser parser = new JsonParser();
                        JsonArray jArray = parser.parse(responseString).getAsJsonArray();

                        regObj = new ArrayList<Registration>();
                        for (JsonElement obj : jArray) {
                            Registration temp = gson.fromJson(obj, Registration.class);
                            regObj.add(temp);
                        }
                        if (regObj.size() > 0) {
                            Log.d("Test", "Register: " + regObj.get(0).getStatus());
                            if (regObj.get(0).getStatus().equalsIgnoreCase("success")) {
                                showSnack(getStringRes(R.string.notif_validation_request));
                                crossFadeViews(view.findViewById(R.id.login_progress), view.findViewById(R.id.login_content_layout_top));
                                fadeInView(view.findViewById(R.id.login_content_layout_bottom));
                                ((MainActivity) getActivity()).FacebookSignOut();
                            } else if (regObj.get(0).getStatus().equalsIgnoreCase("user is already exists")) {
                                showSnack(getStringRes(R.string.error_verify_email));
                                crossFadeViews(view.findViewById(R.id.login_progress), view.findViewById(R.id.login_content_layout_top));
                                fadeInView(view.findViewById(R.id.login_content_layout_bottom));
                                ((MainActivity) getActivity()).FacebookSignOut();
                            }
                        } else {
                            showSnack(getStringRes(R.string.error_unknown));
                            crossFadeViews(view.findViewById(R.id.login_progress), view.findViewById(R.id.login_content_layout_top));
                            fadeInView(view.findViewById(R.id.login_content_layout_bottom));
                        }
                    }
                }
            });
        } else {
            showSnack(getStringRes(R.string.error_no_internet));
            crossFadeViews(view.findViewById(R.id.login_progress), view.findViewById(R.id.login_content_layout_top));
            fadeInView(view.findViewById(R.id.login_content_layout_bottom));
        }
    }

    private void doGoogleRegister(final View view) {
        Log.d("Register", "Start");
        String url = "";

        Log.e("GMAIL REGISTER", "TRIGGERED");

        try {
            url = "http://210.5.41.103/tuneapp/api/Registration/" +
                    //"{FullName}/" +
                    acct.getDisplayName().replaceAll(" ", "%20") + "/" +
                    //"{UserName@Email}/" +
                    URLEncoder.encode(acct.getEmail(), "UTF-8") + "/" +
                    //"{Password}/" +
                    URLEncoder.encode("gmail", "UTF-8") + "/" +
                    //"{RegisterType}/" +
                    URLEncoder.encode("gmail", "UTF-8") + "/" +
                    //"{RegisterKey}/" +
                    URLEncoder.encode(acct.getId(), "UTF-8") + "/" +
                    //"{AppsName}";
                    "top%20model%20indo";
        } catch (Exception e) {
            e.printStackTrace();
            showSnack(getStringRes(R.string.error_encoding));
        }

        Log.e("GMAIL REGISTER", url);

        if (isNetworkAvailable()) {
            client.get(url, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    if (running) {
                        Log.d("Register", "Fail\nStatus Code: " + statusCode + " \nResponse: " + responseString);
                        showSnack(getStringRes(R.string.error_unknown));
                        crossFadeViews(view.findViewById(R.id.login_progress), view.findViewById(R.id.login_content_layout_top));
                        fadeInView(view.findViewById(R.id.login_content_layout_bottom));
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    Log.e("GOOGLE IN", responseString);
                    if (running) {
                        Gson gson = new GsonBuilder().create();
                        JsonParser parser = new JsonParser();
                        JsonArray jArray = parser.parse(responseString).getAsJsonArray();

                        regObj = new ArrayList<Registration>();
                        for (JsonElement obj : jArray) {
                            Registration temp = gson.fromJson(obj, Registration.class);
                            regObj.add(temp);
                        }
                        if (regObj.size() > 0) {
                            Log.d("Test", "Register: " + regObj.get(0).getStatus());
                            if (regObj.get(0).getStatus().equalsIgnoreCase("success")) {
                                Log.d("Register", "Success");
                                /*showSnack(getStringRes(R.string.notif_validation_request));
                                crossFadeViews(view.findViewById(R.id.login_progress), view.findViewById(R.id.login_content_layout_top));
                                fadeInView(view.findViewById(R.id.login_content_layout_bottom));
                                ((MainActivity) getActivity()).GoogleSignOut();*/

                                /*SharedPreferences settings = getActivity().getSharedPreferences("sharedpref", 0);
                                global.setLoggedIn(true);
                                settings.edit().putBoolean("loggedIn", true).commit();
                                settings.edit().putString("user_data", responseString).commit();


                                global.setIsGoogleLogin(true);
                                settings.edit().putBoolean("google_login", true).commit();
                                try {
                                    settings.edit().putString("google_pic", acct.getPhotoUrl().toString()).commit();
                                } catch (NullPointerException e) {
                                    e.printStackTrace();
                                }
                                settings.edit().putString("facebook_email", acct.getEmail()).commit();
                                global.setUser_data(loginObj.get(0));
                                //Purchase History
                                getPurchasedContent(view);*/
                                doGoogleLogin(acct.getEmail(), "gmail", mView);

                            } else if (regObj.get(0).getStatus().equalsIgnoreCase("user is already exists")) {
                                showSnack(getStringRes(R.string.error_verify_email));
                                crossFadeViews(view.findViewById(R.id.login_progress), view.findViewById(R.id.login_content_layout_top));
                                fadeInView(view.findViewById(R.id.login_content_layout_bottom));
                                ((MainActivity) getActivity()).GoogleSignOut();
                            }
                        } else {
                            Log.d("Register", "Fail size");
                            showSnack(getStringRes(R.string.error_unknown));
                            crossFadeViews(view.findViewById(R.id.login_progress), view.findViewById(R.id.login_content_layout_top));
                            fadeInView(view.findViewById(R.id.login_content_layout_bottom));
                        }
                    }
                }
            });
        } else {
            showSnack(getStringRes(R.string.error_no_internet));
            crossFadeViews(view.findViewById(R.id.login_progress), view.findViewById(R.id.login_content_layout_top));
            fadeInView(view.findViewById(R.id.login_content_layout_bottom));
        }
    }

    private void doGoogleLogin(String email, String password, final View view) {
        String url = "";
        try {
            url = "http://210.5.41.103/tuneapp/api/login/" +
                    URLEncoder.encode(email, "UTF-8") + "/" +
                    URLEncoder.encode(password, "UTF-8") + "/" +
                    "top%20model%20indo";
        } catch (UnsupportedEncodingException e) {
            showSnack(getStringRes(R.string.error_encoding));
            e.printStackTrace();
        }

        Log.e("GMAIL LOGIN", url);

        if (isNetworkAvailable()) {
            client.get(url, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    if (running) {
                        Log.d("Login API", "G+ :" + statusCode + responseString);
                        showSnack(getStringRes(R.string.error_unknown));
                        crossFadeViews(view.findViewById(R.id.login_progress), view.findViewById(R.id.login_content_layout_top));
                        fadeInView(view.findViewById(R.id.login_content_layout_bottom));
                    }

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    if (running) {
                        Gson gson = new GsonBuilder().create();
                        JsonParser parser = new JsonParser();
                        JsonArray jArray = parser.parse(responseString).getAsJsonArray();
                        loginObj = new ArrayList<Login>();
                        for (JsonElement obj : jArray) {
                            Login temp = gson.fromJson(obj, Login.class);
                            loginObj.add(temp);
                        }
                        if (loginObj.size() > 0) {
                            Log.d("Test", "Login: " + loginObj.get(0).getAccount_status());
                            if (loginObj.get(0).getAccount_status().trim().equalsIgnoreCase("true")) {
                                SharedPreferences settings = getActivity().getSharedPreferences("sharedpref", 0);
                                global.setLoggedIn(true);
                                settings.edit().putBoolean("loggedIn", true).commit();
                                settings.edit().putString("user_data", responseString).commit();


                                global.setIsGoogleLogin(true);
                                settings.edit().putBoolean("google_login", true).commit();
                                try {
                                    settings.edit().putString("google_pic", acct.getPhotoUrl().toString()).commit();
                                } catch (NullPointerException e) {
                                    e.printStackTrace();
                                }
                                settings.edit().putString("facebook_email", acct.getEmail()).commit();
                                global.setUser_data(loginObj.get(0));
                                //Purchase History
                                getPurchasedContent(view);
                            } else if (loginObj.get(0).getAccount_status().equalsIgnoreCase("false")) {
                                Log.d("Test", "Register Triggered");
                                doGoogleRegister(mView);
                            } else {
                                showSnack(getStringRes(R.string.error_failed_login));
                                crossFadeViews(view.findViewById(R.id.login_progress), view.findViewById(R.id.login_content_layout_top));
                                fadeInView(view.findViewById(R.id.login_content_layout_bottom));
                            }
                        } else {
                            Log.d("Login", "Empty Object");
                            showSnack(getStringRes(R.string.error_unknown));
                            crossFadeViews(view.findViewById(R.id.login_progress), view.findViewById(R.id.login_content_layout_top));
                            fadeInView(view.findViewById(R.id.login_content_layout_bottom));
                        }
                    }
                }
            });
        } else {
            showSnack(getStringRes(R.string.error_no_internet));
            crossFadeViews(view.findViewById(R.id.login_progress), view.findViewById(R.id.login_content_layout_top));
            fadeInView(view.findViewById(R.id.login_content_layout_bottom));
        }
    }

    private void doLogin(String email, String password, final View view) {
        String url = null;
        try {
            url = "http://210.5.41.103/tuneapp/api/login/" +
                    URLEncoder.encode(email, "UTF-8") + "/" +
                    URLEncoder.encode(password, "UTF-8") + "/" +
                    "top%20model%20indo";
        } catch (UnsupportedEncodingException e) {
            showSnack(getStringRes(R.string.error_encoding));
            e.printStackTrace();
        }

        Log.d("Login", "URL :" + url);

        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                if (running) {
                    Log.d("Login", "Email Fail :" + statusCode + responseString);
                    showSnack(getStringRes(R.string.error_unknown));
                    crossFadeViews(view.findViewById(R.id.login_progress), view.findViewById(R.id.login_content_layout_top));
                    fadeInView(view.findViewById(R.id.login_content_layout_bottom));
                }

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                if (running) {
                    //Toast.makeText(getActivity().getApplicationContext(), "API OK", Toast.LENGTH_SHORT).show();
                    Gson gson = new GsonBuilder().create();
                    JsonParser parser = new JsonParser();
                    JsonArray jArray = parser.parse(responseString).getAsJsonArray();
                    loginObj = new ArrayList<Login>();
                    for (JsonElement obj : jArray) {
                        Login temp = gson.fromJson(obj, Login.class);
                        loginObj.add(temp);
                    }
                    if (loginObj.size() > 0) {
                        if (loginObj.get(0).getAccount_status().trim().equalsIgnoreCase("true")) {
                            //Toast.makeText(ctx, "Log in ok", Toast.LENGTH_SHORT).show();
                            SharedPreferences settings = getActivity().getSharedPreferences("sharedpref", 0);
                            global.setLoggedIn(true);
                            settings.edit().putBoolean("loggedIn", true).commit();
                            settings.edit().putString("user_data", responseString).commit();
                            global.setUser_data(loginObj.get(0));
                            //Purchase History
                            getPurchasedContent(view);
                        /*((MainActivity) getActivity()).setLoginAdapter();
                        ((MainActivity) getActivity()).setDrawerMainList();
                        showSnack("Login Successful");
                        changeFragment(new HomeFrag(), "home");*/
                        } else {
                            //Toast.makeText(ctx, "Incorrect credentials", Toast.LENGTH_SHORT).show();
                            showSnack(getStringRes(R.string.error_failed_login));
                            crossFadeViews(view.findViewById(R.id.login_progress), view.findViewById(R.id.login_content_layout_top));
                            fadeInView(view.findViewById(R.id.login_content_layout_bottom));
                        }
                    } else {
                        //Toast.makeText(ctx, "Unexpected Error Occured, Please try again", Toast.LENGTH_SHORT).show();
                        showSnack(getStringRes(R.string.error_unknown));
                        crossFadeViews(view.findViewById(R.id.login_progress), view.findViewById(R.id.login_content_layout_top));
                        fadeInView(view.findViewById(R.id.login_content_layout_bottom));
                    }
                }
            }
        });
    }

    private void getPurchasedContent(final View view) {
        String url = "";
        try {
            url = "http://210.5.41.103/tuneapp/api/PurchaseHistory/" +
                    //"{username}/" +
                    URLEncoder.encode(global.getUser_data().getUsername(), "UTF-8") + "/"
                    //"{appsname}";
                    + "top%20model%20indo";
        } catch (UnsupportedEncodingException e) {
            showSnack(getStringRes(R.string.error_encoding));
            e.printStackTrace();
        }

        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                if (running) {
                    showSnack(getStringRes(R.string.error_failed_login));
                    crossFadeViews(view.findViewById(R.id.login_progress), view.findViewById(R.id.login_content_layout_top));
                    fadeInView(view.findViewById(R.id.login_content_layout_bottom));
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                if (running) {
                    Gson gson = new GsonBuilder().create();
                    JsonParser parser = new JsonParser();
                    JsonArray jArray = parser.parse(responseString).getAsJsonArray();
                    ArrayList<PurchaseHistory> purchaseObj = new ArrayList<PurchaseHistory>();
                    for (JsonElement obj : jArray) {
                        PurchaseHistory temp = gson.fromJson(obj, PurchaseHistory.class);
                        if (!checkDupe(purchaseObj, temp)) {
                            purchaseObj.add(temp);
                        }
                    }
                    if (purchaseObj.size() > 0) {
                        SharedPreferences settings = getActivity().getSharedPreferences("sharedpref", 0);
                        settings.edit().putString("user_purchase_history", responseString).commit();
                        global.setPurchaseHistory(purchaseObj);
                        //Navigate
                        ((MainActivity) getActivity()).setLoginAdapter();
                        ((MainActivity) getActivity()).setDrawerMainList();
                        ((MainActivity) getActivity()).setProfilePic();
                        //showSnack("Login Successful");
                        showSnack(getStringRes(R.string.notif_login_success));
                        changeFragment(new HomeFrag(), "home");
                    } else {
                        SharedPreferences settings = getActivity().getSharedPreferences("sharedpref", 0);
                        settings.edit().putString("user_purchase_history", "").commit();
                        //Navigate
                        ((MainActivity) getActivity()).setLoginAdapter();
                        ((MainActivity) getActivity()).setDrawerMainList();
                        ((MainActivity) getActivity()).setProfilePic();
                        showSnack(getStringRes(R.string.notif_login_success));
                        changeFragment(new HomeFrag(), "home");
                    }


                }
            }
        });
    }

    private boolean checkDupe(ArrayList<PurchaseHistory> array, PurchaseHistory obj) {
        if (array.size() > 0) {
            for (int i = 0; i < array.size(); i++) {
                if (array.get(i).getContent_id().equalsIgnoreCase(obj.getContent_id())) {
                    return true;
                }
            }
        } else {
            return false;
        }
        return false;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
