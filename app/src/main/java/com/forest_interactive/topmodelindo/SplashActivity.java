package com.forest_interactive.topmodelindo;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import com.forest_interactive.topmodelindo.SVG.AnimatedSvgView;
import com.forest_interactive.topmodelindo.SVG.LogoPaths;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.w3c.dom.Text;

import cz.msebera.android.httpclient.Header;

public class SplashActivity extends AppCompatActivity implements LogoPaths {
    private NetworkCompletionListener completionListener;
    private int status = 0;
    private int totalStatus = 4;
    private float progress;
    private int uiProgress = 0;
    private int svgCount = 0;

    private AsyncHttpClient client;

    private AnimatedSvgView svgView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        client = new AsyncHttpClient();
        final TextView status_tv = (TextView) findViewById(R.id.status_tv);
        final TextView progress_tv = (TextView) findViewById(R.id.progress_tv);
        final TextView ui_tv = (TextView) findViewById(R.id.ui_prog_tv);

        svgView = (AnimatedSvgView) findViewById(R.id.animated_svg_view);
        svgView.setGlyphStrings(GLYPHS);
        svgView.setFillPaints(
                /*new int[]{255, 255},
                new int[]{51, 117},
                new int[]{138, 203},
                new int[]{151, 196});*/
                /*new int[]{255, 255},
                new int[]{0, 117},
                new int[]{0, 203},
                new int[]{0, 196});*/
                new int[]{255, 255},
                new int[]{241, 117},
                new int[]{242, 203},
                new int[]{242, 196});
        //int traceColor = Color.argb(255, 0, 0, 0);
        int traceColor = Color.argb(255, 255, 255, 255);
        int[] traceColors = new int[2]; // 4 glyphs
        //int residueColor = Color.argb(50, 0, 0, 0);
        int residueColor = Color.argb(50, 255, 255, 255);
        int[] residueColors = new int[2]; // 4 glyphs
        for (int i = 0; i < traceColors.length; i++) {
            traceColors[i] = traceColor;
            residueColors[i] = residueColor;
        }
        svgView.setTraceColors(traceColors);
        svgView.setTraceResidueColors(residueColors);
        svgView.reset();
        startLogoAnimation();
        svgView.setOnStateChangeListener(new AnimatedSvgView.OnStateChangeListener() {
            @Override
            public void onStateChange(int state) {
                if (state == AnimatedSvgView.STATE_FINISHED){
                    ObjectAnimator fadeOut = ObjectAnimator.ofFloat(svgView, "alpha", 0.0f);
                    fadeOut.addListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            startLogoAnimation();
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    });
                    fadeOut.setInterpolator(new AccelerateDecelerateInterpolator());
                    fadeOut.setDuration(500);
                    if (svgCount >= 2) {
                        if (uiProgress == 100) {
                            startTitleAnimation();
                        } else {
                            fadeOut.start();
                        }
                    } else {
                        fadeOut.start();
                    }
                }
            }
        });

        this.completionListener = new NetworkCompletionListener() {
            @Override
            public void onVideosLoaded(String responseString) {
                saveToShared("video_data", responseString);
                updateStatus();
                updateProgress();
                progress_tv.setText(""+progress);
                status_tv.setText(status_tv.getText()+"\n"+"onVideosLoaded");
            }

            @Override
            public void onRecommendedVideosLoaded(String responseString) {
                saveToShared("rec_video_data", responseString);
                updateStatus();
                updateProgress();
                progress_tv.setText(""+progress);
                status_tv.setText(status_tv.getText()+"\n"+"onRecommendedVideosLoaded");
            }

            @Override
            public void onWallpapersLoaded(String responseString) {
                saveToShared("wallpaper_data", responseString);
                updateStatus();
                updateProgress();
                progress_tv.setText(""+progress);
                status_tv.setText(status_tv.getText()+"\n"+"onWallpapersLoaded");
            }

            @Override
            public void onRecommendedWallpapersLoaded(String responseString) {
                saveToShared("rec_wallpaper_data", responseString);
                updateStatus();
                updateProgress();
                progress_tv.setText(""+progress);
                status_tv.setText(status_tv.getText()+"\n"+"onRecommendedWallpapersLoaded");
            }

            @Override
            public void onError() {
                //do stuff
                uiProgress = uiProgress + 25;
            }

            @Override
            public void checkCompletion() {
                if (status < totalStatus){
                    progress = (status/totalStatus)*100;
                } else {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                }
            }

            @Override
            public void onProgress() {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        uiProgress = uiProgress + 25;
                        ui_tv.setText(""+uiProgress);
                    }
                }, 1000);
            }
        };


        getVideos();
        getWallpapers();
        getRecommendedVideos();
        getRecommendedWallpapers();
    }

    private void startLogoAnimation(){
        svgCount = svgCount + 1;
        ObjectAnimator fadeIn = ObjectAnimator.ofFloat(svgView, "alpha", 1.0f);
        fadeIn.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                svgView.reset();
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                svgView.start();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        fadeIn.setInterpolator(new AccelerateDecelerateInterpolator());
        fadeIn.setDuration(250);
        fadeIn.start();
    }

    private void startTitleAnimation(){
        ImageView title_logo = (ImageView) findViewById(R.id.splash_title_logo);
        ObjectAnimator fadeIn = ObjectAnimator.ofFloat(title_logo, "alpha", 1.0f);
        fadeIn.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                //start MainActivity
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        fadeIn.setInterpolator(new AccelerateDecelerateInterpolator());
        fadeIn.setDuration(1000);
        fadeIn.start();
    }

    private void getVideos(){
        if (isNetworkAvailable()) {
            client.get("http://210.5.41.103/tuneapp/api/Video/top%20model%20indo", new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    completionListener.onError();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    completionListener.onVideosLoaded(responseString);
                }
            });
        } else {
            completionListener.onError();
        }
    }

    private void getWallpapers(){
        if (isNetworkAvailable()) {
            client.get("http://210.5.41.103/tuneapp/api/Wallpaper/top%20model%20indo", new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    completionListener.onError();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    completionListener.onWallpapersLoaded(responseString);
                }
            });
        } else {
            completionListener.onError();
        }
    }

    private void getRecommendedVideos(){
        if (isNetworkAvailable()) {
            client.get("http://210.5.41.103/tuneapp/api/FeaturedVideo/top%20model%20indo/3", new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    completionListener.onError();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    completionListener.onRecommendedVideosLoaded(responseString);
                }
            });
        } else {
            completionListener.onError();
        }
    }

    private void getRecommendedWallpapers(){
        if (isNetworkAvailable()) {
            client.get("http://210.5.41.103/tuneapp/api/FeaturedWallpaper/top%20model%20indo/3", new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    completionListener.onError();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    completionListener.onRecommendedWallpapersLoaded(responseString);
                }
            });
        } else {
            completionListener.onError();
        }
    }

    private void saveToShared(String key, String toSave){
        SharedPreferences settings = getSharedPreferences("sharedpref", 0);
        settings.edit().putString(key, toSave).commit();
    }

    private void updateStatus(){
        status = status + 1;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void updateProgress(){
        progress = (float)status/totalStatus;
        progress = progress*100;
        completionListener.onProgress();
    }

    private interface NetworkCompletionListener{
        public void onVideosLoaded(String responseString);
        public void onRecommendedVideosLoaded(String responseString);
        public void onWallpapersLoaded(String responseString);
        public void onRecommendedWallpapersLoaded(String responseString);
        public void onError();
        public void checkCompletion();
        public void onProgress();
    }

    @Override
    public void onStop(){
        super.onStop();
        client.cancelAllRequests(true);
    }
}
