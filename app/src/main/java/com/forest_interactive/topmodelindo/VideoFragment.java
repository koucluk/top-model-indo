package com.forest_interactive.topmodelindo;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.forest_interactive.topmodelindo.Adapters.VideoRecyclerViewAdapter;
import com.forest_interactive.topmodelindo.Models.Video;
import com.forest_interactive.topmodelindo.Widgets.CustomBaseFragment;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

import cz.msebera.android.httpclient.Header;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link VideoFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link VideoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VideoFragment extends CustomBaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    //Toolbar tb;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    AsyncHttpClient client;

    ArrayList<Video> videoObj;
    ArrayList<Video> recObj;

    private View mView;

    private boolean running;

    TopModelGlobal global;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public VideoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment VideoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static VideoFragment newInstance(String param1, String param2) {
        VideoFragment fragment = new VideoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_video_dep, container, false);
        fragmentManager = getActivity().getSupportFragmentManager();

        //final View view = inflater.inflate(R.layout.fragment_video_new_dep, container, false);
        final View view = inflater.inflate(R.layout.fragment_video, container, false);
        //View view = inflater.inflate(R.layout.fragment_video_dep, container, false);

        mView = view;

        return view;

    }

    private void setupViews(final View view){
        mRecyclerView = (RecyclerView) view.findViewById(R.id.video_rv);
        mRecyclerView.setHasFixedSize(false);
        mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        final SharedPreferences settings = getActivity().getSharedPreferences("sharedpref", 0);

        if (settings.getString("video_data", "").equalsIgnoreCase("")) {
            if (isNetworkAvailable()) {
                client.get("http://210.5.41.103/tuneapp/api/Video/top%20model%20indo", new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        if (running) {
                            //Toast.makeText(getActivity().getApplicationContext(), "Fail", Toast.LENGTH_SHORT).show();
                            TextView error_tv = (TextView) view.findViewById(R.id.video_rv_error_tv);
                            error_tv.setText(getStringRes(R.string.error_unknown));
                            crossFadeViews(view.findViewById(R.id.video_progress), view.findViewById(R.id.video_rv_error_tv));
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        if (running) {
                            //Toast.makeText(getActivity().getApplicationContext(), "Success "+responseString, Toast.LENGTH_SHORT).show();
                            Gson gson = new GsonBuilder().create();
                            JsonParser parser = new JsonParser();
                            JsonArray jArray = parser.parse(responseString).getAsJsonArray();

                            videoObj = new ArrayList<Video>();

                            for (JsonElement obj : jArray) {
                                Video cse = gson.fromJson(obj, Video.class);
                                if (!cse.getVideo_name().contains("GCM")) {
                                    videoObj.add(cse);
                                }
                            }


                            settings.edit().putString("video_data", responseString).commit();

                /*for (int i = 0; i < videoObj.size(); i++) {
                    Video testObj = videoObj.get(i);
                    String withoutHtml = Html.fromHtml(testObj.getVideo_description()).toString().trim();
                    //Toast.makeText(getActivity().getApplicationContext(), "description: " + withoutHtml, Toast.LENGTH_SHORT).show();
                    //Toast.makeText(getActivity().getApplicationContext(), "artist name: " + testObj.getArtist_name(), Toast.LENGTH_SHORT).show();
                    //Toast.makeText(getActivity().getApplicationContext(), "url : " + testObj.getVideo_url(), Toast.LENGTH_SHORT).show();
                    //Toast.makeText(getActivity().getApplicationContext(), "id : " + testObj.getContent_id(), Toast.LENGTH_SHORT).show();
                }*/

                            if (videoObj.size() > 0) {
                                //mAdapter = new VideoRecyclerViewAdapter(videoObj);

                                sortByDate();
                                //sortByDownloads();

                                //mAdapter = new VideoRecyclerViewAdapter(videoObj, (MainActivity)getActivity());
                                //mAdapter = new VideoRecyclerViewAdapter(getDataSet());

                                //mRecyclerView.setAdapter(mAdapter);

                                ((VideoRecyclerViewAdapter) mAdapter).setOnItemClickListener(new VideoRecyclerViewAdapter.ListClickListener() {
                                    @Override
                                    public void onItemClick(int position, View v) {
                                        //Toast.makeText(getActivity().getApplicationContext(), "Touched "+position, Toast.LENGTH_SHORT).show();
                                        Bundle arg = new Bundle();
                                        arg.putString("content_id", videoObj.get(position).getContent_id());
                                        arg.putString("artist_name", videoObj.get(position).getArtist_name());
                                        arg.putString("video_name", videoObj.get(position).getVideo_name());
                                        arg.putString("video_url", videoObj.get(position).getVideo_url());
                                        arg.putString("video_description", Html.fromHtml(videoObj.get(position).getVideo_description()).toString().trim());
                                        arg.putString("publishdate", videoObj.get(position).getPublishDate());
                                        arg.putString("total_download", videoObj.get(position).getTotal_download());
                                        arg.putString("total_count", videoObj.get(position).getTotal_count());
                                        arg.putString("thumbnail", videoObj.get(position).getThumbnail_url());
                                        arg.putString("price", videoObj.get(position).getPrice());

                                        changeFragment(new VideoDetailFragment(), "videodetail", arg);
                                    }
                                });
                    /*fadeOutView(view.findViewById(R.id.video_progress));
                    fadeInView(mRecyclerView);*/
                                crossFadeViews(view.findViewById(R.id.video_progress), mRecyclerView);

                                //Disable RecyclerView Scroll - Set Static Height
                                RelativeLayout rvContainer = (RelativeLayout) view.findViewById(R.id.video_rv_container);
                                ViewGroup.LayoutParams params = rvContainer.getLayoutParams();
                                params.height = convertToDp(140 * videoObj.size());
                                //params.height = convertToDp(140 * getDataSet().size());
                                rvContainer.setLayoutParams(params);

                                //
                                //Toast.makeText(getActivity().getApplicationContext(), "Date "+videoObj.get(0).getPublishDateFormatted(), Toast.LENGTH_SHORT).show();
                            } else {
                                crossFadeViews(view.findViewById(R.id.video_progress), view.findViewById(R.id.video_rv_error_tv));
                            }
                        }
                    }
                });

            } else {
                TextView error_tv = (TextView) view.findViewById(R.id.video_rv_error_tv);
                error_tv.setText(getStringRes(R.string.error_no_internet));
                crossFadeViews(view.findViewById(R.id.video_progress), view.findViewById(R.id.video_rv_error_tv));
            }
        } else {
            String json = settings.getString("video_data", "");
            Gson gson = new GsonBuilder().create();
            JsonParser parser = new JsonParser();
            JsonArray jArray = parser.parse(json).getAsJsonArray();

            videoObj = new ArrayList<Video>();

            for (JsonElement obj : jArray) {
                Video cse = gson.fromJson(obj, Video.class);
                videoObj.add(cse);
            }

            if (videoObj.size() > 0) {

                sortByDate();

                ((VideoRecyclerViewAdapter) mAdapter).setOnItemClickListener(new VideoRecyclerViewAdapter.ListClickListener() {
                    @Override
                    public void onItemClick(int position, View v) {
                        //Toast.makeText(getActivity().getApplicationContext(), "Touched "+position, Toast.LENGTH_SHORT).show();
                        Bundle arg = new Bundle();
                        arg.putString("content_id", videoObj.get(position).getContent_id());
                        arg.putString("artist_name", videoObj.get(position).getArtist_name());
                        arg.putString("video_name", videoObj.get(position).getVideo_name());
                        arg.putString("video_url", videoObj.get(position).getVideo_url());
                        arg.putString("video_description", Html.fromHtml(videoObj.get(position).getVideo_description()).toString().trim());
                        arg.putString("publishdate", videoObj.get(position).getPublishDate());
                        arg.putString("total_download", videoObj.get(position).getTotal_download());
                        arg.putString("total_count", videoObj.get(position).getTotal_count());
                        arg.putString("thumbnail", videoObj.get(position).getThumbnail_url());
                        arg.putString("price", videoObj.get(position).getPrice());

                        changeFragment(new VideoDetailFragment(), "videodetail", arg);
                    }
                });

                crossFadeViews(view.findViewById(R.id.video_progress), mRecyclerView);

                //Disable RecyclerView Scroll - Set Static Height
                RelativeLayout rvContainer = (RelativeLayout) view.findViewById(R.id.video_rv_container);
                ViewGroup.LayoutParams params = rvContainer.getLayoutParams();
                params.height = convertToDp(140 * videoObj.size());
                rvContainer.setLayoutParams(params);

                doBackgroundVideoGet(view);
            } else {
                crossFadeViews(view.findViewById(R.id.video_progress), view.findViewById(R.id.video_rv_error_tv));
            }

        }
        //initOnClickListeners(view);

        /*mAdapter = new VideoRecyclerViewAdapter(getDataSet());
        mRecyclerView.setAdapter(mAdapter);*/
        /*RecyclerView.ItemDecoration itemDecoration =
                new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);
        mRecyclerView.addItemDecoration(itemDecoration);*/
    }

    public void initOnClickListeners(View view){
        /*RelativeLayout topVideoLayout = (RelativeLayout)view.findViewById(R.id.topvideo_large_container);
        topVideoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                *//*fragmentTransaction
                        .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                        .replace(R.id.fragment_container, new VideoDetailFragment()).commit();*//*
                changeFragment(new VideoDetailFragment(), "videodetail");
            }
        });*/
        Toolbar tb = (Toolbar)getActivity().findViewById(R.id.toolbar);
        RelativeLayout title_container = (RelativeLayout) tb.findViewById(R.id.toolbar_title_container);
        title_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getActivity().getApplicationContext(), "works", Toast.LENGTH_SHORT).show();
                PopupMenu popup = new PopupMenu(getActivity().getApplicationContext(), v);
                popup.getMenuInflater()
                        .inflate(R.menu.sort_dropdown, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case (R.id.bydate):
                                //Toast.makeText(getActivity().getApplicationContext(), "bydate", Toast.LENGTH_SHORT).show();
                                sortByDate();
                                break;
                            case (R.id.bydownload):
                                //Toast.makeText(getActivity().getApplicationContext(), "bydl", Toast.LENGTH_SHORT).show();
                                sortByDownloads();
                                break;
                            default:
                                //This shouldn't get called
                                Toast.makeText(getActivity().getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                                break;
                        }
                        return true;
                    }
                });

                popup.show();
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        global = (TopModelGlobal) getActivity().getApplicationContext();
        running = true;

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        client.cancelAllRequests(true);

        running = false;
    }

    @Override
    public void onResume(){
        super.onResume();
        Toolbar tb = (Toolbar)getActivity().findViewById(R.id.toolbar);
        tb.findViewById(R.id.toolbar_triangle).setVisibility(View.VISIBLE);
    }

    @Override
    public void onPause(){
        super.onPause();
        Toolbar tb = (Toolbar)getActivity().findViewById(R.id.toolbar);
        RelativeLayout title_container = (RelativeLayout) tb.findViewById(R.id.toolbar_title_container);
        tb.findViewById(R.id.toolbar_triangle).setVisibility(View.GONE);
        title_container.setOnClickListener(null);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private ArrayList<Video> getDataSet() {
        ArrayList results = new ArrayList<Video>();
        for (int index = 0; index < 5; index++) {
            Video obj = new Video();
            obj.setVideo_name("Title "+index);
            obj.setArtist_name("art " + index);
            obj.setContent_id("id " + index);
            obj.setTotal_count("count " + index);
            obj.setTotal_download("dl " + index);
            obj.setVideo_description("des " + index);
            obj.setVideo_url("url " + index);
            obj.setPublishDate("date T"+index);

            results.add(index, obj);
        }
        return results;
    }

    private void setupRecommendedVideo(final View view){
        final SharedPreferences settings = getActivity().getSharedPreferences("sharedpref", 0);

        if (settings.getString("rec_video_data", "").equalsIgnoreCase("")) {
            if (isNetworkAvailable()) {
                //client.get("http://210.5.41.103/tuneapp/api/FeaturedVideo/top%20model%20indo/1", new TextHttpResponseHandler() {
                client.get("http://210.5.41.103/tuneapp/api/FeaturedVideo/top%20model%20indo/3", new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        if (running) {
                            //Toast.makeText(getActivity().getApplicationContext(), "Failed", Toast.LENGTH_SHORT).show();
                            TextView error_tv = (TextView) view.findViewById(R.id.topvideo_error_tv);
                            error_tv.setText(getStringRes(R.string.error_unknown));
                            crossFadeViews(view.findViewById(R.id.topvideo_progress), view.findViewById(R.id.topvideo_error_tv));
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        if (running) {
                            Gson gson = new GsonBuilder().create();
                            JsonParser parser = new JsonParser();
                            JsonArray jArray = parser.parse(responseString).getAsJsonArray();

                            settings.edit().putString("rec_video_data", responseString).commit();

                            recObj = new ArrayList<Video>();
                            for (JsonElement obj : jArray) {
                                Video temp = gson.fromJson(obj, Video.class);
                                recObj.add(temp);
                            }

                            SimpleDraweeView featuredVideoImg = (SimpleDraweeView) view.findViewById(R.id.wallpaper_thumb_image);
                            TextView featuredVideoDuration = (TextView) view.findViewById(R.id.featured_video_duration);
                            TextView featuredVideoTitle = (TextView) view.findViewById(R.id.featured_wallpaper_title);
                            TextView featuredVideoArtist = (TextView) view.findViewById(R.id.featured_wallpaper_author);
                            TextView featuredVideoDate = (TextView) view.findViewById(R.id.featured_wallpaper_publishdate);
                            TextView featuredVideoDownloadCount = (TextView) view.findViewById(R.id.featured_wallpaper_downloadcount);
                            TextView featuredVideoPrice = (TextView) view.findViewById(R.id.featured_wallpaper_price);
                            RelativeLayout topVideoLayout = (RelativeLayout) view.findViewById(R.id.topvideo_large_container);

                            featuredVideoImg.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/" +
                                    recObj.get(0).getThumbnail_url()));
                            featuredVideoDuration.setText("");
                            if (recObj.size() > 0) {
                                featuredVideoTitle.setText(recObj.get(0).getVideo_name());
                                //featuredVideoArtist.setText("by " + recObj.get(0).getArtist_name());
                                //featuredVideoArtist.setText("by Top Model Indo");
                                featuredVideoArtist.setText(getStringRes(R.string.by) + " Top Model Indo");
                                featuredVideoDate.setText(recObj.get(0).getPublishDate());
                                //featuredVideoDownloadCount.setText(recObj.get(0).getTotal_download() + " Downloads");
                                featuredVideoDownloadCount.setText(recObj.get(0).getTotal_download() + " "
                                        + getStringRes(R.string.download_count));
                                featuredVideoPrice.setText(recObj.get(0).getPrice());
                                //topVideoLayout.setOnClickListener(new View.OnClickListener() {
                                featuredVideoImg.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Bundle arg = new Bundle();
                                        arg.putString("content_id", recObj.get(0).getContent_id());
                                        arg.putString("artist_name", recObj.get(0).getArtist_name());
                                        arg.putString("video_name", recObj.get(0).getVideo_name());
                                        arg.putString("video_url", recObj.get(0).getVideo_url());
                                        arg.putString("video_description", Html.fromHtml(recObj.get(0).getVideo_description()).toString().trim());
                                        arg.putString("publishdate", recObj.get(0).getPublishDate());
                                        arg.putString("total_download", recObj.get(0).getTotal_download());
                                        arg.putString("total_count", recObj.get(0).getTotal_count());
                                        arg.putString("thumbnail", recObj.get(0).getThumbnail_url());
                                        arg.putString("price", recObj.get(0).getPrice());

                                        changeFragment(new VideoDetailFragment(), "videodetail", arg);
                                    }
                                });
                                //crossFadeViews(view.findViewById(R.id.topvideo_progress), topVideoLayout);
                                //crossFadeViews(view.findViewById(R.id.topvideo_progress), view.findViewById(R.id.topvideo_large_image_container));
                                crossFadeViews(view.findViewById(R.id.topvideo_progress), view.findViewById(R.id.top_video_content_container));
                            } else {
                                crossFadeViews(view.findViewById(R.id.topvideo_progress), view.findViewById(R.id.topvideo_error_tv));
                            }
                        }
                    }
                });
            } else {
                TextView error_tv = (TextView) view.findViewById(R.id.topvideo_error_tv);
                error_tv.setText(getStringRes(R.string.error_no_internet));
                crossFadeViews(view.findViewById(R.id.topvideo_progress), view.findViewById(R.id.topvideo_error_tv));
            }
        } else {
            String json = settings.getString("rec_video_data", "");
            Gson gson = new GsonBuilder().create();
            JsonParser parser = new JsonParser();
            JsonArray jArray = parser.parse(json).getAsJsonArray();

            recObj = new ArrayList<Video>();
            for (JsonElement obj : jArray) {
                Video temp = gson.fromJson(obj, Video.class);
                recObj.add(temp);
            }

            SimpleDraweeView featuredVideoImg = (SimpleDraweeView) view.findViewById(R.id.wallpaper_thumb_image);
            TextView featuredVideoDuration = (TextView) view.findViewById(R.id.featured_video_duration);
            TextView featuredVideoTitle = (TextView) view.findViewById(R.id.featured_wallpaper_title);
            TextView featuredVideoArtist = (TextView) view.findViewById(R.id.featured_wallpaper_author);
            TextView featuredVideoDate = (TextView) view.findViewById(R.id.featured_wallpaper_publishdate);
            TextView featuredVideoDownloadCount = (TextView) view.findViewById(R.id.featured_wallpaper_downloadcount);
            TextView featuredVideoPrice = (TextView) view.findViewById(R.id.featured_wallpaper_price);
            RelativeLayout topVideoLayout = (RelativeLayout) view.findViewById(R.id.topvideo_large_container);

            featuredVideoImg.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/" +
                    recObj.get(0).getThumbnail_url()));
            featuredVideoDuration.setText("");
            if (recObj.size() > 0) {
                featuredVideoTitle.setText(recObj.get(0).getVideo_name());
                //featuredVideoArtist.setText("by " + recObj.get(0).getArtist_name());
                //featuredVideoArtist.setText("by Top Model Indo");
                featuredVideoArtist.setText(getStringRes(R.string.by) + " Top Model Indo");
                featuredVideoDate.setText(recObj.get(0).getPublishDate());
                //featuredVideoDownloadCount.setText(recObj.get(0).getTotal_download() + " Downloads");
                featuredVideoDownloadCount.setText(recObj.get(0).getTotal_download() + " "
                        + getStringRes(R.string.download_count));
                featuredVideoPrice.setText(recObj.get(0).getPrice());
                //topVideoLayout.setOnClickListener(new View.OnClickListener() {
                featuredVideoImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Bundle arg = new Bundle();
                        arg.putString("content_id", recObj.get(0).getContent_id());
                        arg.putString("artist_name", recObj.get(0).getArtist_name());
                        arg.putString("video_name", recObj.get(0).getVideo_name());
                        arg.putString("video_url", recObj.get(0).getVideo_url());
                        arg.putString("video_description", Html.fromHtml(recObj.get(0).getVideo_description()).toString().trim());
                        arg.putString("publishdate", recObj.get(0).getPublishDate());
                        arg.putString("total_download", recObj.get(0).getTotal_download());
                        arg.putString("total_count", recObj.get(0).getTotal_count());
                        arg.putString("thumbnail", recObj.get(0).getThumbnail_url());
                        arg.putString("price", recObj.get(0).getPrice());

                        changeFragment(new VideoDetailFragment(), "videodetail", arg);
                    }
                });
                crossFadeViews(view.findViewById(R.id.topvideo_progress), view.findViewById(R.id.top_video_content_container));
                doBackgroundRecVideoGet(view);
            }
        }
    }

    private void doBackgroundVideoGet(final View view){
        client.get("http://210.5.41.103/tuneapp/api/Video/top%20model%20indo", new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                if (running){
                    Gson gson = new GsonBuilder().create();
                    JsonParser parser = new JsonParser();
                    JsonArray jArray = parser.parse(responseString).getAsJsonArray();

                    videoObj = new ArrayList<Video>();

                    for (JsonElement obj : jArray) {
                        Video cse = gson.fromJson(obj, Video.class);
                        videoObj.add(cse);
                    }

                    SharedPreferences settings = getActivity().getSharedPreferences("sharedpref", 0);
                    settings.edit().putString("video_data", responseString).commit();

                    if (videoObj.size() > 0) {

                        sortByDate();

                        ((VideoRecyclerViewAdapter) mAdapter).setOnItemClickListener(new VideoRecyclerViewAdapter.ListClickListener() {
                            @Override
                            public void onItemClick(int position, View v) {
                                //Toast.makeText(getActivity().getApplicationContext(), "Touched "+position, Toast.LENGTH_SHORT).show();
                                Bundle arg = new Bundle();
                                arg.putString("content_id", videoObj.get(position).getContent_id());
                                arg.putString("artist_name", videoObj.get(position).getArtist_name());
                                arg.putString("video_name", videoObj.get(position).getVideo_name());
                                arg.putString("video_url", videoObj.get(position).getVideo_url());
                                arg.putString("video_description", Html.fromHtml(videoObj.get(position).getVideo_description()).toString().trim());
                                arg.putString("publishdate", videoObj.get(position).getPublishDate());
                                arg.putString("total_download", videoObj.get(position).getTotal_download());
                                arg.putString("total_count", videoObj.get(position).getTotal_count());
                                arg.putString("thumbnail", videoObj.get(position).getThumbnail_url());
                                arg.putString("price", videoObj.get(position).getPrice());

                                changeFragment(new VideoDetailFragment(), "videodetail", arg);
                            }
                        });
                        //crossFadeViews(view.findViewById(R.id.video_progress), mRecyclerView);

                        //Disable RecyclerView Scroll - Set Static Height
                        RelativeLayout rvContainer = (RelativeLayout) view.findViewById(R.id.video_rv_container);
                        ViewGroup.LayoutParams params = rvContainer.getLayoutParams();
                        params.height = convertToDp(140 * videoObj.size());
                        //params.height = convertToDp(140 * getDataSet().size());
                        rvContainer.setLayoutParams(params);

                    } else {
                        //crossFadeViews(view.findViewById(R.id.video_progress), view.findViewById(R.id.video_rv_error_tv));
                    }
                }
            }
        });
    }

    private void doBackgroundRecVideoGet(final View view){
        client.get("http://210.5.41.103/tuneapp/api/FeaturedVideo/top%20model%20indo/3", new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                if (running) {
                    Gson gson = new GsonBuilder().create();
                    JsonParser parser = new JsonParser();
                    JsonArray jArray = parser.parse(responseString).getAsJsonArray();

                    SharedPreferences settings = getActivity().getSharedPreferences("sharedpref", 0);
                    settings.edit().putString("rec_video_data", responseString).commit();

                    recObj = new ArrayList<Video>();
                    for (JsonElement obj : jArray) {
                        Video temp = gson.fromJson(obj, Video.class);
                        recObj.add(temp);
                    }

                    SimpleDraweeView featuredVideoImg = (SimpleDraweeView) view.findViewById(R.id.wallpaper_thumb_image);
                    TextView featuredVideoDuration = (TextView) view.findViewById(R.id.featured_video_duration);
                    TextView featuredVideoTitle = (TextView) view.findViewById(R.id.featured_wallpaper_title);
                    TextView featuredVideoArtist = (TextView) view.findViewById(R.id.featured_wallpaper_author);
                    TextView featuredVideoDate = (TextView) view.findViewById(R.id.featured_wallpaper_publishdate);
                    TextView featuredVideoDownloadCount = (TextView) view.findViewById(R.id.featured_wallpaper_downloadcount);
                    TextView featuredVideoPrice = (TextView) view.findViewById(R.id.featured_wallpaper_price);
                    RelativeLayout topVideoLayout = (RelativeLayout) view.findViewById(R.id.topvideo_large_container);

                    featuredVideoImg.setImageURI(Uri.parse("http://210.5.41.103/CmsAdminPanel/" +
                            recObj.get(0).getThumbnail_url()));
                    featuredVideoDuration.setText("");
                    if (recObj.size() > 0) {
                        featuredVideoTitle.setText(recObj.get(0).getVideo_name());
                        //featuredVideoArtist.setText("by " + recObj.get(0).getArtist_name());
                        //featuredVideoArtist.setText("by Top Model Indo");
                        featuredVideoArtist.setText(getStringRes(R.string.by) + " Top Model Indo");
                        featuredVideoDate.setText(recObj.get(0).getPublishDate());
                        //featuredVideoDownloadCount.setText(recObj.get(0).getTotal_download() + " Downloads");
                        featuredVideoDownloadCount.setText(recObj.get(0).getTotal_download() + " "
                                + getStringRes(R.string.download_count));
                        featuredVideoPrice.setText(recObj.get(0).getPrice());
                        //topVideoLayout.setOnClickListener(new View.OnClickListener() {
                        featuredVideoImg.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Bundle arg = new Bundle();
                                arg.putString("content_id", recObj.get(0).getContent_id());
                                arg.putString("artist_name", recObj.get(0).getArtist_name());
                                arg.putString("video_name", recObj.get(0).getVideo_name());
                                arg.putString("video_url", recObj.get(0).getVideo_url());
                                arg.putString("video_description", Html.fromHtml(recObj.get(0).getVideo_description()).toString().trim());
                                arg.putString("publishdate", recObj.get(0).getPublishDate());
                                arg.putString("total_download", recObj.get(0).getTotal_download());
                                arg.putString("total_count", recObj.get(0).getTotal_count());
                                arg.putString("thumbnail", recObj.get(0).getThumbnail_url());
                                arg.putString("price", recObj.get(0).getPrice());

                                changeFragment(new VideoDetailFragment(), "videodetail", arg);
                            }
                        });
                    }
                }
            }
        });
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        client = new AsyncHttpClient();
        client.setTimeout(40000);
        setupViews(mView);
        setupRecommendedVideo(mView);
        setToolBarTitle(getStringRes(R.string.title_video));
        initOnClickListeners(mView);
    }

    private void sortByDate(){
        Collections.sort(videoObj, new Comparator<Video>() {
            @Override
            public int compare(/*Video lhs, Video rhs*/ Video rhs, Video lhs) {
                //return 0;
                return lhs.getPublishDateFormatted().compareTo(rhs.getPublishDateFormatted());
            }
        });
        //
        boolean lang;
        if (global.getLocale().equalsIgnoreCase("in")){
            lang = true;
        } else {
            lang = false;
        }

        //mAdapter = new VideoRecyclerViewAdapter(videoObj, (MainActivity)getActivity());
        mAdapter = new VideoRecyclerViewAdapter(videoObj, (MainActivity)getActivity(), lang);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void sortByDownloads(){
        Collections.sort(videoObj, new Comparator<Video>() {
            @Override
            public int compare(Video lhs, Video rhs) {
                //return 0;
                return lhs.getDownloadCountInt() > rhs.getDownloadCountInt() ? -1 : 1;
            }
        });
        //
        boolean lang;
        if (global.getLocale().equalsIgnoreCase("in")){
            lang = true;
        } else {
            lang = false;
        }

        //mAdapter = new VideoRecyclerViewAdapter(videoObj, (MainActivity)getActivity());
        mAdapter = new VideoRecyclerViewAdapter(videoObj, (MainActivity)getActivity(), lang);
        mRecyclerView.setAdapter(mAdapter);
    }

}
