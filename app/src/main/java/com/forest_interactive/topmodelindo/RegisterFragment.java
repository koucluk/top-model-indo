package com.forest_interactive.topmodelindo;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.forest_interactive.topmodelindo.Models.Registration;
import com.forest_interactive.topmodelindo.Widgets.CustomBaseFragment;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.w3c.dom.Text;

import java.net.URLEncoder;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RegisterFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RegisterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RegisterFragment extends CustomBaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    AsyncHttpClient client;

    private View mView;

    ArrayList<Registration> regObj;

    private boolean running;

    private OnFragmentInteractionListener mListener;

    public RegisterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RegisterFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RegisterFragment newInstance(String param1, String param2) {
        RegisterFragment fragment = new RegisterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_register, container, false);

        return mView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        client = new AsyncHttpClient();
        client.setTimeout(30000);
        setToolBarTitle(getStringRes(R.string.title_register));
        initOnClickListeners(mView);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        running = true;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        client.cancelAllRequests(true);
        running = false;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public void initOnClickListeners(final View view) {
        view.findViewById(R.id.sign_up_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                crossFadeViews(view.findViewById(R.id.registration_content_layout), view.findViewById(R.id.registration_progress));
                fadeOutView(view.findViewById(R.id.registration_login_button));
                registrationRequest(view);
            }
        });

        view.findViewById(R.id.registration_login_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //changeFragment(new LoginFragment(), "login");
                getFragmentManager().popBackStackImmediate();
            }
        });
    }

    public void registrationRequest(final View view) {

        TextView fullname_et = (TextView) view.findViewById(R.id.fullname_et);
        TextView email_et = (TextView) view.findViewById(R.id.email_et);
        TextView password_et = (TextView) view.findViewById(R.id.password_et);
        TextView confirmPassword_et = (TextView) view.findViewById(R.id.password_confirm_et);

        String fullname = fullname_et.getText().toString();
        String email = email_et.getText().toString();
        String password = password_et.getText().toString();
        String confirmPassword = confirmPassword_et.getText().toString();

        if (checkFields(fullname, email, password, confirmPassword)) {
            //showSnack("okla");
            String url = "";
            try {
                url = "http://210.5.41.103/tuneapp/api/Registration/" +
                        //"{FullName}/" +
                        fullname.replaceAll(" ", "%20") + "/" +
                        //"{UserName@Email}/" +
                        URLEncoder.encode(email, "UTF-8") + "/" +
                        //"{Password}/" +
                        URLEncoder.encode(password, "UTF-8") + "/" +
                        //"{RegisterType}/" +
                        URLEncoder.encode("email", "UTF-8") + "/" +
                        //"{RegisterKey}/" +
                        URLEncoder.encode("0", "UTF-8") + "/" +
                        //"{AppsName}";
                        "top%20model%20indo";
            } catch (Exception e) {
                e.printStackTrace();
                showSnack(getStringRes(R.string.error_encoding));
            }
            //Toast.makeText(getActivity().getApplicationContext(), "URL "+ url, Toast.LENGTH_SHORT).show();
            if (isNetworkAvailable()) {
                Log.d("Test", "Register Request");
                client.get(url, new TextHttpResponseHandler() {
                    @Override
                    public void onStart() {
                        super.onStart();
                        Log.d("Test", "Register Request Start");
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        if (running) {
                            Log.d("Test", "Fail\nResponse: " + responseString);
                            showSnack(getStringRes(R.string.error_unknown));
                            crossFadeViews(view.findViewById(R.id.registration_progress), view.findViewById(R.id.registration_content_layout));
                            fadeInView(view.findViewById(R.id.registration_login_button));
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        if (running) {
                            Log.d("Test", "Success\nResponse: " + responseString);
                            Gson gson = new GsonBuilder().create();
                            JsonParser parser = new JsonParser();
                            JsonArray jArray = parser.parse(responseString).getAsJsonArray();

                            regObj = new ArrayList<Registration>();
                            for (JsonElement obj : jArray) {
                                Registration temp = gson.fromJson(obj, Registration.class);
                                regObj.add(temp);
                            }
                            if (regObj.size() > 0) {
                                if (regObj.get(0).getStatus().equalsIgnoreCase("success")) {
                                    showSnack(getStringRes(R.string.notif_register_success));
                                    getFragmentManager().popBackStackImmediate();
                                } else if (regObj.get(0).getStatus().equalsIgnoreCase("user is already exists")) {
                                    showSnack(getStringRes(R.string.notif_validation_request));
                                    getFragmentManager().popBackStackImmediate();
                                }
                            } else {
                                showSnack(getStringRes(R.string.error_unknown));
                                crossFadeViews(view.findViewById(R.id.registration_progress), view.findViewById(R.id.registration_content_layout));
                            }
                        }
                    }
                });
            } else {
                showSnack(getStringRes(R.string.error_no_internet));
            }

        } else {
            //showSnack("taboleh");
            showSnack(getStringRes(R.string.error_empty_email_password));
        }
    }

    public boolean checkFields(String fullname, String email, String password, String confirmPassword) {
        if (fullname.isEmpty() || email.isEmpty() || password.isEmpty() || confirmPassword.isEmpty()) {
            //showSnack("Empty Field");
            showSnack(getStringRes(R.string.error_empty_email_password));
            return false;
        } else if (!password.equalsIgnoreCase(confirmPassword)) {
            //showSnack("Passwords do not match");
            showSnack(getStringRes(R.string.error_password_mismatch));
            return false;
            //} else if (!email.contains("@")){
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            //showSnack("Invalid E-mail Address");
            showSnack(getStringRes(R.string.error_invalid_email));
            return false;
        }else if(password.length() < 4){
            showSnack(getStringRes(R.string.error_size_passwd));
            return false;
        } else {
            return true;
        }
    }

}
