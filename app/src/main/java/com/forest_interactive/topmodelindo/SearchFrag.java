package com.forest_interactive.topmodelindo;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.forest_interactive.topmodelindo.Adapters.VideoSearchRecyclerViewAdapter;
import com.forest_interactive.topmodelindo.Adapters.WallpaperSearchRecyclerViewAdapter;
import com.forest_interactive.topmodelindo.Models.Video;
import com.forest_interactive.topmodelindo.Models.Wallpaper;
import com.forest_interactive.topmodelindo.Widgets.CustomProgressFragment;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by abdulla.hainum on 15/3/2016.
 */
public class SearchFrag extends CustomProgressFragment {
    private EditText searchBar;
    private View mView;
    private TextWatcher searchbarWatcher;
    private AsyncHttpClient client;
    private ArrayList<Video> videoObj;
    private ArrayList<Wallpaper> wallpaperObj;

    private RecyclerView mVideoRecyclerView;
    private VideoSearchRecyclerViewAdapter mVideoAdapter;

    private RecyclerView mWallpaperRecyclerView;
    private WallpaperSearchRecyclerViewAdapter mWallpaperAdapter;

    private boolean running;

    private View mContentView;
    private Handler mHandler;
    private Runnable mShowContentRunnable = new Runnable() {

        @Override
        public void run() {
            //setContentEmpty(true);

            getWallpaperData();
            //setContentShown(true);
        }

    };

    public static SearchFrag newInstance(){
        SearchFrag searchFrag = new SearchFrag();
        return searchFrag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //mContentView = inflater.inflate(R.layout.fragment_search, null);
        mContentView = inflater.inflate(R.layout.fragment_search, null);
        //return inflater.inflate(R.layout.fragment_custom_progress, container, false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        running = true;
        // Setup content view
        setContentView(mContentView);
        // Setup text for empty content
        //setEmptyText(R.string.empty);
        setToolBarTitle(getStringRes(R.string.title_search));
        Toolbar tb = (Toolbar) getActivity().findViewById(R.id.toolbar);
        searchBar = (EditText) tb.findViewById(R.id.toolbar_search_et);
        client = new AsyncHttpClient();
        client.setTimeout(40000);

        obtainData();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mHandler.removeCallbacks(mShowContentRunnable);
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onDetach(){
        super.onDetach();
        searchBar.removeTextChangedListener(searchbarWatcher);
        client.cancelAllRequests(true);
        running = false;
    }

    private void obtainData() {
        // Show indeterminate progress
        setContentShown(false);

        mHandler = new Handler();
        //mHandler.postDelayed(mShowContentRunnable, 3000);
        mHandler.postDelayed(mShowContentRunnable, 1000);
    }


    private void setupViews(){
        mVideoRecyclerView = (RecyclerView) mContentView.findViewById(R.id.search_video_rv);
        mVideoRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mVideoAdapter = new VideoSearchRecyclerViewAdapter(videoObj);
        mVideoRecyclerView.setAdapter(mVideoAdapter);

        ((VideoSearchRecyclerViewAdapter) mVideoAdapter).setOnItemClickListener(new VideoSearchRecyclerViewAdapter.ListClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Bundle arg = new Bundle();
                arg.putString("content_id", videoObj.get(position).getContent_id());
                arg.putString("artist_name", videoObj.get(position).getArtist_name());
                arg.putString("video_name", videoObj.get(position).getVideo_name());
                arg.putString("video_url", videoObj.get(position).getVideo_url());
                arg.putString("video_description", Html.fromHtml(videoObj.get(position).getVideo_description()).toString().trim());
                arg.putString("publishdate", videoObj.get(position).getPublishDate());
                arg.putString("total_download", videoObj.get(position).getTotal_download());
                arg.putString("total_count", videoObj.get(position).getTotal_count());
                arg.putString("thumbnail", videoObj.get(position).getThumbnail_url());
                arg.putString("price", videoObj.get(position).getPrice());

                //searchBar.setText("");
                ((MainActivity)getActivity()).collapseSearch(false);

                changeFragment(new VideoDetailFragment(), "videodetail", arg);
            }
        });

        mWallpaperRecyclerView = (RecyclerView) mContentView.findViewById(R.id.search_wallpaper_rv);
        mWallpaperRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mWallpaperAdapter = new WallpaperSearchRecyclerViewAdapter(wallpaperObj);
        mWallpaperRecyclerView.setAdapter(mWallpaperAdapter);

        ((WallpaperSearchRecyclerViewAdapter) mWallpaperAdapter)
                .setOnItemClickListener(new WallpaperSearchRecyclerViewAdapter.ListClickListener() {
                    @Override
                    public void onItemClick(int position, View v) {
                        Bundle arg = new Bundle();
                        arg.putString("content_id", wallpaperObj.get(position).getContent_id());
                        arg.putString("artist_name", wallpaperObj.get(position).getArtist_name());
                        arg.putString("wallpaper_name", wallpaperObj.get(position).getWallpaper_name());
                        arg.putString("wallpaper_url", wallpaperObj.get(position).getWallpaper_url());
                        arg.putString("wallpaper_description", wallpaperObj.get(position).getWallpaper_description());
                        arg.putString("publishdate", wallpaperObj.get(position).getPublishDate());
                        arg.putString("total_download", wallpaperObj.get(position).getTotal_download());
                        arg.putString("total_count", wallpaperObj.get(position).getTotal_count());
                        arg.putString("price", wallpaperObj.get(position).getPrice());
                        arg.putString("thumbnail_url", wallpaperObj.get(position).getThumbnail());

                        //searchBar.setText("");
                        ((MainActivity)getActivity()).collapseSearch(false);

                        changeFragment(new WallpaperDetailFragment(), "wallpaperdetail", arg);
                    }
                });


        searchBar = ((MainActivity)getActivity()).getSearchBar();
        searchbarWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //add refresh here? test
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                /*TextView test = (TextView) mView.findViewById(R.id.search_msg);
                test.setText("Search Query: " + s.toString());*/

                if (before > 1 && count < 2){
                    repopulateVideoObj();
                    repopulateWallpaperObj();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                //add refresh here? test
                refreshVideo(s);
                refreshWallpaper(s);
                Log.d("Search", "Query: "+s);
            }
        };

        searchBar.addTextChangedListener(searchbarWatcher);

        refreshVideo("");
        refreshWallpaper("");

        setContentShown(true);
    }

    private void refreshWallpaper(CharSequence s){
        if (s.length() <= 3){
        //if (s.length() <= 2){
            mWallpaperRecyclerView.setVisibility(View.GONE);
            TextView msg = (TextView) mContentView.findViewById(R.id.search_wallpaper_msg);
            msg.setText(getStringRes(R.string.notif_enter_search_parameters));
            if (msg.getAlpha() == 0) {
                fadeInView(msg);
            }

            RelativeLayout rvContainer = (RelativeLayout) mContentView.findViewById(R.id.search_wallpaper_rv_container);
            ViewGroup.LayoutParams params = rvContainer.getLayoutParams();
            //params.height = convertToDp(140 * videoObj.size());
            params.height = convertToDp(60 * 1);
            rvContainer.setLayoutParams(params);
        } else {
            List<Wallpaper> filteredWallpaperList = filterWallpaper(wallpaperObj, s.toString());

            if (filteredWallpaperList.size() < 1) {
                TextView msg = (TextView) mContentView.findViewById(R.id.search_wallpaper_msg);
                msg.setText(getStringRes(R.string.error_no_results_found));
                crossFadeViews(mContentView.findViewById(R.id.search_wallpaper_rv), mContentView.findViewById(R.id.search_wallpaper_msg));

                RelativeLayout rvContainer = (RelativeLayout) mContentView.findViewById(R.id.search_wallpaper_rv_container);
                ViewGroup.LayoutParams params = rvContainer.getLayoutParams();
                //params.height = convertToDp(140 * videoObj.size());
                params.height = convertToDp(60 * filteredWallpaperList.size());
                rvContainer.setLayoutParams(params);
            } else {
                mWallpaperRecyclerView.setVisibility(View.VISIBLE);
                mWallpaperAdapter.animateTo(filteredWallpaperList);
                mWallpaperRecyclerView.scrollToPosition(0);
                //Disable Scroll
                RelativeLayout rvContainer = (RelativeLayout) mContentView.findViewById(R.id.search_wallpaper_rv_container);
                ViewGroup.LayoutParams params = rvContainer.getLayoutParams();
                //params.height = convertToDp(140 * videoObj.size());
                params.height = convertToDp(60 * filteredWallpaperList.size());
                rvContainer.setLayoutParams(params);
                crossFadeViews(mContentView.findViewById(R.id.search_wallpaper_msg), mContentView.findViewById(R.id.search_wallpaper_rv));
            }
        }
    }

    private void refreshVideo(CharSequence s){
        if (s.length() <= 3){
        //if (s.length() <= 2){
            mVideoRecyclerView.setVisibility(View.GONE);
            TextView msg = (TextView) mContentView.findViewById(R.id.search_video_msg);
            msg.setText(getStringRes(R.string.notif_enter_search_parameters));
            if (msg.getAlpha() == 0) {
                fadeInView(msg);
            }

            RelativeLayout rvContainer = (RelativeLayout) mContentView.findViewById(R.id.search_video_rv_container);
            ViewGroup.LayoutParams params = rvContainer.getLayoutParams();
            //params.height = convertToDp(140 * videoObj.size());
            params.height = convertToDp(60 * 1);
            rvContainer.setLayoutParams(params);
        } else {
            List<Video> filteredVideoList = filterVideo(videoObj, s.toString());
                    /*mVideoAdapter.animateTo(filteredModelList);
                    mVideoRecyclerView.scrollToPosition(0);*/
            if (filteredVideoList.size()<1){
                //add view.gone?
                TextView msg = (TextView) mContentView.findViewById(R.id.search_video_msg);
                msg.setText(getStringRes(R.string.error_no_results_found));
                crossFadeViews(mContentView.findViewById(R.id.search_video_rv), mContentView.findViewById(R.id.search_video_msg));
                mVideoRecyclerView.setVisibility(View.GONE);

                RelativeLayout rvContainer = (RelativeLayout) mContentView.findViewById(R.id.search_video_rv_container);
                ViewGroup.LayoutParams params = rvContainer.getLayoutParams();
                //params.height = convertToDp(140 * videoObj.size());
                params.height = convertToDp(60 * filteredVideoList.size());
                rvContainer.setLayoutParams(params);
            } else {
                mVideoRecyclerView.setVisibility(View.VISIBLE);
                mVideoAdapter.animateTo(filteredVideoList);
                mVideoRecyclerView.scrollToPosition(0);
                //Disable RecyclerView Scroll - Set Static Height
                RelativeLayout rvContainer = (RelativeLayout) mContentView.findViewById(R.id.search_video_rv_container);
                ViewGroup.LayoutParams params = rvContainer.getLayoutParams();
                //params.height = convertToDp(140 * videoObj.size());
                params.height = convertToDp(60 * filteredVideoList.size());
                rvContainer.setLayoutParams(params);
                crossFadeViews(mContentView.findViewById(R.id.search_video_msg), mContentView.findViewById(R.id.search_video_rv));
            }
        }
    }

    private void getWallpaperData(){
        final SharedPreferences settings = getActivity().getSharedPreferences("sharedpref", 0);
        String wallpaper_json = settings.getString("wallpaper_data","");
        //if (wallpaper_json.isEmpty()) {
        if (wallpaper_json.equalsIgnoreCase("")) {
            //download
            if (isNetworkAvailable()) {
                client.get("http://210.5.41.103/tuneapp/api/Wallpaper/top%20model%20indo", new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        if (running) {
                            //error handle
                            showSnack(getStringRes(R.string.error_unknown));
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        if (running) {
                            Gson gson = new GsonBuilder().create();
                            JsonParser parser = new JsonParser();
                            JsonArray jArray = parser.parse(responseString).getAsJsonArray();
                            settings.edit().putString("wallpaper_data", responseString).commit();

                            wallpaperObj = new ArrayList<Wallpaper>();
                            for (JsonElement obj : jArray) {
                                Wallpaper temp = gson.fromJson(obj, Wallpaper.class);
                                wallpaperObj.add(temp);
                            }
                            getVideoData();
                        }
                    }

                });
            } else {
                showSnack(getStringRes(R.string.error_no_internet));
            }
        } else {
            Gson gson = new GsonBuilder().create();
            JsonParser parser = new JsonParser();
            JsonArray jArray = parser.parse(wallpaper_json).getAsJsonArray();

            wallpaperObj = new ArrayList<>();
            for (JsonElement obj : jArray) {
                Wallpaper temp = gson.fromJson(obj, Wallpaper.class);
                wallpaperObj.add(temp);
            }
            getVideoData();
        }

    }

    private void getVideoData(){
        final SharedPreferences settings = getActivity().getSharedPreferences("sharedpref", 0);
        String video_json = settings.getString("video_data","");
        //if (video_json.isEmpty()){
        if (video_json.equalsIgnoreCase("")){
            //download
            if (isNetworkAvailable()) {
                client.get("http://210.5.41.103/tuneapp/api/Video/top%20model%20indo", new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        if (running) {
                            //error handle
                            showSnack(getStringRes(R.string.error_unknown));
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        if (running) {
                            Gson gson = new GsonBuilder().create();
                            JsonParser parser = new JsonParser();
                            JsonArray jArray = parser.parse(responseString).getAsJsonArray();

                            settings.edit().putString("video_data", responseString).commit();

                            videoObj = new ArrayList<Video>();

                            for (JsonElement obj : jArray) {
                                Video cse = gson.fromJson(obj, Video.class);
                                videoObj.add(cse);
                            }
                            setupViews();
                        }
                    }
                });
            } else {
                showSnack(getStringRes(R.string.error_no_internet));
            }

        } else {
            //parse json
            Gson gson = new GsonBuilder().create();
            JsonParser parser = new JsonParser();
            JsonArray jArray = parser.parse(video_json).getAsJsonArray();

            videoObj = new ArrayList<Video>();

            for (JsonElement obj : jArray) {
                Video cse = gson.fromJson(obj, Video.class);
                videoObj.add(cse);
            }
            setupViews();
        }
    }


    private List<Video> filterVideo(List<Video> models, String query) {
        query = query.toLowerCase();
        Log.d("Search", "VD Lower "+ query);

        final List<Video> filteredModelList = new ArrayList<>();
        for (Video model : models) {
            final String text = model.getVideo_name().toLowerCase();
            Log.d("Search", "Get  Name: "+ text);
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    private List<Wallpaper> filterWallpaper(List<Wallpaper> models, String query) {
        query = query.toLowerCase();
        Log.d("Search", "WP Lower "+ query);

        final List<Wallpaper> filteredModelList = new ArrayList<>();
        for (Wallpaper model : models) {
            final String text = model.getWallpaper_name().toLowerCase();
            Log.d("Search", "Get  Name: "+ text);
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    private void repopulateVideoObj(){
        final SharedPreferences settings = getActivity().getSharedPreferences("sharedpref", 0);
        String video_json = settings.getString("video_data","");

        Gson gson = new GsonBuilder().create();
        JsonParser parser = new JsonParser();
        JsonArray jArray = parser.parse(video_json).getAsJsonArray();

        videoObj = new ArrayList<Video>();

        for (JsonElement obj : jArray) {
            Video cse = gson.fromJson(obj, Video.class);
            videoObj.add(cse);
        }

    }

    private void repopulateWallpaperObj(){
        final SharedPreferences settings = getActivity().getSharedPreferences("sharedpref", 0);
        String wallpaper_json = settings.getString("wallpaper_data","");

        Gson gson = new GsonBuilder().create();
        JsonParser parser = new JsonParser();
        JsonArray jArray = parser.parse(wallpaper_json).getAsJsonArray();

        wallpaperObj = new ArrayList<>();
        for (JsonElement obj : jArray) {
            Wallpaper temp = gson.fromJson(obj, Wallpaper.class);
            wallpaperObj.add(temp);
        }
    }


}
