package com.forest_interactive.topmodelindo;

import android.app.Application;
import android.content.SharedPreferences;
import android.support.multidex.MultiDexApplication;

import com.forest_interactive.topmodelindo.Models.Login;
import com.forest_interactive.topmodelindo.Models.PurchaseHistory;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by abdulla.hainum on 4/3/2016.
 */
public class TopModelGlobal extends MultiDexApplication {

    Tracker tracker;
    HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

    private boolean loggedIn;
    private Login user_data;
    private ArrayList<PurchaseHistory> purchaseHistory;
    private String locale;

    private String facebook_email;
    private String facebook_profile_pic_url;
    private boolean isFacebookLogin;

    private boolean isGoogleLogin;

    synchronized public Tracker getDefaultTracker(TrackerName trackerId) {
        /*if (mTracker == null) {
            GoogleAnalytics gma = GoogleAnalytics.getInstance(this);

            mTracker = gma.newTracker(R.xml.global_tracker);
        }*/

        if (!mTrackers.containsKey(trackerId)) {

            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker("UA-69724784-29")
                    : (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics.newTracker(R.xml.global_tracker)
                    : analytics.newTracker(R.xml.ecommerce_tracker);
            mTrackers.put(trackerId, t);

        }
        return mTrackers.get(trackerId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SharedPreferences settings = getSharedPreferences("sharedpref", 0);
        if (settings.getBoolean("loggedIn", false)) {
            setLoggedIn(true);
            setUser_data(decodeUserJson(settings.getString("user_data", "")));
            if (!settings.getString("user_purchase_history", "").isEmpty()) {
                setPurchaseHistory(decodePurchaseHistory(settings.getString("user_purchase_history", "")));
            }

            if (settings.getBoolean("facebook_login", false)) {
                setIsFacebookLogin(true);
                setFacebook_email(settings.getString("facebook_email", ""));
                setFacebook_profile_pic_url(settings.getString("facebook_pic", ""));
            }

            if (settings.getBoolean("google_login", false)) {
                setIsGoogleLogin(true);
            }

        } else {
            setLoggedIn(false);
        }
    }

    //purchase history object array, update on login & purchase content
    public ArrayList<PurchaseHistory> getPurchaseHistory() {
        return purchaseHistory;
    }

    public void setPurchaseHistory(ArrayList<PurchaseHistory> purchaseHistory) {
        this.purchaseHistory = purchaseHistory;
    }

    public Login getUser_data() {
        return user_data;
    }

    public void setUser_data(Login user_data) {
        this.user_data = user_data;
    }

    public boolean getLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean status) {
        loggedIn = status;
    }

    public Login decodeUserJson(String json) {
        ArrayList<Login> loginObj = new ArrayList<Login>();

        Gson gson = new GsonBuilder().create();
        JsonParser parser = new JsonParser();
        JsonArray jArray = parser.parse(json).getAsJsonArray();

        for (JsonElement obj : jArray) {
            Login temp = gson.fromJson(obj, Login.class);
            loginObj.add(temp);
        }

        if (loginObj.size() > 0) {
            return loginObj.get(0);
        } else {
            return null;
        }

    }

    public ArrayList<PurchaseHistory> decodePurchaseHistory(String json) {
        ArrayList<PurchaseHistory> purchaseHistory = new ArrayList<PurchaseHistory>();
        Gson gson = new GsonBuilder().create();
        JsonParser parser = new JsonParser();
        JsonArray jArray = parser.parse(json).getAsJsonArray();

        for (JsonElement obj : jArray) {
            PurchaseHistory temp = gson.fromJson(obj, PurchaseHistory.class);
            if (!checkDupe(purchaseHistory, temp)) {
                purchaseHistory.add(temp);
            }
        }
        if (purchaseHistory.size() > 0) {
            return purchaseHistory;
        } else {
            return null;
        }
    }

    private boolean checkDupe(ArrayList<PurchaseHistory> array, PurchaseHistory obj) {
        if (array.size() > 0) {
            for (int i = 0; i < array.size(); i++) {
                if (array.get(i).getContent_id().equalsIgnoreCase(obj.getContent_id())) {
                    return true;
                }
            }
        } else {
            return false;
        }
        return false;
    }

    public String getFacebook_email() {
        return facebook_email;
    }

    public void setFacebook_email(String facebook_email) {
        this.facebook_email = facebook_email;
    }

    public String getFacebook_profile_pic_url() {
        return facebook_profile_pic_url;
    }

    public void setFacebook_profile_pic_url(String facebook_profile_pic_url) {
        this.facebook_profile_pic_url = facebook_profile_pic_url;
    }

    public boolean isFacebookLogin() {
        return isFacebookLogin;
    }

    public void setIsFacebookLogin(boolean isFacebookLogin) {
        this.isFacebookLogin = isFacebookLogin;
    }

    public boolean isGoogleLogin() {
        return isGoogleLogin;
    }

    public void setIsGoogleLogin(boolean isGoogleLogin) {
        this.isGoogleLogin = isGoogleLogin;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public enum TrackerName {
        APP_TRACKER, // Tracker used only in this app.
        GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
        ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.
    }
}
