package com.forest_interactive.topmodelindo;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.forest_interactive.aseandcb.AdcbHelper;
import com.forest_interactive.aseandcb.Aseandcb;
import com.forest_interactive.topmodelindo.Models.WalletResponse;
import com.forest_interactive.topmodelindo.Widgets.CustomBaseFragment;
import com.loopj.android.http.AsyncHttpClient;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CoinStoreFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CoinStoreFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CoinStoreFragment extends CustomBaseFragment /*implements AseandcbResult*/ {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    TopModelGlobal global;
    AdcbHelper adcbHelper;
    AsyncHttpClient client;
    ArrayList<WalletResponse> walletResponse;
    Aseandcb aseandcb;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View mView;
    private String[] price;
    private OnFragmentInteractionListener mListener;

    public CoinStoreFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CoinStoreFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CoinStoreFragment newInstance(String param1, String param2) {
        CoinStoreFragment fragment = new CoinStoreFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_coin_store, container, false);

        mView = view;

        return view;
    }

    private void setupViews(final View view) {
        TextView coins_tv = (TextView) view.findViewById(R.id.coin_amount);
        coins_tv.setText(global.getUser_data().getWallet());

        price = new String[6];
        /*price[0] = "1000";
        price[1] = "2000";
        price[2] = "3000";
        price[3] = "5000";
        price[4] = "8000";
        price[5] = "10000";*/
        price[0] = "2000";
        price[1] = "3000";
        price[2] = "5000";
        price[3] = "10000";


        adcbHelper = new AdcbHelper(getActivity().getApplicationContext());

    }

    private void initOnClickListeners(View view) {
        aseandcb = new Aseandcb((MainActivity) getActivity(),
                "NWMOJL359542732", "UKBWDV765209139");
        view.findViewById(R.id.coin_20_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*aseandcb.AseandcbCountryPay("Indonesia", "Top Model Indo: Coins Purchased Successfully",
                        price[0], "20 Coins");*/

                //((MainActivity)getActivity()).googlePurchase("android.test.purchased");
                showPurchaseDialog(0);

                /*aseandcb.AseandcbPay((MainActivity)getActivity(), "Malaysia", "Test Payment Successful", "50", "Test",
                        "FRZZWM738595030", "KIPWVD477066692");*/
            }
        });

        view.findViewById(R.id.coin_50_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*aseandcb.AseandcbCountryPay("Indonesia", "Top Model Indo: Coins Purchased Successfully",
                        price[1], "50 Coins");*/

                showPurchaseDialog(1);
            }
        });

        /*view.findViewById(R.id.coin_80_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                *//*aseandcb.AseandcbCountryPay("Indonesia", "Top Model Indo: Coins Purchased Successfully",
                        price[2], "80 Coins");*//*

                showPurchaseDialog(2);
            }
        });*/

        view.findViewById(R.id.coin_120_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*aseandcb.AseandcbCountryPay("Indonesia", "Top Model Indo: Coins Purchased Successfully",
                        price[3], "130 Coins");*/

                showPurchaseDialog(2);
            }
        });

        /*view.findViewById(R.id.coin_200_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                *//*aseandcb.AseandcbCountryPay("Indonesia", "Top Model Indo: Coins Purchased Successfully",
                        price[4], "200 Coins");*//*
                showPurchaseDialog(4);
            }
        });*/

        view.findViewById(R.id.coin_250_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*aseandcb.AseandcbCountryPay("Indonesia", "Top Model Indo: Coins Purchased Successfully",
                        price[5], "250 Coins");*/
                showPurchaseDialog(3);
            }
        });
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        client = new AsyncHttpClient();
        client.setTimeout(40000);
        setToolBarTitle(getStringRes(R.string.title_coin_store));
        setupViews(mView);
        initOnClickListeners(mView);

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

        global = (TopModelGlobal) getActivity().getApplicationContext();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        client.cancelAllRequests(true);
    }

    /*private void awardCoins(String amt){
        Login userData = global.getUser_data();
        int currentWallet = Integer.parseInt(userData.getWallet());
        int newWallet;

        int amount = Integer.parseInt(amt);
        switch (amount){
            case 1000:
                newWallet = currentWallet + 20;
                break;
            case 2000:
                newWallet = currentWallet + 50;
                break;
            case 3000:
                newWallet = currentWallet + 80;
                break;
            case 5000:
                newWallet = currentWallet + 130;
                break;
            case 8000:
                newWallet = currentWallet + 200;
                break;
            case 10000:
                newWallet = currentWallet + 250;
                break;
            //test
            case 50:
                newWallet = currentWallet + 300;
                break;
            default:
                showSnack("Invalid Amount");
                newWallet = currentWallet;
                break;
        }

        userData.setWallet(String.valueOf(newWallet));
        global.setUser_data(userData);
        updateSharedPref(userData);
    }

    private void updateSharedPref(Login userData){
        Gson gson = new Gson();
        String json = gson.toJson(userData);
        SharedPreferences settings = getActivity().getSharedPreferences("sharedpref", 0);
        settings.edit().putString("user_data", json).commit();

        updateDatabase(userData.getWallet());
    }

    private void updateDatabase(final String amount){
        String url = "";
        try {
            url = "http://210.5.41.103/tuneapp/api/wallet/" +
                    //"{username}/" +
                    URLEncoder.encode(global.getUser_data().getUsername(), "UTF-8") + "/" +
                    //"{value}";
                    URLEncoder.encode(amount, "UTF-8");
        } catch (UnsupportedEncodingException e){
            showSnack(getStringRes(R.string.error_encoding));
        }
        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                showSnack(getStringRes(R.string.error_unknown));
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Gson gson = new GsonBuilder().create();
                JsonParser parser = new JsonParser();
                JsonArray jArray = parser.parse(responseString).getAsJsonArray();

                walletResponse = new ArrayList<WalletResponse>();
                for (JsonElement obj : jArray) {
                    WalletResponse temp = gson.fromJson(obj, WalletResponse.class);
                    walletResponse.add(temp);
                }

                if (walletResponse.get(0).getStatus().equalsIgnoreCase("success")){
                    if (walletResponse.get(0).getNew_wallet().equalsIgnoreCase(amount)) {
                        showSnack("Changes Saved Successfully");
                    } else {
                        showSnack("Wallet Mismatch");
                        global.getUser_data().setWallet(walletResponse.get(0).getNew_wallet());
                    }
                } else {
                    showSnack(getStringRes(R.string.error_unknown));
                }


            }
        });
    }*/

    /*@Override
    public void AseandcbChargingResult(String StatusCode, String amount, String telco) {
        if (adcbHelper.checkPayment(StatusCode, telco)){
            showSnack("Payment Successful");
            awardCoins(amount);
        }
    }*/

    private void showPurchaseDialog(int option) {
        final String adcbprice;
        final String sku;
        final String itemName;
        switch (option) {
            case 0:
                adcbprice = "2000";
                sku = "coin20";
                itemName = "20 Coins";
                break;
            case 1:
                adcbprice = "3000";
                sku = "coin50";
                itemName = "50 Coins";
                break;
            case 2:
                adcbprice = "5000";
                sku = "coin120";
                itemName = "120 Coins";
                break;
            case 3:
                adcbprice = "10000";
                sku = "coin250";
                itemName = "250 Coins";
                break;
            default:
                adcbprice = "0";
                sku = "0";
                itemName = "0 Coins";
                break;
        }

        final Dialog tdialog = new Dialog(getActivity(), R.style.ThemeDialogCustom);
        tdialog.setContentView(R.layout.dialog_purchase_options);
        tdialog.findViewById(R.id.adcb_pay_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tdialog.dismiss();
                /*aseandcb.AseandcbCountryPay("Indonesia", "Top Model Indo: Coins Purchased Successfully",
                        adcbprice, "20 Coins");*/
                aseandcb.AseandcbPay("Indonesia", adcbprice, "Top Model Indo: Coins Purchased Successfully", itemName);
            }
        });

        tdialog.findViewById(R.id.google_pay_pay_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tdialog.dismiss();
                ((MainActivity) getActivity()).googlePurchase(sku);
            }
        });

        tdialog.show();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
