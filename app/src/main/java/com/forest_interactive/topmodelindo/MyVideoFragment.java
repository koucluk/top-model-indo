package com.forest_interactive.topmodelindo;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.forest_interactive.topmodelindo.Adapters.VideoRecyclerViewAdapter;
import com.forest_interactive.topmodelindo.Models.Video;
import com.forest_interactive.topmodelindo.Widgets.CustomBaseFragment;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.lang.reflect.Array;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyVideoFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyVideoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyVideoFragment extends CustomBaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    View mView;

    TopModelGlobal global;

    AsyncHttpClient client;

    private boolean running;

    private OnFragmentInteractionListener mListener;

    ArrayList<Video> videoObj;

    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;


    public MyVideoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyVideoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyVideoFragment newInstance(String param1, String param2) {
        MyVideoFragment fragment = new MyVideoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_video, container, false);
        mView = view;

        RelativeLayout containerLayout = (RelativeLayout)view.findViewById(R.id.my_video_list_container);


        /*ArrayList<LinearLayout> templates = new ArrayList<LinearLayout>();
        for (int i = 0; i < 3; i++){
            LinearLayout layout = (LinearLayout)View.inflate(getActivity().getApplicationContext(),
                    R.layout.template_video,null);
            removePrice(layout);
            templates.add(layout);
        }

        for (int i = 0; i < templates.size(); i++) {
            containerLayout.addView(templates.get(i));
        }

        for (int i = 0;i<global.getPurchaseHistory().size(); i++) {
            Toast.makeText(getActivity().getApplicationContext(), "" +
                    global.getPurchaseHistory().get(i).getContent_id(), Toast.LENGTH_SHORT).show();
        }*/

        return view;
    }

    private void setUpViews(View view){
        SharedPreferences settings = getActivity().getApplicationContext().getSharedPreferences("sharedpref", 0);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.video_rv);
        mRecyclerView.setHasFixedSize(false);
        mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);


        //if (settings.getString("all_videos","").isEmpty()){
        if (settings.getString("video_data","").equalsIgnoreCase("")){
            getVideoData(view);
        } else {
            String json = settings.getString("video_data", "");
            Gson gson = new GsonBuilder().create();
            JsonParser parser = new JsonParser();
            JsonArray jArray = parser.parse(json).getAsJsonArray();

            videoObj = new ArrayList<Video>();

            for (JsonElement obj : jArray) {
                Video cse = gson.fromJson(obj, Video.class);
                //videoObj.add(cse);
                if (isPurchased(cse.getContent_id())) {
                    videoObj.add(cse);
                }
            }
            if (videoObj.size() > 0) {
                mAdapter = new VideoRecyclerViewAdapter(videoObj, (MainActivity) getActivity());
                mRecyclerView.setAdapter(mAdapter);
                ((VideoRecyclerViewAdapter) mAdapter).setOnItemClickListener(new VideoRecyclerViewAdapter.ListClickListener() {
                    @Override
                    public void onItemClick(int position, View v) {
                        Bundle arg = new Bundle();
                        arg.putString("content_id", videoObj.get(position).getContent_id());
                        arg.putString("artist_name", videoObj.get(position).getArtist_name());
                        arg.putString("video_name", videoObj.get(position).getVideo_name());
                        arg.putString("video_url", videoObj.get(position).getVideo_url());
                        arg.putString("video_description", Html.fromHtml(videoObj.get(position).getVideo_description()).toString().trim());
                        arg.putString("publishdate", videoObj.get(position).getPublishDate());
                        arg.putString("total_download", videoObj.get(position).getTotal_download());
                        arg.putString("total_count", videoObj.get(position).getTotal_count());
                        arg.putString("thumbnail", videoObj.get(position).getThumbnail_url());
                        arg.putString("price", videoObj.get(position).getPrice());

                        changeFragment(new VideoDetailFragment(), "videodetail", arg);
                    }
                });

                crossFadeViews(view.findViewById(R.id.video_progress), mRecyclerView);
                    /*//Disable RecyclerView Scroll - Set Static Height
                    RelativeLayout rvContainer = (RelativeLayout) view.findViewById(R.id.my_video_list_container);
                    ViewGroup.LayoutParams params = rvContainer.getLayoutParams();
                    params.height = convertToDp(140 * videoObj.size());
                    rvContainer.setLayoutParams(params);*/



            } else {
                crossFadeViews(view.findViewById(R.id.video_progress), view.findViewById(R.id.video_rv_error_tv));
            }

        }
    }

    private void getVideoData(final View view){

        if (isNetworkAvailable()) {
            client.get("http://210.5.41.103/tuneapp/api/Video/top%20model%20indo", new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    if (running) {
                        //Toast.makeText(getActivity().getApplicationContext(), "Fail", Toast.LENGTH_SHORT).show();
                        TextView error_tv = (TextView) view.findViewById(R.id.video_rv_error_tv);
                        error_tv.setText(getStringRes(R.string.error_unknown));
                        crossFadeViews(view.findViewById(R.id.video_progress), view.findViewById(R.id.video_rv_error_tv));
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    if (running) {
                        Gson gson = new GsonBuilder().create();
                        JsonParser parser = new JsonParser();
                        JsonArray jArray = parser.parse(responseString).getAsJsonArray();

                        videoObj = new ArrayList<Video>();

                        for (JsonElement obj : jArray) {
                            Video cse = gson.fromJson(obj, Video.class);
                            //videoObj.add(cse);
                            if (isPurchased(cse.getContent_id())) {
                                videoObj.add(cse);
                            }
                        }

                        if (videoObj.size() > 0) {
                            mAdapter = new VideoRecyclerViewAdapter(videoObj, (MainActivity) getActivity());
                            mRecyclerView.setAdapter(mAdapter);
                            ((VideoRecyclerViewAdapter) mAdapter).setOnItemClickListener(new VideoRecyclerViewAdapter.ListClickListener() {
                                @Override
                                public void onItemClick(int position, View v) {
                                    Bundle arg = new Bundle();
                                    arg.putString("content_id", videoObj.get(position).getContent_id());
                                    arg.putString("artist_name", videoObj.get(position).getArtist_name());
                                    arg.putString("video_name", videoObj.get(position).getVideo_name());
                                    arg.putString("video_url", videoObj.get(position).getVideo_url());
                                    arg.putString("video_description", Html.fromHtml(videoObj.get(position).getVideo_description()).toString().trim());
                                    arg.putString("publishdate", videoObj.get(position).getPublishDate());
                                    arg.putString("total_download", videoObj.get(position).getTotal_download());
                                    arg.putString("total_count", videoObj.get(position).getTotal_count());
                                    arg.putString("thumbnail", videoObj.get(position).getThumbnail_url());
                                    arg.putString("price", videoObj.get(position).getPrice());

                                    changeFragment(new VideoDetailFragment(), "videodetail", arg);
                                }
                            });

                            crossFadeViews(view.findViewById(R.id.video_progress), mRecyclerView);
                    /*//Disable RecyclerView Scroll - Set Static Height
                    RelativeLayout rvContainer = (RelativeLayout) view.findViewById(R.id.my_video_list_container);
                    ViewGroup.LayoutParams params = rvContainer.getLayoutParams();
                    params.height = convertToDp(140 * videoObj.size());
                    rvContainer.setLayoutParams(params);*/

                        } else {
                            crossFadeViews(view.findViewById(R.id.video_progress), view.findViewById(R.id.video_rv_error_tv));
                        }
                    }
                }

            });
        } else {
            //showSnack("Unable to connect to the Internet");
            showSnack(getStringRes(R.string.error_no_internet));
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        client = new AsyncHttpClient();
        client.setTimeout(40000);
        setToolBarTitle(getStringRes(R.string.title_my_videos));
        setUpViews(mView);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        global = (TopModelGlobal) getActivity().getApplicationContext();
        running = true;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        client.cancelAllRequests(true);
        running = false;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void removePrice(View rootView){
        ImageView coinImage = (ImageView) rootView.findViewById(R.id.video_list_coin);
        TextView priceText = (TextView) rootView.findViewById(R.id.video_list_price);
        coinImage.setVisibility(View.GONE);
        priceText.setVisibility(View.GONE);
    }
}
